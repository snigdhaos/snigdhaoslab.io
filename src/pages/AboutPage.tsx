import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import Philosophy from "../components/about/Philosophy";
import Differentiators from "../components/about/Differentiators";
import SystemRequirements from "../components/about/SystemRequirements";
import PerformanceMetrics from "../components/about/PerformanceMetrics";
import Timeline from "../components/about/Timeline";
import Milestones from "../components/about/Milestones";

const staggerContainer = {
  visible: {
    transition: {
      staggerChildren: 0.1,
    },
  },
};

export default function AboutPage() {
  const [ref, inView] = useInView({ triggerOnce: true });

  return (
    <div className="min-h-screen py-16">
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={staggerContainer}
      >
        <Philosophy />
        <Differentiators />
        <Timeline />
        <Milestones />
        <SystemRequirements />
        <PerformanceMetrics />
      </motion.div>
    </div>
  );
}
