import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { Download, Terminal, HardDrive, Globe } from "lucide-react";
import EditionSelector from "../components/download/EditionSelector";
import VersionSelector from "../components/download/VersionSelector";
import MirrorSelector from "../components/download/MirrorSelector";
import NetworkSpeed from "../components/download/NetworkSpeed";
import FeatureGrid from "../components/download/FeatureGrid";
import InstallationSteps from "../components/download/InstallationSteps";
import SystemRequirements from "../components/download/SystemRequirements";
import VerifyChecksum from "../components/download/VerifyChecksum";
import DownloadButton from "../components/download/DownloadButton";
import HardwareChecker from "../components/download/HardwareChecker";
import { getEditions } from "../data/editions";
import { getMirrors, getClosestMirror } from "../data/mirrors";
import { getVersions, getLatestVersion } from "../data/versions";
import type { Edition, Version, Mirror } from "../types";

const fadeInUp = {
  hidden: { opacity: 0, y: 20 },
  visible: { opacity: 1, y: 0 },
};

const staggerContainer = {
  visible: {
    transition: {
      staggerChildren: 0.1,
    },
  },
};

export default function DownloadPage() {
  const [editions, setEditions] = useState<Edition[]>([]);
  const [mirrors, setMirrors] = useState<Mirror[]>([]);
  const [versions, setVersions] = useState<Version[]>([]);
  const [selectedEdition, setSelectedEdition] = useState<Edition | null>(null);
  const [selectedVersion, setSelectedVersion] = useState<Version | null>(null);
  const [selectedMirror, setSelectedMirror] = useState<Mirror | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [ref, inView] = useInView({ triggerOnce: true });

  useEffect(() => {
    async function fetchData() {
      try {
        setLoading(true);
        const [
          editionsData,
          mirrorsData,
          versionsData,
          closestMirror,
          latestVersion,
        ] = await Promise.all([
          getEditions(),
          getMirrors(),
          getVersions(),
          getClosestMirror(),
          getLatestVersion(),
        ]);

        setEditions(editionsData);
        setMirrors(mirrorsData);
        setVersions(versionsData);
        setSelectedEdition(editionsData[0]);
        setSelectedVersion(latestVersion);
        setSelectedMirror(closestMirror || mirrorsData[0]);
        setError(null);
      } catch (err) {
        setError("Failed to load download options. Please try again later.");
        console.error("Error fetching data:", err);
      } finally {
        setLoading(false);
      }
    }

    fetchData();
  }, []);

  if (loading) {
    return (
      <div className="min-h-screen py-16 flex items-center justify-center">
        <div className="text-center">
          <div className="inline-block animate-spin rounded-full h-12 w-12 border-4 border-primary-500 border-t-transparent"></div>
          <p className="mt-4 text-lg text-gray-600 dark:text-gray-300">
            Loading download options...
          </p>
        </div>
      </div>
    );
  }

  if (error || !selectedEdition || !selectedVersion || !selectedMirror) {
    return (
      <div className="min-h-screen py-16 flex items-center justify-center">
        <div className="text-center">
          <p className="text-xl text-red-500 mb-4">{error}</p>
          <button
            onClick={() => window.location.reload()}
            className="btn-primary"
          >
            Try Again
          </button>
        </div>
      </div>
    );
  }

  const downloadUrl = `${selectedMirror.url}/${selectedVersion.version}/snigdha-os-${selectedEdition.name.toLowerCase()}-${selectedVersion.version}.iso`;

  return (
    <div className="min-h-screen py-16 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900">
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={staggerContainer}
        className="section-container relative"
      >
        {/* Background Elements */}
        <div className="absolute inset-0 overflow-hidden pointer-events-none">
          <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
          <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />
        </div>

        {/* Header */}
        <motion.div variants={fadeInUp} className="text-center mb-16 relative">
          <div className="inline-flex items-center justify-center p-3 mb-4 rounded-2xl bg-gradient-to-br from-primary-500 to-secondary-500">
            <Download className="w-8 h-8 text-white" />
          </div>
          <h1 className="text-4xl md:text-5xl font-bold mb-4">
            Download <span className="gradient-text">Snigdha OS</span>
          </h1>
          <p className="text-xl text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
            Experience the next generation of Linux with unmatched performance
            and security
          </p>
        </motion.div>

        <motion.div variants={fadeInUp}>
          <FeatureGrid />
        </motion.div>

        {/* Network Speed Indicator */}
        <motion.div variants={fadeInUp} className="max-w-4xl mx-auto mb-12">
          <NetworkSpeed />
        </motion.div>

        {/* Main Selection Sections - Wider on desktop */}
        <div className="max-w-6xl mx-auto space-y-8">
          {/* Edition Selection */}
          <motion.div variants={fadeInUp} className="card">
            <div className="flex items-center gap-3 mb-8">
              <div className="p-2 rounded-lg bg-primary-100 dark:bg-primary-900/30">
                <Terminal className="w-6 h-6 text-primary-500" />
              </div>
              <h2 className="text-2xl font-semibold">Choose Edition</h2>
            </div>
            <EditionSelector
              editions={editions}
              selectedEdition={selectedEdition}
              onSelect={setSelectedEdition}
            />
          </motion.div>

          {/* Version Selection */}
          <motion.div variants={fadeInUp} className="card">
            <div className="flex items-center gap-3 mb-8">
              <div className="p-2 rounded-lg bg-secondary-100 dark:bg-secondary-900/30">
                <HardDrive className="w-6 h-6 text-secondary-500" />
              </div>
              <h2 className="text-2xl font-semibold">Select Version</h2>
            </div>
            <VersionSelector
              versions={versions}
              selectedVersion={selectedVersion}
              onSelect={setSelectedVersion}
            />
          </motion.div>

          {/* Mirror Selection */}
          <motion.div variants={fadeInUp} className="card">
            <div className="flex items-center gap-3 mb-8">
              <div className="p-2 rounded-lg bg-accent-100 dark:bg-accent-900/30">
                <Globe className="w-6 h-6 text-accent-500" />
              </div>
              <h2 className="text-2xl font-semibold">Choose Mirror</h2>
            </div>
            <MirrorSelector
              mirrors={mirrors}
              selectedMirror={selectedMirror}
              onSelect={setSelectedMirror}
            />
          </motion.div>

          {/* Additional Sections - Standard width */}
          <div className="max-w-4xl mx-auto">
            <motion.div variants={fadeInUp}>
              <HardwareChecker edition={selectedEdition} />
            </motion.div>

            <motion.div variants={fadeInUp} className="mt-8">
              <SystemRequirements edition={selectedEdition} />
            </motion.div>

            <motion.div variants={fadeInUp} className="mt-8">
              <InstallationSteps />
            </motion.div>

            <motion.div variants={fadeInUp} className="mt-8">
              <VerifyChecksum checksum={selectedVersion.checksum} />
            </motion.div>

            <motion.div variants={fadeInUp} className="mt-8">
              <DownloadButton
                edition={selectedEdition}
                version={selectedVersion}
                downloadUrl={downloadUrl}
              />
            </motion.div>
          </div>
        </div>
      </motion.div>
    </div>
  );
}
