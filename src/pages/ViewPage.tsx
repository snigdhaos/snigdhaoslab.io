import { useState, useEffect } from "react";
import { motion, AnimatePresence } from "framer-motion";
import { useInView } from "react-intersection-observer";
import {
  Image as ImageIcon,
  Loader2,
  Camera,
  Layout,
  Palette,
  Terminal,
} from "lucide-react";
import { categories, getScreenshots } from "../data/screenshots";
import CategorySelector from "../components/screenshots/CategorySelector";
import ScreenshotCard from "../components/screenshots/ScreenshotCard";
import ScreenshotModal from "../components/screenshots/ScreenshotModal";
import type { Screenshot } from "../types";

const pageVariants = {
  initial: { opacity: 0 },
  animate: {
    opacity: 1,
    transition: {
      staggerChildren: 0.1,
    },
  },
};

const itemVariants = {
  initial: { opacity: 0, y: 20 },
  animate: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.5,
      ease: "easeOut",
    },
  },
};

const stats = [
  {
    icon: Camera,
    label: "Total Screenshots",
    value: "50+",
    color: "from-blue-500 to-indigo-500",
  },
  {
    icon: Layout,
    label: "Categories",
    value: "4",
    color: "from-purple-500 to-pink-500",
  },
  {
    icon: Palette,
    label: "Themes",
    value: "10+",
    color: "from-amber-500 to-orange-500",
  },
  {
    icon: Terminal,
    label: "Configurations",
    value: "25+",
    color: "from-emerald-500 to-teal-500",
  },
];

export default function ViewPage() {
  const [selectedCategory, setSelectedCategory] = useState(categories[0].id);
  const [screenshots, setScreenshots] = useState<Screenshot[]>([]);
  const [selectedScreenshot, setSelectedScreenshot] =
    useState<Screenshot | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [ref, inView] = useInView({ triggerOnce: true });

  useEffect(() => {
    async function fetchScreenshots() {
      try {
        setLoading(true);
        const data = await getScreenshots();
        setScreenshots(data);
      } catch (err) {
        setError(
          err instanceof Error ? err.message : "Failed to load screenshots",
        );
      } finally {
        setLoading(false);
      }
    }

    fetchScreenshots();
  }, []);

  const filteredScreenshots = screenshots.filter(
    (screenshot) => selectedCategory === screenshot.category,
  );

  return (
    <div className="min-h-screen py-16 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900">
      <motion.div
        ref={ref}
        initial="initial"
        animate={inView ? "animate" : "initial"}
        variants={pageVariants}
        className="section-container relative"
      >
        {/* Decorative Background Elements */}
        <div className="absolute inset-0 overflow-hidden pointer-events-none">
          <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
          <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />
          <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 w-full h-full max-w-6xl">
            <div className="absolute inset-0 bg-gradient-to-r from-primary-500/5 via-transparent to-secondary-500/5 rounded-3xl transform rotate-6" />
            <div className="absolute inset-0 bg-gradient-to-r from-secondary-500/5 via-transparent to-primary-500/5 rounded-3xl transform -rotate-6" />
          </div>
        </div>

        {/* Header Section */}
        <motion.div
          variants={itemVariants}
          className="relative text-center mb-16"
        >
          <div className="inline-flex items-center justify-center p-4 mb-6 rounded-2xl bg-white dark:bg-gray-800 shadow-xl">
            <ImageIcon className="w-10 h-10 text-primary-500" />
          </div>
          <h1 className="text-5xl md:text-6xl font-bold mb-6">
            Screenshot <span className="gradient-text">Gallery</span>
          </h1>
          <p className="text-xl text-gray-600 dark:text-gray-300 max-w-3xl mx-auto">
            Immerse yourself in the visual experience of Snigdha OS through our
            curated collection of stunning screenshots
          </p>
        </motion.div>

        {/* Stats Grid */}
        <motion.div
          variants={itemVariants}
          className="grid grid-cols-2 md:grid-cols-4 gap-6 mb-16"
        >
          {stats.map((stat) => (
            <motion.div
              key={stat.label}
              whileHover={{ y: -5 }}
              className="relative group"
            >
              <div className="bg-white dark:bg-gray-800 rounded-2xl p-6 shadow-lg">
                <div
                  className={`w-14 h-14 rounded-2xl bg-gradient-to-br ${stat.color} 
                             flex items-center justify-center mb-4 transform group-hover:scale-110 
                             group-hover:rotate-6 transition-transform duration-300`}
                >
                  <stat.icon className="w-7 h-7 text-white" />
                </div>
                <h3 className="text-3xl font-bold mb-2">{stat.value}</h3>
                <p className="text-gray-600 dark:text-gray-300">{stat.label}</p>
              </div>
            </motion.div>
          ))}
        </motion.div>

        {/* Category Selector */}
        <motion.div variants={itemVariants} className="max-w-5xl mx-auto mb-16">
          <CategorySelector
            categories={categories}
            selectedCategory={selectedCategory}
            onSelect={setSelectedCategory}
          />
        </motion.div>

        {/* Screenshots Grid */}
        {loading ? (
          <motion.div
            variants={itemVariants}
            className="flex flex-col items-center justify-center py-16"
          >
            <Loader2 className="w-12 h-12 text-primary-500 animate-spin mb-4" />
            <p className="text-gray-600 dark:text-gray-300">
              Loading amazing screenshots...
            </p>
          </motion.div>
        ) : error ? (
          <motion.div variants={itemVariants} className="text-center py-16">
            <div className="inline-flex items-center justify-center p-4 rounded-full bg-red-100 dark:bg-red-900/30 mb-4">
              <ImageIcon className="w-8 h-8 text-red-500" />
            </div>
            <p className="text-red-500 text-lg mb-4">{error}</p>
            <button
              onClick={() => window.location.reload()}
              className="px-6 py-2 bg-red-500 text-white rounded-lg hover:bg-red-600 transition-colors"
            >
              Try Again
            </button>
          </motion.div>
        ) : (
          <motion.div
            layout
            variants={itemVariants}
            className="grid md:grid-cols-2 lg:grid-cols-3 gap-8 max-w-6xl mx-auto"
          >
            <AnimatePresence mode="popLayout">
              {filteredScreenshots.map((screenshot) => (
                <motion.div
                  key={screenshot.id}
                  layout
                  initial={{ opacity: 0, scale: 0.8 }}
                  animate={{ opacity: 1, scale: 1 }}
                  exit={{ opacity: 0, scale: 0.8 }}
                  transition={{ duration: 0.3 }}
                >
                  <ScreenshotCard
                    screenshot={screenshot}
                    onClick={() => setSelectedScreenshot(screenshot)}
                  />
                </motion.div>
              ))}
            </AnimatePresence>
          </motion.div>
        )}

        {/* Screenshot Modal */}
        <AnimatePresence>
          {selectedScreenshot && (
            <ScreenshotModal
              screenshot={selectedScreenshot}
              onClose={() => setSelectedScreenshot(null)}
            />
          )}
        </AnimatePresence>

        {/* Empty State */}
        {!loading && !error && filteredScreenshots.length === 0 && (
          <motion.div variants={itemVariants} className="text-center py-16">
            <div className="inline-flex items-center justify-center p-4 rounded-full bg-gray-100 dark:bg-gray-800 mb-4">
              <ImageIcon className="w-8 h-8 text-gray-400" />
            </div>
            <h3 className="text-xl font-semibold mb-2">No Screenshots Found</h3>
            <p className="text-gray-600 dark:text-gray-300">
              There are no screenshots available in this category yet.
            </p>
          </motion.div>
        )}
      </motion.div>
    </div>
  );
}
