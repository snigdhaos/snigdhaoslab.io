import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import {
  Heart,
  Gift,
  Github,
  Star,
  Users,
  Award,
  DollarSign,
  Sparkles,
  Coffee,
  Zap,
} from "lucide-react";
import DonationCard from "../components/donate/DonationCard";
import DonorCard from "../components/donate/DonorCard";
import { getDonationTiers } from "../data/donations";
import { getDonors, getTotalDonations } from "../data/donors";
import type { DonationTier, Donor } from "../types";

const fadeInUp = {
  hidden: { opacity: 0, y: 20 },
  visible: { opacity: 1, y: 0 },
};

const staggerContainer = {
  visible: {
    transition: {
      staggerChildren: 0.1,
    },
  },
};

const impactStats = [
  {
    icon: Coffee,
    value: "24/7",
    label: "Development",
    description: "Continuous improvement and updates",
    color: "from-amber-500 to-orange-500",
  },
  {
    icon: Users,
    value: "50K+",
    label: "Community",
    description: "Active users worldwide",
    color: "from-blue-500 to-indigo-500",
  },
  {
    icon: Zap,
    value: "100+",
    label: "Updates",
    description: "Regular feature releases",
    color: "from-emerald-500 to-teal-500",
  },
  {
    icon: Sparkles,
    value: "1000+",
    label: "Contributions",
    description: "From our amazing community",
    color: "from-purple-500 to-pink-500",
  },
];

export default function DonatePage() {
  const [tiers, setTiers] = useState<DonationTier[]>([]);
  const [donors, setDonors] = useState<Donor[]>([]);
  const [totalDonations, setTotalDonations] = useState(0);
  const [loading, setLoading] = useState(true);
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.1,
  });

  useEffect(() => {
    async function fetchData() {
      const [tiersData, donorsData, total] = await Promise.all([
        getDonationTiers(),
        getDonors(),
        getTotalDonations(),
      ]);
      setTiers(tiersData);
      setDonors(donorsData);
      setTotalDonations(total);
      setLoading(false);
    }
    fetchData();
  }, []);

  return (
    <div className="min-h-screen py-16 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900">
      <motion.div
        ref={ref}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        variants={staggerContainer}
        className="section-container"
      >
        {/* Hero Section */}
        <motion.div variants={fadeInUp} className="text-center mb-16">
          <div className="inline-flex items-center justify-center p-3 mb-4 rounded-2xl bg-gradient-to-br from-red-500 to-pink-500">
            <Heart className="w-8 h-8 text-white" />
          </div>
          <h1 className="text-4xl md:text-5xl font-bold mb-4">
            Support <span className="gradient-text">Snigdha OS</span>
          </h1>
          <p className="text-xl text-gray-600 dark:text-gray-300 max-w-3xl mx-auto">
            Join our mission to create the most innovative Linux distribution.
            Your support helps us maintain and improve Snigdha OS for everyone.
          </p>
        </motion.div>

        {/* Impact Stats */}
        <motion.div
          variants={fadeInUp}
          className="grid md:grid-cols-4 gap-6 mb-16"
        >
          {impactStats.map((stat) => (
            <div key={stat.label} className="relative group">
              <div className="card h-full">
                {/* Background Glow */}
                <div
                  className={`absolute -inset-2 bg-gradient-to-r ${stat.color} rounded-3xl blur-xl 
                              opacity-0 group-hover:opacity-20 transition-opacity duration-500`}
                />

                <div className="relative">
                  <div
                    className={`w-16 h-16 rounded-2xl bg-gradient-to-br ${stat.color} 
                                flex items-center justify-center mb-6 transform group-hover:scale-110 
                                group-hover:rotate-6 transition-transform duration-300`}
                  >
                    <stat.icon className="w-8 h-8 text-white" />
                  </div>
                  <h3
                    className={`text-3xl font-bold mb-2 bg-gradient-to-r ${stat.color} bg-clip-text text-transparent`}
                  >
                    {stat.value}
                  </h3>
                  <h4 className="text-lg font-semibold mb-2">{stat.label}</h4>
                  <p className="text-gray-600 dark:text-gray-300">
                    {stat.description}
                  </p>
                </div>
              </div>
            </div>
          ))}
        </motion.div>

        {/* GitHub Sponsor Button */}
        <motion.div variants={fadeInUp} className="flex justify-center mb-16">
          <a
            href="https://github.com/sponsors/eshanized"
            target="_blank"
            rel="noopener noreferrer"
            className="group relative overflow-hidden px-8 py-4 bg-[#2A3544] hover:bg-[#22272E] 
                     text-white rounded-xl transition-all duration-300 hover:shadow-2xl hover:scale-105"
          >
            <div
              className="absolute inset-0 bg-gradient-to-r from-[#2EA043]/20 to-[#3FB950]/20 
                         transform rotate-45 translate-x-3/4 group-hover:translate-x-1/4 transition-transform duration-500"
            />
            <div className="relative flex items-center gap-3">
              <Github className="w-6 h-6" />
              <span className="text-lg font-medium">Sponsor on GitHub</span>
              <Star className="w-5 h-5 text-yellow-400 group-hover:rotate-45 transition-transform duration-300" />
            </div>
          </a>
        </motion.div>

        {/* Donation Tiers */}
        <div className="grid md:grid-cols-3 gap-8 max-w-6xl mx-auto mb-24">
          {tiers.map((tier, index) => (
            <DonationCard key={tier.name} tier={tier} index={index} />
          ))}
        </div>

        {/* Donors Section */}
        <motion.div variants={fadeInUp} className="mb-16">
          <div className="text-center mb-12">
            <h2 className="text-3xl font-bold mb-4">Our Amazing Supporters</h2>
            <p className="text-xl text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
              Join these incredible individuals who support the future of Linux
            </p>
          </div>
          <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-8">
            {donors.map((donor) => (
              <DonorCard key={donor.id} donor={donor} />
            ))}
          </div>
        </motion.div>

        {/* Additional Support Section */}
        <motion.div
          variants={fadeInUp}
          className="text-center max-w-3xl mx-auto"
        >
          <h2 className="text-2xl font-semibold mb-4">Other Ways to Support</h2>
          <p className="text-lg text-gray-600 dark:text-gray-300 mb-8">
            There are many ways to contribute to Snigdha OS beyond financial
            support
          </p>
          <div className="grid md:grid-cols-2 gap-6">
            <a
              href="/developers"
              className="card hover:shadow-xl transition-all duration-300 group hover:scale-105"
            >
              <div className="flex flex-col items-center p-6">
                <div
                  className="p-4 rounded-xl bg-gradient-to-br from-primary-500 to-secondary-500 mb-4 
                             group-hover:scale-110 transition-transform duration-300"
                >
                  <Gift className="w-8 h-8 text-white" />
                </div>
                <h3 className="text-xl font-semibold mb-3">Contribute Code</h3>
                <p className="text-gray-600 dark:text-gray-300">
                  Help us improve Snigdha OS by contributing to our codebase
                </p>
              </div>
            </a>
            <div className="card hover:shadow-xl transition-all duration-300 group hover:scale-105">
              <div className="flex flex-col items-center p-6">
                <div
                  className="p-4 rounded-xl bg-gradient-to-br from-secondary-500 to-accent-500 mb-4 
                             group-hover:scale-110 transition-transform duration-300"
                >
                  <Star className="w-8 h-8 text-white" />
                </div>
                <h3 className="text-xl font-semibold mb-3">Spread the Word</h3>
                <p className="text-gray-600 dark:text-gray-300">
                  Share Snigdha OS with your network and help us grow
                </p>
              </div>
            </div>
          </div>
        </motion.div>
      </motion.div>
    </div>
  );
}
