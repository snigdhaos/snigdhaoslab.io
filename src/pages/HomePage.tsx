import AnimatedHero from "../components/home/AnimatedHero";
import Features from "../components/home/Features";
import Statistics from "../components/home/Statistics";
import LatestRelease from "../components/home/LatestRelease";
// import Testimonials from '../components/home/Testimonials';
import QuickStart from "../components/home/QuickStart";
import Community from "../components/home/Community";

export default function HomePage() {
  return (
    <div className="min-h-screen">
      <AnimatedHero />
      <Statistics />
      <Features />
      <LatestRelease />
      {/* <Testimonials /> */}
      <QuickStart />
      <Community />
    </div>
  );
}
