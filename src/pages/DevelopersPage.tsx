import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { Code, Loader2 } from "lucide-react";
import { getDevelopers } from "../data/developers";
import DeveloperCard from "../components/developers/DeveloperCard";
import TeamStats from "../components/developers/TeamStats";
import type { TeamMember } from "../types";

export default function DevelopersPage() {
  const [developers, setDevelopers] = useState<TeamMember[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [ref, inView] = useInView({ triggerOnce: true });

  useEffect(() => {
    async function fetchDevelopers() {
      try {
        setLoading(true);
        const data = await getDevelopers();
        setDevelopers(data);
      } catch (err) {
        setError(
          err instanceof Error ? err.message : "Failed to load team members",
        );
      } finally {
        setLoading(false);
      }
    }

    fetchDevelopers();
  }, []);

  if (loading) {
    return (
      <div className="min-h-screen py-16 flex items-center justify-center">
        <Loader2 className="w-12 h-12 text-primary-500 animate-spin" />
      </div>
    );
  }

  if (error) {
    return (
      <div className="min-h-screen py-16 flex items-center justify-center">
        <div className="text-center">
          <p className="text-red-500 text-xl mb-4">{error}</p>
          <button
            onClick={() => window.location.reload()}
            className="btn-primary"
          >
            Try Again
          </button>
        </div>
      </div>
    );
  }

  return (
    <div className="min-h-screen py-16 bg-gradient-to-b from-gray-50 to-white dark:from-gray-900 dark:to-gray-800">
      <motion.div
        ref={ref}
        initial={{ opacity: 0 }}
        animate={inView ? { opacity: 1 } : { opacity: 0 }}
        className="section-container"
      >
        {/* Header */}
        <motion.div
          initial={{ opacity: 0, y: 20 }}
          animate={{ opacity: 1, y: 0 }}
          className="text-center mb-16"
        >
          <div className="inline-flex items-center justify-center p-3 mb-4 rounded-2xl bg-white dark:bg-gray-800 shadow-md">
            <Code className="w-8 h-8 text-primary-500" />
          </div>
          <h1 className="text-4xl md:text-5xl font-bold mb-4">
            Meet Our <span className="gradient-text">Team</span>
          </h1>
          <p className="text-xl text-gray-600 dark:text-gray-300 max-w-3xl mx-auto">
            The brilliant minds behind Snigdha OS, working together to create
            the most innovative Linux distribution.
          </p>
        </motion.div>

        {/* Team Stats */}
        <div className="mb-16">
          <TeamStats />
        </div>

        {/* Team Members Grid */}
        <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-8">
          {developers.map((developer) => (
            <DeveloperCard key={developer.id} developer={developer} />
          ))}
        </div>
      </motion.div>
    </div>
  );
}
