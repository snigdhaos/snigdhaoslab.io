import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import SearchBar from "../components/tools/SearchBar";
import CategoryFilter from "../components/tools/CategoryFilter";
import PackageList from "../components/tools/PackageList";
import MaintainersSection from "../components/tools/MaintainersSection";
import { getCategories, searchPackages } from "../data/tools";
import { Package as PackageIcon, Loader2 } from "lucide-react";
import type { Package } from "../types";

export default function ToolsPage() {
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedCategory, setSelectedCategory] = useState("All");
  const [packages, setPackages] = useState<Package[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [ref, inView] = useInView({ triggerOnce: true });

  const categories = getCategories();

  useEffect(() => {
    const fetchPackages = async () => {
      try {
        setLoading(true);
        const response = await fetch(
          "https://gitlab.com/api/v4/projects/snigdhaos%2Fpkgbuilds/repository/files/packages.json/raw",
        );
        if (!response.ok) throw new Error("Failed to fetch packages");

        const data = await response.json();

        // Transform the data into a flat array with category information
        const packagesArray = Object.entries(data).flatMap(([category, pkgs]) =>
          (pkgs as Package[]).map((pkg) => ({
            ...pkg,
            category,
          })),
        );

        setPackages(packagesArray);
      } catch (err) {
        setError(err instanceof Error ? err.message : "An error occurred");
      } finally {
        setLoading(false);
      }
    };

    fetchPackages();
  }, []);

  const filteredPackages =
    packages.length > 0
      ? searchPackages(packages, searchQuery, selectedCategory)
      : [];

  return (
    <div className="min-h-screen py-16 bg-gradient-to-b from-gray-50 to-white dark:from-gray-900 dark:to-gray-800">
      <motion.div
        ref={ref}
        initial={{ opacity: 0 }}
        animate={inView ? { opacity: 1 } : { opacity: 0 }}
        className="section-container relative"
      >
        {/* Decorative Elements */}
        <div className="absolute inset-0 overflow-hidden pointer-events-none">
          <div className="absolute -top-40 -right-40 w-80 h-80 bg-primary-500/10 rounded-full blur-3xl" />
          <div className="absolute -bottom-40 -left-40 w-80 h-80 bg-secondary-500/10 rounded-full blur-3xl" />
        </div>

        {/* Header Section */}
        <div className="relative">
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            className="text-center mb-12"
          >
            <div className="inline-flex items-center justify-center p-2 mb-4 rounded-2xl bg-white dark:bg-gray-800 shadow-md">
              <PackageIcon className="w-8 h-8 text-primary-500" />
            </div>
            <h1 className="text-4xl md:text-5xl font-bold mb-4">
              Package <span className="gradient-text">Repository</span>
            </h1>
            <p className="text-lg text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
              Discover and install powerful tools and applications for your
              Snigdha OS system
            </p>
          </motion.div>

          {/* Search and Filter Section */}
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ delay: 0.2 }}
            className="max-w-5xl mx-auto mb-12"
          >
            <div className="bg-white/80 dark:bg-gray-800/80 backdrop-blur-xl rounded-2xl shadow-xl p-6">
              <div className="flex flex-col md:flex-row gap-4 items-start">
                <div className="w-full md:flex-1">
                  <SearchBar value={searchQuery} onChange={setSearchQuery} />
                </div>
                <CategoryFilter
                  categories={categories}
                  selectedCategory={selectedCategory}
                  onSelect={setSelectedCategory}
                />
              </div>
            </div>
          </motion.div>

          {/* Results Section */}
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ delay: 0.3 }}
            className="relative z-10"
          >
            <div className="max-w-5xl mx-auto">
              {loading ? (
                <div className="flex items-center justify-center py-12">
                  <Loader2 className="w-8 h-8 text-primary-500 animate-spin" />
                </div>
              ) : error ? (
                <div className="text-center py-12">
                  <p className="text-red-500">{error}</p>
                </div>
              ) : (
                <>
                  <div className="flex items-center justify-between mb-6">
                    <h2 className="text-xl font-semibold">
                      {selectedCategory === "All"
                        ? "All Packages"
                        : `${selectedCategory} Packages`}
                    </h2>
                    <span className="text-sm text-gray-500 dark:text-gray-400">
                      {filteredPackages.length}{" "}
                      {filteredPackages.length === 1 ? "package" : "packages"}{" "}
                      found
                    </span>
                  </div>
                  <PackageList packages={filteredPackages} />
                </>
              )}
            </div>
          </motion.div>

          {/* Maintainers Section */}
          <MaintainersSection />
        </div>
      </motion.div>
    </div>
  );
}
