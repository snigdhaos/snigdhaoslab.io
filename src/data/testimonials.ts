import type { Testimonial } from "../types";

const API_URL = "https://api.snigdha-os.org/v1";

export async function getTestimonials(): Promise<Testimonial[]> {
  // Simulated API call - replace with actual API endpoint
  return [
    {
      quote:
        "Snigdha OS has transformed my development workflow. It's blazing fast and incredibly stable.",
      author: "Alex Chen",
      role: "Senior Software Engineer",
      githubUsername: "torvalds",
      company: "Google",
    },
    {
      quote:
        "The attention to detail and performance optimizations make this distribution stand out from the rest.",
      author: "Sarah Johnson",
      role: "DevOps Engineer",
      githubUsername: "gaearon",
      company: "Microsoft",
    },
    {
      quote:
        "Finally, a Linux distribution that combines power with elegance. The UI is simply beautiful.",
      author: "Michael Park",
      role: "UI/UX Designer",
      githubUsername: "yyx990803",
      company: "Apple",
    },
  ];
}

export function getGitHubAvatar(username: string): string {
  return `https://github.com/${username}.png`;
}

export function getGitLabAvatar(username: string): string {
  return `https://gitlab.com/${username}.png`;
}
