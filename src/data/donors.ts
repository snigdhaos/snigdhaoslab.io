import type { Donor } from "../types";

export async function getDonors(): Promise<Donor[]> {
  // Simulated API call - replace with actual API endpoint
  return [
    {
      id: "1",
      name: "John Smith",
      avatar: "#?auto=format&fit=crop&w=200&h=200&q=80",
      amount: 500,
      tier: "Champion",
      joinDate: "2024-01-15",
      testimonial: "Proud to support this amazing project!",
      social: {
        github: "johnsmith",
        twitter: "johnsmith_dev",
      },
      badges: ["Early Supporter", "Top Donor"],
    },
    {
      id: "2",
      name: "Emily Chen",
      avatar: "#?auto=format&fit=crop&w=200&h=200&q=80",
      amount: 250,
      tier: "Enthusiast",
      joinDate: "2024-02-01",
      testimonial: "Love seeing this project grow!",
      social: {
        github: "emilychen",
        gitlab: "emilychen",
      },
      badges: ["Regular Donor"],
    },
    {
      id: "3",
      name: "Michael Park",
      avatar: "#?auto=format&fit=crop&w=200&h=200&q=80",
      amount: 100,
      tier: "Supporter",
      joinDate: "2024-02-15",
      testimonial: "Happy to be part of this community!",
      social: {
        twitter: "mpark_dev",
      },
      badges: ["New Supporter"],
    },
  ];
}

export async function getTopDonors(limit: number = 5): Promise<Donor[]> {
  const donors = await getDonors();
  return donors.sort((a, b) => b.amount - a.amount).slice(0, limit);
}

export async function getTotalDonations(): Promise<number> {
  const donors = await getDonors();
  return donors.reduce((total, donor) => total + donor.amount, 0);
}
