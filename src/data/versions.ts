import type { Version } from "../types";

const API_URL = "https://api.snigdha-os.org/v1";

export async function getVersions(): Promise<Version[]> {
  // Simulated API call - replace with actual API endpoint
  return [
    {
      version: "2.0.0",
      releaseDate: "March 15, 2025",
      size: "2.1 GB",
      isLatest: true,
      checksum:
        "sha256:8f4e5d6a7b8c9d0e1f2a3b4c5d6e7f8g9h0i1j2k3l4m5n6o7p8q9r0s1t2u3v4w5x6y7z",
    },
    {
      version: "1.9.0",
      releaseDate: "January 1, 2025",
      size: "2.0 GB",
      isLatest: false,
      checksum:
        "sha256:1a2b3c4d5e6f7g8h9i0j1k2l3m4n5o6p7q8r9s0t1u2v3w4x5y6z7a8b9c0d1e2f3g4h5",
    },
  ];
}

export async function getLatestVersion(): Promise<Version> {
  const versions = await getVersions();
  return versions.find((version) => version.isLatest) || versions[0];
}

export async function getVersionByNumber(
  versionNumber: string,
): Promise<Version | undefined> {
  const versions = await getVersions();
  return versions.find((version) => version.version === versionNumber);
}

export async function getVersionChangelog(version: string): Promise<string[]> {
  // Simulated API call - replace with actual API endpoint
  return [
    "Enhanced system performance and boot time optimization",
    "Updated package manager with improved dependency resolution",
    "New desktop environment themes and customization options",
    "Security patches and kernel updates",
  ];
}
