import type { GitLabCommit } from "../types";

const GITLAB_API = "https://gitlab.com/api/v4";
const GROUP_PATH = "snigdhaos";

export async function getLatestCommits(): Promise<GitLabCommit[]> {
  try {
    const response = await fetch(
      `${GITLAB_API}/groups/${encodeURIComponent(GROUP_PATH)}/projects`,
    );

    if (!response.ok) {
      throw new Error("Failed to fetch projects");
    }

    const projects = await response.json();
    const commits: GitLabCommit[] = [];

    // Fetch commits from all projects in the group
    await Promise.all(
      projects.map(async (project: { id: number }) => {
        const commitsResponse = await fetch(
          `${GITLAB_API}/projects/${project.id}/repository/commits?per_page=15`,
        );

        if (commitsResponse.ok) {
          const projectCommits: GitLabCommit[] = await commitsResponse.json();
          commits.push(...projectCommits);
        }
      }),
    );

    // Sort commits by date and get the most recent 15
    return commits
      .sort(
        (a, b) =>
          new Date(b.authored_date).getTime() -
          new Date(a.authored_date).getTime(),
      )
      .slice(0, 15);
  } catch (error) {
    console.error("Error fetching commits:", error);
    return [];
  }
}
