import type { Edition } from "../types";

// const API_URL = 'https://api.snigdha-os.org/v1';

export async function getEditions(): Promise<Edition[]> {
  // Simulated API call - replace with actual API endpoint
  return [
    {
      name: "Desktop",
      description: "Full featured desktop environment with productivity tools",
      size: "2.1 GB",
      requirements: {
        cpu: "Dual Core 2GHz",
        ram: "4GB",
        storage: "20GB",
      },
    },
    {
      name: "Developer",
      description: "Optimized for software development with essential tools",
      size: "2.4 GB",
      requirements: {
        cpu: "Quad Core 2.5GHz",
        ram: "8GB",
        storage: "30GB",
      },
    },
    {
      name: "Server",
      description: "Minimal installation for servers and cloud deployment",
      size: "1.2 GB",
      requirements: {
        cpu: "Dual Core 1.5GHz",
        ram: "2GB",
        storage: "10GB",
      },
    },
    {
      name: "Minimal",
      description: "Base system with essential packages only",
      size: "800 MB",
      requirements: {
        cpu: "Single Core 1GHz",
        ram: "1GB",
        storage: "5GB",
      },
    },
  ];
}

export async function getEditionByName(
  name: string,
): Promise<Edition | undefined> {
  const editions = await getEditions();
  return editions.find(
    (edition) => edition.name.toLowerCase() === name.toLowerCase(),
  );
}
