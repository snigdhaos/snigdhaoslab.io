import type { Screenshot, ScreenshotCategory } from "../types";

export const categories: ScreenshotCategory[] = [
  {
    id: "desktop",
    name: "Desktop Environment",
    description: "Beautiful and functional desktop screenshots",
  },
  {
    id: "apps",
    name: "Applications",
    description: "Native and third-party applications",
  },
  {
    id: "themes",
    name: "Themes & Customization",
    description: "Different themes and customization options",
  },
  {
    id: "terminal",
    name: "Terminal & CLI",
    description: "Command line interface and terminal customizations",
  },
];

export async function getScreenshots(): Promise<Screenshot[]> {
  // In a real app, this would fetch from an API
  return [
    {
      id: "1",
      title: "Default Desktop",
      description: "Clean and minimal desktop environment",
      category: "desktop",
      imageUrl:
        "https://images.unsplash.com/photo-1629654297299-c8506221ca97?auto=format&fit=crop&w=1920&q=80",
      tags: ["desktop", "default", "clean"],
      uploadedBy: "Alex Chen",
      uploadDate: "2025-03-15",
    },
    {
      id: "2",
      title: "Development Setup",
      description: "Perfect environment for coding",
      category: "apps",
      imageUrl:
        "https://images.unsplash.com/photo-1461749280684-dccba630e2f6?auto=format&fit=crop&w=1920&q=80",
      tags: ["coding", "development", "IDE"],
      uploadedBy: "Sarah Johnson",
      uploadDate: "2025-03-14",
    },
    {
      id: "3",
      title: "Dark Theme",
      description: "Elegant dark theme customization",
      category: "themes",
      imageUrl:
        "https://images.unsplash.com/photo-1555099962-4199c345e5dd?auto=format&fit=crop&w=1920&q=80",
      tags: ["dark", "theme", "custom"],
      uploadedBy: "Michael Park",
      uploadDate: "2025-03-13",
    },
    {
      id: "4",
      title: "Terminal Setup",
      description: "Customized terminal with plugins",
      category: "terminal",
      imageUrl:
        "https://images.unsplash.com/photo-1629654297299-c8506221ca97?auto=format&fit=crop&w=1920&q=80",
      tags: ["terminal", "cli", "plugins"],
      uploadedBy: "Emma Wilson",
      uploadDate: "2025-03-12",
    },
  ];
}

export async function getScreenshotsByCategory(
  categoryId: string,
): Promise<Screenshot[]> {
  const screenshots = await getScreenshots();
  return screenshots.filter((screenshot) => screenshot.category === categoryId);
}

export async function getScreenshotById(
  id: string,
): Promise<Screenshot | undefined> {
  const screenshots = await getScreenshots();
  return screenshots.find((screenshot) => screenshot.id === id);
}
