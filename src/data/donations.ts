import type { DonationTier } from "../types";

export async function getDonationTiers(): Promise<DonationTier[]> {
  return [
    {
      name: "Supporter",
      amount: 5,
      description: "Show your support for Snigdha OS",
      benefits: [
        "Name in contributors list",
        "Special Discord role",
        "Early access to beta releases",
        "Exclusive wallpapers pack",
      ],
    },
    {
      name: "Enthusiast",
      amount: 10,
      description: "Help us grow and improve",
      benefits: [
        "All Supporter benefits",
        "Priority support",
        "Vote on feature requests",
        "Monthly development insights",
        "Custom forum badge",
      ],
    },
    {
      name: "Champion",
      amount: 25,
      description: "Become a vital part of our community",
      benefits: [
        "All Enthusiast benefits",
        "Access to developer meetings",
        "Name in release credits",
        "Exclusive merchandise",
        "Direct influence on roadmap",
      ],
    },
  ];
}
