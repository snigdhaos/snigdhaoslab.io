import type { TeamMember } from "../types";

export async function getDevelopers(): Promise<TeamMember[]> {
  // Simulated API call - replace with actual API endpoint
  return [
    {
      id: "1",
      name: "Eshan Roy",
      role: "Lead Developer & Maintainer",
      avatar: "#?auto=format&fit=crop&w=800&h=800&q=80",
      bio: "System architecture specialist with 10+ years of Linux kernel development experience.",
      location: "India",
      skills: [
        { name: "Kernel Development", level: 95 },
        { name: "System Architecture", level: 90 },
        { name: "C/C++", level: 88 },
        { name: "Rust", level: 85 },
      ],
      projects: [
        {
          name: "Kernel Optimization",
          description: "Led the kernel optimization initiative",
        },
        {
          name: "Boot Time Improvement",
          description: "Reduced boot time by 40%",
        },
      ],
      social: {
        gitlab: "eshanized",
        twitter: "eshanized",
        linkedin: "eshanized",
        email: "eshan@snigdhaos.org",
      },
      joinedDate: "2023-01",
    },
    {
      id: "2",
      name: "Dmitry Afamiew",
      role: "Security Engineer",
      avatar: "#?auto=format&fit=crop&w=800&h=800&q=80",
      bio: "Cybersecurity expert focused on system hardening and vulnerability assessment.",
      location: "Ukraine",
      skills: [
        { name: "Security Analysis", level: 92 },
        { name: "Penetration Testing", level: 88 },
        { name: "Python", level: 85 },
        { name: "Shell Scripting", level: 82 },
      ],
      projects: [
        {
          name: "Security Framework",
          description: "Developed the core security framework",
        },
        {
          name: "Threat Detection",
          description: "Implemented real-time threat detection",
        },
      ],
      social: {
        gitlab: "celestifyx",
        twitter: "celestifyx",
        linkedin: "celestifyx",
        email: "celestifyx@snigdhaos.org",
      },
      joinedDate: "2024-12",
    },
  ];
}

export async function getDeveloperById(
  id: string,
): Promise<TeamMember | undefined> {
  const developers = await getDevelopers();
  return developers.find((dev) => dev.id === id);
}

export async function getDevelopersBySkill(
  skill: string,
): Promise<TeamMember[]> {
  const developers = await getDevelopers();
  return developers.filter((dev) =>
    dev.skills.some((s) => s.name.toLowerCase().includes(skill.toLowerCase())),
  );
}
