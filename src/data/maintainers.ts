import type { PackageMaintainer } from "../types";

// const API_URL = 'https://api.snigdha-os.org/v1';

export async function getMaintainers(): Promise<PackageMaintainer[]> {
  // Simulated API call - replace with actual API endpoint
  return [
    {
      name: "Eshan Roy",
      email: "eshan@snigdhaos.org",
      github: "eshanized",
      matrix: "@eshanized:matrix.org",
      role: "maintainer",
      avatar:
        "https://gitlab.com/uploads/-/system/user/avatar/18545989/avatar.png?width=800?auto=format&fit=crop&w=200&h=200&q=80",
      pgpKey: "D5196799B9F40D5F35C45805289BD78BFDD9492D",
    },
    {
      name: "Dmitry Afamiew",
      email: "celestifyx@snigdhaos.org",
      github: "celestifyx",
      irc: "celestifyx",
      role: "maintainer",
      avatar:
        "https://gitlab.com/uploads/-/system/user/avatar/21889820/avatar.png?width=800?auto=format&fit=crop&w=200&h=200&q=80",
      pgpKey: "N/A",
    },
  ];
}

export async function getMaintainerByEmail(
  email: string,
): Promise<PackageMaintainer | undefined> {
  const maintainers = await getMaintainers();
  return maintainers.find((maintainer) => maintainer.email === email);
}

export async function getMaintainersByRole(
  role: "maintainer" | "contributor",
): Promise<PackageMaintainer[]> {
  const maintainers = await getMaintainers();
  return maintainers.filter((maintainer) => maintainer.role === role);
}
