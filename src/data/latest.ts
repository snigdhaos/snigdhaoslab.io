import type { Release } from "../types";

// const API_URL = "https://api.snigdha-os.org/v1";

export async function getLatestReleases(): Promise<Release[]> {
  // Simulated API call - replace with actual API endpoint
  return [
    {
      version: "2.0.0",
      date: "March 15, 2025",
      type: "Major Release",
      description: "Enhanced system performance and boot time optimization",
      changes: [
        "Improved package manager with AI-powered recommendations",
        "New desktop environment with enhanced customization options",
        "Kernel optimizations for better hardware support",
        "Security patches and system hardening",
      ],
      metrics: {
        bootTime: "15 seconds",
        memoryUsage: "500MB",
        packageInstallSpeed: "50MB/s",
      },
    },
    {
      version: "1.9.2",
      date: "February 28, 2025",
      type: "Security Update",
      description: "Critical security patches and performance improvements",
      changes: [
        "Fixed potential vulnerability in network stack",
        "Updated core system packages",
        "Performance optimizations for SSD storage",
        "Improved system responsiveness",
      ],
      metrics: {
        bootTime: "18 seconds",
        memoryUsage: "520MB",
        packageInstallSpeed: "45MB/s",
      },
    },
    {
      version: "1.9.1",
      date: "February 15, 2025",
      type: "Maintenance Update",
      description: "System stability and package updates",
      changes: [
        "Package repository synchronization improvements",
        "Desktop environment bug fixes",
        "Updated development tools",
        "System logging enhancements",
      ],
      metrics: {
        bootTime: "17 seconds",
        memoryUsage: "515MB",
        packageInstallSpeed: "45MB/s",
      },
    },
  ];
}

export async function getLatestRelease(): Promise<Release> {
  const releases = await getLatestReleases();
  return releases[0];
}

export async function getReleaseByVersion(
  version: string,
): Promise<Release | undefined> {
  const releases = await getLatestReleases();
  return releases.find((release) => release.version === version);
}

export async function getReleaseChangelog(version: string): Promise<string[]> {
  const release = await getReleaseByVersion(version);
  return release?.changes || [];
}
