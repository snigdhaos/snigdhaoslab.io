import type { Package } from "../types";

export const categories = ["All", "aur", "main"] as const;

export type Category = (typeof categories)[number];

export const getCategories = () => categories;

export const getPackagesByCategory = (
  packages: Package[],
  category: string,
) => {
  if (!Array.isArray(packages)) return [];
  if (category === "All") return packages;
  return packages.filter((pkg) => pkg.category === category);
};

export const searchPackages = (
  packages: Package[],
  query: string,
  category: string = "All",
) => {
  if (!Array.isArray(packages)) return [];
  const normalizedQuery = query.toLowerCase();
  const filteredByCategory = getPackagesByCategory(packages, category);
  return filteredByCategory.filter(
    (pkg) =>
      pkg.name.toLowerCase().includes(normalizedQuery) ||
      (pkg.description &&
        pkg.description.toLowerCase().includes(normalizedQuery)),
  );
};
