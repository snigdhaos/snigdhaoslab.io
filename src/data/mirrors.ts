import type { Mirror } from "../types";

const API_URL = "https://api.snigdha-os.org/v1";

export async function getMirrors(): Promise<Mirror[]> {
  try {
    // Simulated API call - replace with actual API endpoint
    return [
      {
        name: "Primary Mirror",
        location: "United States",
        url: "https://mirror1.snigdha-os.org",
        speed: 12.5,
      },
      {
        name: "European Mirror",
        location: "Germany",
        url: "https://mirror2.snigdha-os.org",
        speed: 10.2,
      },
      {
        name: "Asian Mirror",
        location: "Singapore",
        url: "https://mirror3.snigdha-os.org",
        speed: 8.7,
      },
    ];
  } catch (error) {
    console.error("Failed to fetch mirrors:", error);
    return [];
  }
}

export async function getClosestMirror(): Promise<Mirror | null> {
  try {
    const mirrors = await getMirrors();
    if (!mirrors.length) return null;

    // In a real implementation, you would:
    // 1. Get user's location (with permission)
    // 2. Calculate distance to each mirror
    // 3. Perform speed tests
    // 4. Return the optimal mirror

    // For now, return the first mirror
    return mirrors[0];
  } catch (error) {
    console.error("Failed to find closest mirror:", error);
    return null;
  }
}

export async function testMirrorSpeed(mirror: Mirror): Promise<number> {
  try {
    const startTime = performance.now();
    const response = await fetch(`${mirror.url}/ping`, {
      method: "HEAD",
      cache: "no-store",
    });
    const endTime = performance.now();

    if (!response.ok) throw new Error("Mirror not responding");

    // Calculate round-trip time in milliseconds
    const rtt = endTime - startTime;

    // Simulate bandwidth based on RTT (this is a simplified example)
    // In a real implementation, you would download a test file and measure actual speed
    const estimatedSpeed = (1000 / rtt) * 100; // Simplified calculation

    return Math.min(estimatedSpeed, mirror.speed); // Cap at advertised speed
  } catch (error) {
    console.error(`Failed to test mirror speed for ${mirror.name}:`, error);
    return 0;
  }
}
