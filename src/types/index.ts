export interface NavItem {
  label: string;
  path: string;
  icon?: React.ComponentType;
}

export interface FeatureCard {
  title: string;
  description: string;
  icon: React.ComponentType;
  color?: string;
}

export interface Skill {
  name: string;
  level: number;
}

export interface Project {
  name: string;
  description: string;
}

export interface SocialLink {
  gitlab: string;
  twitter: string;
  linkedin: string;
  email: string;
}

export interface TeamMember {
  id: string;
  name: string;
  role: string;
  avatar: string;
  bio: string;
  location: string;
  skills: Skill[];
  projects: Project[];
  social: SocialLink;
  joinedDate: string;
}

export interface DonationTier {
  name: string;
  amount: number;
  description: string;
  benefits: string[];
}

export interface Donor {
  id: string;
  name: string;
  avatar: string;
  amount: number;
  tier: string;
  joinDate: string;
  testimonial?: string;
  social?: {
    github?: string;
    gitlab?: string;
    twitter?: string;
  };
  badges: string[];
}

export interface OSVersion {
  version: string;
  releaseDate: string;
  size: string;
  checksum: string;
  mirrors: string[];
}

export interface PackageMaintainer {
  name: string;
  email: string;
  gitlab?: string;
  matrix?: string;
  irc?: string;
  avatar?: string;
  role: "maintainer" | "contributor";
  pgpKey?: string;
}

export interface Package {
  name: string;
  version: string;
  description: string;
  category?: string;
  depends: string[];
  maintainers: PackageMaintainer[];
  lastUpdated: string;
  repository?: string;
  bugTracker?: string;
}

export interface Edition {
  name: string;
  description: string;
  size: string;
  requirements: {
    cpu: string;
    ram: string;
    storage: string;
  };
}

export interface Version {
  version: string;
  releaseDate: string;
  size: string;
  isLatest: boolean;
  checksum: string;
}

export interface Mirror {
  name: string;
  location: string;
  url: string;
  speed: number;
}

export interface Release {
  version: string;
  date: string;
  type: string;
  description: string;
  changes: string[];
  metrics: {
    bootTime: string;
    memoryUsage: string;
    packageInstallSpeed: string;
  };
}

export interface Screenshot {
  id: string;
  title: string;
  description: string;
  category: string;
  imageUrl: string;
  tags: string[];
  uploadedBy: string;
  uploadDate: string;
}

export interface ScreenshotCategory {
  id: string;
  name: string;
  description: string;
}

export interface GitLabCommit {
  id: string;
  short_id: string;
  title: string;
  author_name: string;
  author_email: string;
  authored_date: string;
  message: string;
  web_url: string;
}

export interface Testimonial {
  quote: string;
  author: string;
  role: string;
  githubUsername?: string;
  gitlabUsername?: string;
  company?: string;
}
