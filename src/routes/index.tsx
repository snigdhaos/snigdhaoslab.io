import { lazy } from "react";
import { Routes, Route } from "react-router-dom";

const HomePage = lazy(() => import("../pages/HomePage"));
const AboutPage = lazy(() => import("../pages/AboutPage"));
// const ToolsPage = lazy(() => import("../pages/ToolsPage"));
const DownloadPage = lazy(() => import("../pages/DownloadPage"));
const DevelopersPage = lazy(() => import("../pages/DevelopersPage"));
const DonatePage = lazy(() => import("../pages/DonatePage"));
const ViewPage = lazy(() => import("../pages/ViewPage"));

export default function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/about" element={<AboutPage />} />
      {/* <Route path="/tools" element={<ToolsPage />} /> */}
      <Route path="/download" element={<DownloadPage />} />
      <Route path="/developers" element={<DevelopersPage />} />
      <Route path="/donate" element={<DonatePage />} />
      <Route path="/view" element={<ViewPage />} />
    </Routes>
  );
}
