import { useEffect } from "react";
import toast from "react-hot-toast";
import { GitBranch } from "lucide-react";
import { getLatestCommits } from "../data/commits";

export function useGitLabCommits() {
  useEffect(() => {
    let mounted = true;
    let currentToast: string | undefined;

    async function fetchAndShowCommits() {
      try {
        const commits = await getLatestCommits();

        if (!mounted) return;

        // Get the most recent commit
        const latestCommit = commits[0];

        // Dismiss previous toast if it exists
        if (currentToast) {
          toast.dismiss(currentToast);
        }

        // Show toast for the latest commit
        currentToast = toast.custom(
          (t) => (
            <div
              className={`${
                t.visible ? "animate-enter" : "animate-leave"
              } max-w-[calc(100vw-2rem)] w-full sm:max-w-md bg-gradient-to-br from-primary-500 to-secondary-500 shadow-2xl rounded-2xl 
              pointer-events-auto flex ring-1 ring-black ring-opacity-5 transform transition-all duration-500 
              hover:shadow-3xl hover:-translate-y-1`}
            >
              <div className="flex-1 w-0 p-4 sm:p-5">
                <div className="flex items-start">
                  <div className="flex-shrink-0 pt-0.5">
                    <div
                      className="h-12 w-12 sm:h-14 sm:w-14 rounded-xl bg-white/10 backdrop-blur-sm 
                                flex items-center justify-center transform hover:rotate-12 transition-transform"
                    >
                      <GitBranch className="h-6 w-6 sm:h-7 sm:w-7 text-white" />
                    </div>
                  </div>
                  <div className="ml-4 sm:ml-5 flex-1">
                    <p className="text-sm font-medium text-white line-clamp-1">
                      Latest Commit
                    </p>
                    <p className="mt-1 text-xs sm:text-sm text-white/80 line-clamp-2">
                      {latestCommit.title}
                    </p>
                    <div className="mt-2 sm:mt-3 flex items-center gap-2 text-xs text-white/60">
                      <span className="line-clamp-1">
                        {latestCommit.author_name}
                      </span>
                      <span>•</span>
                      <span>
                        {new Date(
                          latestCommit.authored_date,
                        ).toLocaleDateString()}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="flex border-l border-white/20">
                <a
                  href={latestCommit.web_url}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="flex items-center justify-center px-4 sm:px-5 py-2 border border-transparent text-xs sm:text-sm 
                           font-medium rounded-r-2xl text-white hover:text-white/80 focus:outline-none focus:ring-2 focus:ring-white/50"
                >
                  View
                </a>
              </div>
            </div>
          ),
          {
            duration: 5000, // Increased duration for better readability
            position: "bottom-right",
          },
        );
      } catch (error) {
        console.error("Failed to fetch commits:", error);
      }
    }

    fetchAndShowCommits();

    // Fetch new commits every 5 minutes
    const interval = setInterval(fetchAndShowCommits, 5 * 60 * 1000);

    return () => {
      mounted = false;
      clearInterval(interval);
      if (currentToast) {
        toast.dismiss(currentToast);
      }
    };
  }, []);
}