import { motion } from "framer-motion";
import { Users, Code, Github as Git, Globe } from "lucide-react";

const stats = [
  {
    icon: Users,
    label: "Team Members",
    value: "15+",
    color: "from-blue-500 to-indigo-500",
  },
  {
    icon: Code,
    label: "Commits",
    value: "2.5K+",
    color: "from-emerald-500 to-teal-500",
  },
  {
    icon: Git,
    label: "Pull Requests",
    value: "500+",
    color: "from-purple-500 to-pink-500",
  },
  {
    icon: Globe,
    label: "Countries",
    value: "10+",
    color: "from-amber-500 to-orange-500",
  },
];

export default function TeamStats() {
  return (
    <div className="grid grid-cols-2 md:grid-cols-4 gap-6">
      {stats.map((stat, index) => (
        <motion.div
          key={stat.label}
          initial={{ opacity: 0, y: 20 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ delay: index * 0.1 }}
          className="bg-white dark:bg-gray-800 rounded-2xl p-6 text-center group hover:shadow-lg 
                   transition-all duration-300"
        >
          <div className="mb-4">
            <div
              className={`w-16 h-16 mx-auto rounded-2xl bg-gradient-to-br ${stat.color} 
                       flex items-center justify-center transform group-hover:scale-110 
                       group-hover:rotate-6 transition-transform duration-300`}
            >
              <stat.icon className="w-8 h-8 text-white" />
            </div>
          </div>
          <h3 className="text-3xl font-bold mb-1 group-hover:text-primary-500 transition-colors">
            {stat.value}
          </h3>
          <p className="text-gray-600 dark:text-gray-300">{stat.label}</p>
        </motion.div>
      ))}
    </div>
  );
}
