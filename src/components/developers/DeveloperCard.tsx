import { motion } from "framer-motion";
import { MapPin, Calendar, Twitter, Linkedin, Mail } from "lucide-react";
import type { TeamMember } from "../../types";

interface DeveloperCardProps {
  developer: TeamMember;
}

export default function DeveloperCard({ developer }: DeveloperCardProps) {
  return (
    <motion.div
      initial={{ opacity: 0, y: 20 }}
      animate={{ opacity: 1, y: 0 }}
      whileHover={{ y: -5 }}
      className="bg-white dark:bg-gray-800 rounded-2xl shadow-xl hover:shadow-2xl 
                 transition-all duration-300 overflow-hidden group"
    >
      {/* Header with gradient background */}
      <div className="h-32 bg-gradient-to-r from-primary-500 to-secondary-500 relative overflow-hidden">
        <motion.div
          className="absolute inset-0 bg-gradient-to-r from-primary-600 to-secondary-600 opacity-0 
                     group-hover:opacity-100 transition-opacity duration-300"
        />
      </div>

      {/* Profile Content */}
      <div className="px-6 pb-6 -mt-16 relative">
        {/* Avatar */}
        <div className="relative mb-4">
          <div className="w-32 h-32 mx-auto">
            <img
              src={developer.avatar}
              alt={developer.name}
              className="w-full h-full rounded-full border-4 border-white dark:border-gray-800 object-cover
                       group-hover:scale-105 transition-transform duration-300"
            />
          </div>
        </div>

        {/* Info */}
        <div className="text-center mb-6">
          <h3 className="text-2xl font-bold mb-1 group-hover:text-primary-500 transition-colors">
            {developer.name}
          </h3>
          <p className="text-lg text-gray-600 dark:text-gray-300 mb-2">
            {developer.role}
          </p>
          <div className="flex items-center justify-center gap-2 text-sm text-gray-500 dark:text-gray-400">
            <MapPin className="w-4 h-4" />
            {developer.location}
            <span className="mx-2">•</span>
            <Calendar className="w-4 h-4" />
            Joined {developer.joinedDate}
          </div>
        </div>

        {/* Bio */}
        <p className="text-gray-600 dark:text-gray-300 text-center mb-6">
          {developer.bio}
        </p>

        {/* Skills */}
        <div className="mb-6">
          <h4 className="text-sm font-medium text-gray-500 dark:text-gray-400 mb-3">
            Skills
          </h4>
          <div className="space-y-2">
            {developer.skills.map((skill) => (
              <div key={skill.name} className="relative">
                <div className="flex justify-between text-sm mb-1">
                  <span className="font-medium">{skill.name}</span>
                  <span>{skill.level}%</span>
                </div>
                <div className="h-2 bg-gray-200 dark:bg-gray-700 rounded-full overflow-hidden">
                  <motion.div
                    initial={{ width: 0 }}
                    animate={{ width: `${skill.level}%` }}
                    transition={{ duration: 1, ease: "easeOut" }}
                    className="h-full bg-gradient-to-r from-primary-500 to-secondary-500 rounded-full"
                  />
                </div>
              </div>
            ))}
          </div>
        </div>

        {/* Projects */}
        <div className="mb-6">
          <h4 className="text-sm font-medium text-gray-500 dark:text-gray-400 mb-3">
            Recent Projects
          </h4>
          <div className="space-y-2">
            {developer.projects.map((project) => (
              <div
                key={project.name}
                className="p-3 rounded-lg bg-gray-50 dark:bg-gray-700/50 hover:bg-gray-100 
                         dark:hover:bg-gray-700 transition-colors"
              >
                <h5 className="font-medium mb-1">{project.name}</h5>
                <p className="text-sm text-gray-600 dark:text-gray-300">
                  {project.description}
                </p>
              </div>
            ))}
          </div>
        </div>

        {/* Social Links */}
        <div className="flex justify-center gap-4">
          <a
            href={`https://gitlab.com/${developer.social.gitlab}`}
            target="_blank"
            rel="noopener noreferrer"
            className="p-2 rounded-full bg-gray-100 dark:bg-gray-700 hover:bg-primary-100 
                     dark:hover:bg-primary-900/30 hover:text-primary-500 transition-colors"
          >
            <svg className="w-5 h-5" viewBox="0 0 24 24" fill="currentColor">
              <path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z" />
            </svg>
          </a>
          <a
            href={`https://twitter.com/${developer.social.twitter}`}
            target="_blank"
            rel="noopener noreferrer"
            className="p-2 rounded-full bg-gray-100 dark:bg-gray-700 hover:bg-primary-100 
                     dark:hover:bg-primary-900/30 hover:text-primary-500 transition-colors"
          >
            <Twitter className="w-5 h-5" />
          </a>
          <a
            href={`https://linkedin.com/in/${developer.social.linkedin}`}
            target="_blank"
            rel="noopener noreferrer"
            className="p-2 rounded-full bg-gray-100 dark:bg-gray-700 hover:bg-primary-100 
                     dark:hover:bg-primary-900/30 hover:text-primary-500 transition-colors"
          >
            <Linkedin className="w-5 h-5" />
          </a>
          <a
            href={`mailto:${developer.social.email}`}
            className="p-2 rounded-full bg-gray-100 dark:bg-gray-700 hover:bg-primary-100 
                     dark:hover:bg-primary-900/30 hover:text-primary-500 transition-colors"
          >
            <Mail className="w-5 h-5" />
          </a>
        </div>
      </div>
    </motion.div>
  );
}
