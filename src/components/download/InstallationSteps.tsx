import { motion } from "framer-motion";
import { ArrowRight } from "lucide-react";

const installSteps = [
  {
    title: "Download ISO",
    description: "Choose your edition and download the ISO file",
  },
  {
    title: "Create USB Drive",
    description: "Use your preferred tool to create bootable USB",
  },
  {
    title: "Boot & Install",
    description: "Boot from USB and follow installation wizard",
  },
  {
    title: "First Boot",
    description: "Complete initial setup and start using Snigdha OS",
  },
];

export default function InstallationSteps() {
  return (
    <motion.div
      initial={{ opacity: 0, y: 20 }}
      animate={{ opacity: 1, y: 0 }}
      className="card"
    >
      <div className="flex items-center gap-3 mb-6">
        <div className="p-2 rounded-lg bg-purple-100 dark:bg-purple-900/30">
          <ArrowRight className="w-6 h-6 text-purple-500" />
        </div>
        <h2 className="text-2xl font-semibold">Installation Steps</h2>
      </div>
      <div className="grid md:grid-cols-4 gap-6">
        {installSteps.map((step, index) => (
          <div key={step.title} className="relative group">
            <div className="p-6 rounded-xl bg-gray-50 dark:bg-gray-800/50 relative overflow-hidden h-full">
              <div
                className="absolute inset-0 bg-gradient-to-br from-primary-500/5 to-secondary-500/5 
                            opacity-0 group-hover:opacity-100 transition-opacity"
              />
              <div className="relative">
                <div className="flex items-center gap-3 mb-4">
                  <div
                    className="w-10 h-10 rounded-full bg-primary-100 dark:bg-primary-900/30 
                                flex items-center justify-center text-primary-500 font-semibold 
                                group-hover:scale-110 transition-transform"
                  >
                    {index + 1}
                  </div>
                  <h3 className="font-medium">{step.title}</h3>
                </div>
                <p className="text-gray-600 dark:text-gray-300">
                  {step.description}
                </p>
              </div>
            </div>
            {index < installSteps.length - 1 && (
              <div className="hidden md:block absolute top-1/2 -right-3 transform -translate-y-1/2">
                <ArrowRight className="w-6 h-6 text-gray-300 dark:text-gray-600" />
              </div>
            )}
          </div>
        ))}
      </div>
    </motion.div>
  );
}
