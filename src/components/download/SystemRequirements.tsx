import { motion } from "framer-motion";
import { Terminal, Cpu, MemoryStick as Memory, HardDrive } from "lucide-react";
import type { Edition } from "../../types";

interface SystemRequirementsProps {
  edition: Edition;
}

export default function SystemRequirements({
  edition,
}: SystemRequirementsProps) {
  return (
    <motion.div
      initial={{ opacity: 0, y: 20 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}
      className="card p-6 bg-white dark:bg-gray-900 rounded-xl shadow-lg"
    >
      <div className="flex items-center gap-3 mb-8">
        <div className="p-3 rounded-lg bg-emerald-100 dark:bg-emerald-900/30">
          <Terminal className="w-7 h-7 text-emerald-500" />
        </div>
        <h2 className="text-2xl font-bold text-gray-800 dark:text-white">
          System Requirements
        </h2>
      </div>
      <div className="grid md:grid-cols-3 gap-6">
        {/* CPU Card */}
        <motion.div
          whileHover={{ scale: 1.05 }}
          className="p-6 rounded-xl bg-gray-50 dark:bg-gray-800/50 relative group overflow-hidden 
                     border border-gray-200 dark:border-gray-700 transition-all"
        >
          <div
            className="absolute inset-0 bg-gradient-to-br from-blue-500/10 to-indigo-500/10 
                         opacity-0 group-hover:opacity-100 transition-opacity"
          />
          <div className="relative">
            <div className="flex items-center gap-3 mb-4">
              <Cpu className="w-7 h-7 text-blue-500" />
              <h3 className="font-semibold text-lg text-gray-800 dark:text-white">
                CPU
              </h3>
            </div>
            <p className="text-gray-600 dark:text-gray-300">
              {edition.requirements.cpu}
            </p>
          </div>
        </motion.div>

        {/* RAM Card */}
        <motion.div
          whileHover={{ scale: 1.05 }}
          className="p-6 rounded-xl bg-gray-50 dark:bg-gray-800/50 relative group overflow-hidden 
                     border border-gray-200 dark:border-gray-700 transition-all"
        >
          <div
            className="absolute inset-0 bg-gradient-to-br from-purple-500/10 to-pink-500/10 
                         opacity-0 group-hover:opacity-100 transition-opacity"
          />
          <div className="relative">
            <div className="flex items-center gap-3 mb-4">
              <Memory className="w-7 h-7 text-purple-500" />
              <h3 className="font-semibold text-lg text-gray-800 dark:text-white">
                RAM
              </h3>
            </div>
            <p className="text-gray-600 dark:text-gray-300">
              {edition.requirements.ram}
            </p>
          </div>
        </motion.div>

        {/* Storage Card */}
        <motion.div
          whileHover={{ scale: 1.05 }}
          className="p-6 rounded-xl bg-gray-50 dark:bg-gray-800/50 relative group overflow-hidden 
                     border border-gray-200 dark:border-gray-700 transition-all"
        >
          <div
            className="absolute inset-0 bg-gradient-to-br from-emerald-500/10 to-teal-500/10 
                         opacity-0 group-hover:opacity-100 transition-opacity"
          />
          <div className="relative">
            <div className="flex items-center gap-3 mb-4">
              <HardDrive className="w-7 h-7 text-emerald-500" />
              <h3 className="font-semibold text-lg text-gray-800 dark:text-white">
                Storage
              </h3>
            </div>
            <p className="text-gray-600 dark:text-gray-300">
              {edition.requirements.storage}
            </p>
          </div>
        </motion.div>
      </div>
    </motion.div>
  );
}
