import { motion } from "framer-motion";
import { Download } from "lucide-react";
import type { Edition, Version } from "../../types";

interface DownloadButtonProps {
  edition: Edition;
  version: Version;
  downloadUrl: string;
}

export default function DownloadButton({
  edition,
  version,
  downloadUrl,
}: DownloadButtonProps) {
  return (
    <motion.a
      href={downloadUrl}
      initial={{ opacity: 0, y: 20 }}
      animate={{ opacity: 1, y: 0 }}
      whileHover={{ scale: 1.02 }}
      whileTap={{ scale: 0.98 }}
      className="block w-full"
    >
      <div className="relative overflow-hidden group">
        <div
          className="w-full inline-flex items-center justify-center gap-3 px-8 py-6 rounded-xl
                     bg-gradient-to-r from-primary-500 to-secondary-500 text-white font-medium text-xl
                     shadow-lg hover:shadow-xl transition-all duration-300"
        >
          <Download className="w-7 h-7 group-hover:animate-bounce" />
          Download {edition.name} Edition {version.version}
          <div
            className="absolute inset-0 bg-gradient-to-r from-white/0 via-white/10 to-white/0 
                       transform translate-x-[-100%] group-hover:translate-x-[100%] transition-transform 
                       duration-1000 ease-in-out"
          />
        </div>
      </div>
    </motion.a>
  );
}
