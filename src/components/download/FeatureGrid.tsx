import { motion } from "framer-motion";
import { Shield, Clock, Globe } from "lucide-react";

const features = [
  {
    icon: Shield,
    title: "Secure by Default",
    description: "Enhanced security features and regular updates",
    color: "from-blue-500 to-indigo-500",
  },
  {
    icon: Clock,
    title: "Fast Boot Time",
    description: "Optimized for quick startup and performance",
    color: "from-purple-500 to-pink-500",
  },
  {
    icon: Globe,
    title: "Global Mirrors",
    description: "Fast downloads from servers worldwide",
    color: "from-emerald-500 to-teal-500",
  },
];

export default function FeatureGrid() {
  return (
    <div className="grid md:grid-cols-3 gap-8 mb-16">
      {features.map((feature, index) => (
        <motion.div
          key={feature.title}
          initial={{ opacity: 0, y: 20 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ delay: index * 0.1 }}
          className="relative group"
        >
          <div className="card h-full relative overflow-hidden">
            {/* Background Glow */}
            <div
              className={`absolute -inset-2 bg-gradient-to-r ${feature.color} rounded-3xl blur-xl 
                          opacity-0 group-hover:opacity-20 transition-opacity duration-500`}
            />

            <div className="relative">
              <div
                className={`w-14 h-14 rounded-2xl bg-gradient-to-br ${feature.color} 
                            flex items-center justify-center mb-6 transform group-hover:scale-110 
                            group-hover:rotate-6 transition-transform duration-300`}
              >
                <feature.icon className="w-7 h-7 text-white" />
              </div>
              <h3 className="text-xl font-semibold mb-3">{feature.title}</h3>
              <p className="text-gray-600 dark:text-gray-300">
                {feature.description}
              </p>
            </div>
          </div>
        </motion.div>
      ))}
    </div>
  );
}
