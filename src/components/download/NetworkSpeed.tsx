import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { Wifi, WifiOff } from "lucide-react";

export default function NetworkSpeed() {
  const [speed, setSpeed] = useState<number | null>(null);
  const [isOnline, setIsOnline] = useState(navigator.onLine);
  const [isLoading, setIsLoading] = useState(false);

  // Handle online/offline status
  useEffect(() => {
    const handleOnline = () => setIsOnline(true);
    const handleOffline = () => setIsOnline(false);

    window.addEventListener("online", handleOnline);
    window.addEventListener("offline", handleOffline);

    return () => {
      window.removeEventListener("online", handleOnline);
      window.removeEventListener("offline", handleOffline);
    };
  }, []);

  // Measure network speed
  useEffect(() => {
    let isMounted = true;

    const measureSpeed = async () => {
      if (!isOnline) {
        setSpeed(null);
        setIsLoading(false);
        return;
      }

      try {
        setIsLoading(true);

        // Use a larger file for more accurate speed measurement
        const url = "https://speed.cloudflare.com/__down?bytes=1000000"; // 1MB file
        const startTime = Date.now();
        const response = await fetch(url, { cache: "no-store" });
        const blob = await response.blob();
        const endTime = Date.now();

        const duration = (endTime - startTime) / 1000; // Convert to seconds
        const bytes = blob.size;
        const bitsPerSecond = (bytes * 8) / duration; // Convert bytes to bits
        const megabitsPerSecond = bitsPerSecond / 1_000_000; // Convert to Mbps

        if (isMounted) {
          setSpeed(megabitsPerSecond);
          setIsLoading(false);
        }
      } catch (error) {
        if (isMounted) {
          setSpeed(null);
          setIsLoading(false);
        }
      }
    };

    measureSpeed(); // Initial measurement
    const interval = setInterval(measureSpeed, 5000); // Update every 5 seconds

    return () => {
      isMounted = false;
      clearInterval(interval);
    };
  }, [isOnline]);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className="flex items-center gap-3 bg-white dark:bg-gray-800 rounded-xl p-4 shadow-lg"
    >
      <div
        className={`p-2 rounded-lg ${
          isOnline
            ? "bg-green-100 dark:bg-green-900/30"
            : "bg-red-100 dark:bg-red-900/30"
        }`}
      >
        {isOnline ? (
          <Wifi
            className={`w-5 h-5 ${
              isLoading ? "text-gray-400 animate-pulse" : "text-green-500"
            }`}
          />
        ) : (
          <WifiOff className="w-5 h-5 text-red-500" />
        )}
      </div>
      <div>
        <p className="text-sm font-medium text-gray-600 dark:text-gray-300">
          Network Speed
        </p>
        <p className="text-lg font-semibold">
          {isOnline ? (
            isLoading ? (
              <span className="text-gray-400">Testing...</span>
            ) : (
              <span className="text-green-500">
                {speed !== null ? `${speed.toFixed(1)} Mbps` : "N/A"}
              </span>
            )
          ) : (
            <span className="text-red-500">Offline</span>
          )}
        </p>
      </div>
    </motion.div>
  );
}
