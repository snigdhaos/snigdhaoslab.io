import { Calendar, HardDrive } from "lucide-react";
import type { Version } from "../../types";

interface VersionSelectorProps {
  versions: Version[];
  selectedVersion: Version;
  onSelect: (version: Version) => void;
}

export default function VersionSelector({
  versions,
  selectedVersion,
  onSelect,
}: VersionSelectorProps) {
  return (
    <div className="grid md:grid-cols-2 gap-4">
      {versions.map((version) => (
        <button
          key={version.version}
          onClick={() => onSelect(version)}
          className={`p-6 rounded-xl border-2 transition-colors duration-200 relative overflow-hidden ${
            selectedVersion.version === version.version
              ? "border-primary-500 bg-primary-50 dark:bg-primary-900/20"
              : "border-gray-200 dark:border-gray-700 hover:border-primary-300"
          }`}
        >
          <div className="flex items-start justify-between relative">
            <div>
              <h3 className="text-lg font-semibold mb-3">
                Version {version.version}
              </h3>
              <div className="flex flex-col gap-2 text-sm text-gray-600 dark:text-gray-300">
                <div className="flex items-center gap-2">
                  <Calendar className="w-4 h-4 text-primary-500" />
                  {version.releaseDate}
                </div>
                <div className="flex items-center gap-2">
                  <HardDrive className="w-4 h-4 text-secondary-500" />
                  {version.size}
                </div>
              </div>
            </div>
            {version.isLatest && (
              <span className="px-3 py-1 text-xs font-medium text-green-500 bg-green-50 dark:bg-green-900/20 rounded-full">
                Latest
              </span>
            )}
          </div>
        </button>
      ))}
    </div>
  );
}
