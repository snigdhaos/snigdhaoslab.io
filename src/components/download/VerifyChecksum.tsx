import { motion } from "framer-motion";
import { Shield } from "lucide-react";

interface VerifyChecksumProps {
  checksum: string;
}

export default function VerifyChecksum({ checksum }: VerifyChecksumProps) {
  return (
    <motion.div
      initial={{ opacity: 0, y: 20 }}
      animate={{ opacity: 1, y: 0 }}
      className="card"
    >
      <div className="flex items-center gap-3 mb-4">
        <div className="p-2 rounded-lg bg-purple-100 dark:bg-purple-900/30">
          <Shield className="w-6 h-6 text-purple-500" />
        </div>
        <h3 className="text-xl font-semibold">Verify Checksum</h3>
      </div>
      <div className="bg-gray-50 dark:bg-gray-800/50 p-6 rounded-xl font-mono text-sm break-all relative group overflow-hidden">
        <div
          className="absolute inset-0 bg-gradient-to-br from-purple-500/5 to-pink-500/5 
                     opacity-0 group-hover:opacity-100 transition-opacity"
        />
        <div className="relative">{checksum}</div>
      </div>
    </motion.div>
  );
}
