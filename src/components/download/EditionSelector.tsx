import {
  Monitor,
  Terminal,
  Code,
  Server,
  Zap,
  Package,
  Users,
  Shield,
  Github as Git,
  Cloud,
  Settings,
} from "lucide-react";
import { motion } from "framer-motion";
import type { Edition } from "../../types";

const editions = {
  Desktop: {
    icon: Monitor,
    gradient: {
      from: "#3B82F6",
      to: "#2563EB",
      shadow: "rgba(59, 130, 246, 0.5)",
    },
    features: ["Full Desktop Environment", "Office Suite", "Multimedia Apps"],
  },
  Developer: {
    icon: Code,
    gradient: {
      from: "#8B5CF6",
      to: "#6D28D9",
      shadow: "rgba(139, 92, 246, 0.5)",
    },
    features: ["Development Tools", "Docker Support", "Version Control"],
  },
  Server: {
    icon: Server,
    gradient: {
      from: "#10B981",
      to: "#059669",
      shadow: "rgba(16, 185, 129, 0.5)",
    },
    features: ["Server Optimized", "Container Support", "Cloud Ready"],
  },
  Minimal: {
    icon: Terminal,
    gradient: {
      from: "#F59E0B",
      to: "#D97706",
      shadow: "rgba(245, 158, 11, 0.5)",
    },
    features: ["Lightweight", "Core Utilities", "Customizable"],
  },
};

const featureIcons = {
  "Full Desktop Environment": Monitor,
  "Office Suite": Package,
  "Multimedia Apps": Users,
  "Development Tools": Code,
  "Docker Support": Server,
  "Version Control": Git,
  "Server Optimized": Zap,
  "Container Support": Package,
  "Cloud Ready": Cloud,
  Lightweight: Zap,
  "Core Utilities": Terminal,
  Customizable: Settings,
  "Security Features": Shield,
};

interface EditionSelectorProps {
  editions: Edition[];
  selectedEdition: Edition;
  onSelect: (edition: Edition) => void;
}

export default function EditionSelector({
  editions: editionsList,
  selectedEdition,
  onSelect,
}: EditionSelectorProps) {
  return (
    <div className="grid md:grid-cols-2 lg:grid-cols-4 gap-6">
      {editionsList.map((edition) => {
        const config = editions[edition.name as keyof typeof editions];
        const Icon = config.icon;

        return (
          <motion.button
            key={edition.name}
            onClick={() => onSelect(edition)}
            whileHover={{ scale: 1.02 }}
            whileTap={{ scale: 0.98 }}
            className={`relative overflow-hidden group ${
              selectedEdition.name === edition.name
                ? "ring-2 ring-primary-500 dark:ring-primary-400"
                : "ring-1 ring-gray-200 dark:ring-gray-700"
            }`}
          >
            {/* Card */}
            <div className="relative p-6 rounded-xl bg-white dark:bg-gray-800">
              {/* Background Gradient Effect */}
              <div
                className={`absolute inset-0 bg-gradient-to-br opacity-0 group-hover:opacity-5 
                           transition-opacity duration-300`}
                style={{
                  background: `linear-gradient(135deg, ${config.gradient.from}, ${config.gradient.to})`,
                }}
              />

              {/* Selected Indicator */}
              {selectedEdition.name === edition.name && (
                <motion.div
                  layoutId="selected-edition"
                  className="absolute inset-0 border-2 border-primary-500 dark:border-primary-400 rounded-xl"
                  initial={false}
                  transition={{ type: "spring", stiffness: 300, damping: 30 }}
                />
              )}

              {/* Content */}
              <div className="relative">
                {/* Icon */}
                <div className="relative mb-6 inline-block">
                  <div
                    className="w-16 h-16 rounded-2xl flex items-center justify-center transform 
                             group-hover:scale-110 group-hover:rotate-6 transition-transform duration-300"
                    style={{
                      background: `linear-gradient(135deg, ${config.gradient.from}, ${config.gradient.to})`,
                      boxShadow: `0 8px 16px -4px ${config.gradient.shadow}`,
                    }}
                  >
                    <Icon className="w-8 h-8 text-white" />
                  </div>
                  <div
                    className="absolute inset-0 rounded-2xl blur-xl opacity-40"
                    style={{
                      background: `linear-gradient(135deg, ${config.gradient.from}, ${config.gradient.to})`,
                    }}
                  />
                </div>

                {/* Title & Description */}
                <div className="mb-4">
                  <h3 className="text-xl font-semibold mb-2">{edition.name}</h3>
                  <p className="text-sm text-gray-600 dark:text-gray-300 min-h-[40px]">
                    {edition.description}
                  </p>
                </div>

                {/* Features */}
                <div className="space-y-2">
                  {config.features.map((feature) => {
                    const FeatureIcon =
                      featureIcons[feature as keyof typeof featureIcons];
                    return (
                      <div
                        key={feature}
                        className="flex items-center gap-2 text-sm"
                      >
                        <div className="p-1 rounded-lg bg-gray-100 dark:bg-gray-700">
                          <FeatureIcon className="w-4 h-4 text-gray-600 dark:text-gray-300" />
                        </div>
                        <span className="text-gray-600 dark:text-gray-300">
                          {feature}
                        </span>
                      </div>
                    );
                  })}
                </div>

                {/* Size Badge */}
                <div className="absolute top-0 right-0">
                  <div
                    className="px-3 py-1 text-sm font-medium rounded-full"
                    style={{
                      background: `linear-gradient(135deg, ${config.gradient.from}, ${config.gradient.to})`,
                      color: "white",
                    }}
                  >
                    {edition.size}
                  </div>
                </div>
              </div>
            </div>
          </motion.button>
        );
      })}
    </div>
  );
}
