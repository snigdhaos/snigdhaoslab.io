import { motion } from "framer-motion";
import { useState } from "react";
import {
  Cpu,
  MemoryStick as Memory,
  HardDrive,
  Check,
  X,
  AlertCircle,
} from "lucide-react";
import type { Edition } from "../../types";

interface HardwareCheckerProps {
  edition: Edition;
}

interface SystemSpecs {
  cpu: {
    cores: number;
    speed: number;
  };
  ram: number;
  storage: number;
}

function parseRequirements(edition: Edition) {
  return {
    cpu: {
      cores: parseInt(
        edition.requirements.cpu.match(/(\d+).*?Core/i)?.[1] || "1",
      ),
      speed: parseFloat(
        edition.requirements.cpu.match(/(\d+(?:\.\d+)?).*?GHz/i)?.[1] || "1",
      ),
    },
    ram: parseInt(edition.requirements.ram.match(/(\d+).*?GB/i)?.[1] || "1"),
    storage: parseInt(
      edition.requirements.storage.match(/(\d+).*?GB/i)?.[1] || "1",
    ),
  };
}

export default function HardwareChecker({ edition }: HardwareCheckerProps) {
  const [systemSpecs, setSystemSpecs] = useState<SystemSpecs | null>(null);
  const [checking, setChecking] = useState(false);
  const requirements = parseRequirements(edition);

  const checkHardware = async () => {
    setChecking(true);

    try {
      // Detect CPU cores
      const cores = navigator.hardwareConcurrency || 4;

      // Detect RAM (Chrome-only)
      const ram = (performance as any).memory
        ? Math.round(
            (performance as any).memory.jsHeapSizeLimit / (1024 * 1024 * 1024),
          )
        : 16; // Fallback value

      // Simulate storage detection (not available in browsers)
      const storage = 500; // Placeholder value

      // Simulate CPU speed (not available in browsers)
      const speed = 2.5; // Placeholder value

      const specs: SystemSpecs = {
        cpu: {
          cores,
          speed,
        },
        ram,
        storage,
      };

      setSystemSpecs(specs);
    } catch (error) {
      console.error("Failed to detect hardware:", error);
    } finally {
      setChecking(false);
    }
  };

  const getCompatibilityStatus = (type: "cpu" | "ram" | "storage") => {
    if (!systemSpecs) return "unknown";

    switch (type) {
      case "cpu":
        return (
          systemSpecs.cpu.cores >= requirements.cpu.cores &&
          systemSpecs.cpu.speed >= requirements.cpu.speed
        );
      case "ram":
        return systemSpecs.ram >= requirements.ram;
      case "storage":
        return systemSpecs.storage >= requirements.storage;
      default:
        return "unknown";
    }
  };

  const StatusIcon = ({ status }: { status: boolean | "unknown" }) => {
    if (status === "unknown")
      return <AlertCircle className="w-5 h-5 text-gray-400" />;
    return status ? (
      <Check className="w-5 h-5 text-green-500" />
    ) : (
      <X className="w-5 h-5 text-red-500" />
    );
  };

  return (
    <motion.div
      initial={{ opacity: 0, y: 20 }}
      animate={{ opacity: 1, y: 0 }}
      className="card p-6 bg-white dark:bg-gray-900 rounded-xl shadow-lg"
    >
      <div className="flex items-center gap-3 mb-8">
        <div className="p-3 rounded-lg bg-violet-100 dark:bg-violet-900/30">
          <Cpu className="w-7 h-7 text-violet-500" />
        </div>
        <h2 className="text-2xl font-bold text-gray-800 dark:text-white">
          Hardware Compatibility
        </h2>
      </div>

      <div className="space-y-6">
        {/* CPU Check */}
        <div className="p-4 rounded-xl bg-gray-50 dark:bg-gray-800/50 relative overflow-hidden group">
          <div
            className="absolute inset-0 bg-gradient-to-r from-blue-500/5 to-indigo-500/5 
                       opacity-0 group-hover:opacity-100 transition-opacity"
          />
          <div className="relative">
            <div className="flex items-center justify-between mb-4">
              <div className="flex items-center gap-3">
                <div className="p-2 rounded-lg bg-blue-100 dark:bg-blue-900/30">
                  <Cpu className="w-5 h-5 text-blue-500" />
                </div>
                <div>
                  <h3 className="font-medium">CPU</h3>
                  <p className="text-sm text-gray-500 dark:text-gray-400">
                    Required: {edition.requirements.cpu}
                  </p>
                </div>
              </div>
              <StatusIcon status={getCompatibilityStatus("cpu")} />
            </div>
            {systemSpecs && (
              <div className="text-sm text-gray-600 dark:text-gray-300">
                Detected: {systemSpecs.cpu.cores} Cores @{" "}
                {systemSpecs.cpu.speed.toFixed(1)} GHz
              </div>
            )}
          </div>
        </div>

        {/* RAM Check */}
        <div className="p-4 rounded-xl bg-gray-50 dark:bg-gray-800/50 relative overflow-hidden group">
          <div
            className="absolute inset-0 bg-gradient-to-r from-purple-500/5 to-pink-500/5 
                       opacity-0 group-hover:opacity-100 transition-opacity"
          />
          <div className="relative">
            <div className="flex items-center justify-between mb-4">
              <div className="flex items-center gap-3">
                <div className="p-2 rounded-lg bg-purple-100 dark:bg-purple-900/30">
                  <Memory className="w-5 h-5 text-purple-500" />
                </div>
                <div>
                  <h3 className="font-medium">Memory</h3>
                  <p className="text-sm text-gray-500 dark:text-gray-400">
                    Required: {edition.requirements.ram}
                  </p>
                </div>
              </div>
              <StatusIcon status={getCompatibilityStatus("ram")} />
            </div>
            {systemSpecs && (
              <div className="text-sm text-gray-600 dark:text-gray-300">
                Detected: {systemSpecs.ram} GB
              </div>
            )}
          </div>
        </div>

        {/* Storage Check */}
        <div className="p-4 rounded-xl bg-gray-50 dark:bg-gray-800/50 relative overflow-hidden group">
          <div
            className="absolute inset-0 bg-gradient-to-r from-emerald-500/5 to-teal-500/5 
                       opacity-0 group-hover:opacity-100 transition-opacity"
          />
          <div className="relative">
            <div className="flex items-center justify-between mb-4">
              <div className="flex items-center gap-3">
                <div className="p-2 rounded-lg bg-emerald-100 dark:bg-emerald-900/30">
                  <HardDrive className="w-5 h-5 text-emerald-500" />
                </div>
                <div>
                  <h3 className="font-medium">Storage</h3>
                  <p className="text-sm text-gray-500 dark:text-gray-400">
                    Required: {edition.requirements.storage}
                  </p>
                </div>
              </div>
              <StatusIcon status={getCompatibilityStatus("storage")} />
            </div>
            {systemSpecs && (
              <div className="text-sm text-gray-600 dark:text-gray-300">
                Detected: {systemSpecs.storage} GB
              </div>
            )}
          </div>
        </div>

        {/* Check Button */}
        <motion.button
          whileTap={{ scale: 0.95 }}
          onClick={checkHardware}
          disabled={checking}
          className="w-full px-6 py-3 bg-gradient-to-r from-primary-500 to-secondary-500 text-white 
                   rounded-xl font-medium hover:from-primary-600 hover:to-secondary-600 
                   disabled:opacity-50 disabled:cursor-not-allowed transition-all duration-200"
        >
          {checking ? (
            <span className="flex items-center justify-center gap-2">
              <svg className="animate-spin h-5 w-5" viewBox="0 0 24 24">
                <circle
                  className="opacity-25"
                  cx="12"
                  cy="12"
                  r="10"
                  stroke="currentColor"
                  strokeWidth="4"
                />
                <path
                  className="opacity-75"
                  fill="currentColor"
                  d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                />
              </svg>
              Checking Hardware...
            </span>
          ) : (
            "Check Hardware Compatibility"
          )}
        </motion.button>
      </div>
    </motion.div>
  );
}
