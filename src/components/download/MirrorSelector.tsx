import { motion } from "framer-motion";
import { Globe, Wifi, Server, ArrowRight } from "lucide-react";
import type { Mirror } from "../../types";

interface MirrorSelectorProps {
  mirrors: Mirror[];
  selectedMirror: Mirror;
  onSelect: (mirror: Mirror) => void;
}

export default function MirrorSelector({
  mirrors,
  selectedMirror,
  onSelect,
}: MirrorSelectorProps) {
  return (
    <div className="space-y-6">
      <div className="grid md:grid-cols-3 gap-4 mb-6">
        {mirrors.map((mirror) => (
          <motion.button
            key={mirror.url}
            onClick={() => onSelect(mirror)}
            whileHover={{ scale: 1.02 }}
            whileTap={{ scale: 0.98 }}
            className={`relative group overflow-hidden ${
              selectedMirror.url === mirror.url
                ? "ring-2 ring-primary-500"
                : "hover:ring-1 hover:ring-gray-200 dark:hover:ring-gray-700"
            }`}
          >
            <div className="relative p-6 rounded-xl bg-white dark:bg-gray-800">
              {/* Background Gradient Effect */}
              <div
                className="absolute inset-0 bg-gradient-to-br from-primary-500/5 via-transparent to-secondary-500/5 
                           opacity-0 group-hover:opacity-100 transition-opacity duration-500"
              />

              {/* Selected Indicator */}
              {selectedMirror.url === mirror.url && (
                <motion.div
                  layoutId="selected-mirror"
                  className="absolute inset-0 border-2 border-primary-500 rounded-xl"
                  initial={false}
                  transition={{ type: "spring", stiffness: 300, damping: 30 }}
                />
              )}

              <div className="relative">
                {/* Header */}
                <div className="flex items-center gap-4 mb-4">
                  <div
                    className="p-3 rounded-xl bg-gradient-to-br from-primary-500 to-secondary-500 
                               transform group-hover:scale-110 group-hover:rotate-6 transition-transform duration-300"
                  >
                    <Server className="w-6 h-6 text-white" />
                  </div>
                  <div>
                    <h3 className="text-lg font-semibold">{mirror.name}</h3>
                    <div className="flex items-center gap-2 text-sm text-gray-500 dark:text-gray-400">
                      <Globe className="w-4 h-4" />
                      {mirror.location}
                    </div>
                  </div>
                </div>

                {/* Speed Indicator */}
                <div className="space-y-2">
                  <div className="flex items-center justify-between text-sm">
                    <div className="flex items-center gap-2">
                      <Wifi className="w-4 h-4 text-primary-500" />
                      <span>Download Speed</span>
                    </div>
                    <span className="font-medium text-primary-500">
                      {mirror.speed.toFixed(1)} MB/s
                    </span>
                  </div>
                  <div className="h-2 bg-gray-100 dark:bg-gray-700 rounded-full overflow-hidden">
                    <motion.div
                      initial={{ width: 0 }}
                      animate={{
                        width: `${(mirror.speed / Math.max(...mirrors.map((m) => m.speed))) * 100}%`,
                      }}
                      transition={{ duration: 1, ease: "easeOut" }}
                      className="h-full bg-gradient-to-r from-primary-500 to-secondary-500 rounded-full"
                    />
                  </div>
                </div>

                {/* Select Button */}
                <div
                  className={`mt-4 flex items-center justify-between text-sm font-medium ${
                    selectedMirror.url === mirror.url
                      ? "text-primary-500"
                      : "text-gray-500 dark:text-gray-400 group-hover:text-primary-500"
                  }`}
                >
                  <span>Select Mirror</span>
                  <ArrowRight className="w-4 h-4 transform group-hover:translate-x-1 transition-transform" />
                </div>
              </div>
            </div>
          </motion.button>
        ))}
      </div>

      {/* Selected Mirror Details */}
      {selectedMirror && (
        <motion.div
          initial={{ opacity: 0, y: 10 }}
          animate={{ opacity: 1, y: 0 }}
          className="p-4 rounded-xl bg-gray-50 dark:bg-gray-800/50 border border-gray-200 dark:border-gray-700"
        >
          <div className="flex items-center gap-3 text-sm text-gray-600 dark:text-gray-300">
            <Server className="w-4 h-4 text-primary-500" />
            <span>Selected Mirror URL:</span>
            <code className="flex-1 px-3 py-1 bg-white dark:bg-gray-800 rounded-lg font-mono text-xs">
              {selectedMirror.url}
            </code>
          </div>
        </motion.div>
      )}
    </div>
  );
}
