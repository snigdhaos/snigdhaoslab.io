import { motion } from "framer-motion";
import { Link } from "react-router-dom";
import {
  Code,
  Download,
  Users,
  Heart,
  Book,
  Star,
  Coffee,
  Shield,
} from "lucide-react";

const links = [
  {
    title: "Product",
    items: [
      { name: "Features", href: "/about", icon: Star },
      { name: "Download", href: "/download", icon: Download },
      { name: "Documentation", href: "/docs", icon: Book },
      { name: "Security", href: "/security", icon: Shield },
    ],
  },
  {
    title: "Community",
    items: [
      { name: "Developers", href: "/developers", icon: Code },
      { name: "Contributors", href: "/developers#contributors", icon: Users },
      { name: "Donate", href: "/donate", icon: Heart },
      { name: "Blog", href: "/blog", icon: Coffee },
    ],
  },
];

export default function FooterLinks() {
  return (
    <div className="grid md:grid-cols-2 gap-8">
      {links.map((group) => (
        <div key={group.title}>
          <h3 className="text-lg font-semibold mb-4 bg-gradient-to-r from-primary-500 to-secondary-500 bg-clip-text text-transparent">
            {group.title}
          </h3>
          <ul className="space-y-3">
            {group.items.map((item) => (
              <li key={item.name}>
                <Link
                  to={item.href}
                  className="flex items-center gap-2 text-gray-600 dark:text-gray-400 
                           hover:text-primary-500 dark:hover:text-primary-400 transition-colors group"
                >
                  <motion.div
                    whileHover={{ scale: 1.1, rotate: 5 }}
                    className="p-1 rounded-lg bg-gray-100 dark:bg-gray-800 group-hover:bg-primary-100 
                             dark:group-hover:bg-primary-900/30 transition-colors"
                  >
                    <item.icon className="w-4 h-4" />
                  </motion.div>
                  <span>{item.name}</span>
                </Link>
              </li>
            ))}
          </ul>
        </div>
      ))}
    </div>
  );
}
