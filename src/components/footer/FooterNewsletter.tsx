import { motion } from "framer-motion";
import { Send, CheckCircle, XCircle } from "lucide-react";
import { useState } from "react";
import emailjs from "@emailjs/browser";

export default function FooterNewsletter() {
  const [email, setEmail] = useState("");
  const [status, setStatus] = useState<
    "idle" | "loading" | "success" | "error"
  >("idle");

  // EmailJS Configuration
  const SERVICE_ID = "YOUR_SERVICE_ID"; // Replace with your EmailJS Service ID
  const TEMPLATE_ID = "YOUR_TEMPLATE_ID"; // Replace with your EmailJS Template ID
  const USER_ID = "YOUR_USER_ID"; // Replace with your EmailJS User ID

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    setStatus("loading");

    try {
      // Send email using EmailJS
      await emailjs.send(
        SERVICE_ID,
        TEMPLATE_ID,
        {
          to_email: email, // Pass the email as a parameter to your template
        },
        USER_ID,
      );

      // On success
      setStatus("success");
      setEmail("");
      setTimeout(() => setStatus("idle"), 3000); // Reset status after 3 seconds
    } catch (error) {
      console.error("Failed to send email:", error);
      setStatus("error");
      setTimeout(() => setStatus("idle"), 3000); // Reset status after 3 seconds
    }
  };

  return (
    <div>
      <h3 className="text-lg font-semibold mb-4 bg-gradient-to-r from-primary-500 to-secondary-500 bg-clip-text text-transparent">
        Stay Updated
      </h3>
      <p className="text-gray-600 dark:text-gray-400 mb-6 leading-relaxed">
        Subscribe to our newsletter for the latest updates, releases, and
        community news. No spam, unsubscribe at any time.
      </p>
      <form onSubmit={handleSubmit} className="relative">
        <div className="relative">
          <input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Enter your email"
            className="w-full px-4 py-3 rounded-xl bg-white dark:bg-gray-800 border border-gray-200 
                     dark:border-gray-700 focus:ring-2 focus:ring-primary-500 focus:border-transparent
                     transition-all duration-200 pr-12"
            required
          />
          <motion.button
            whileTap={{ scale: 0.95 }}
            type="submit"
            disabled={status === "loading"}
            className="absolute right-2 top-1/2 -translate-y-1/2 p-2 rounded-lg bg-gradient-to-r 
                     from-primary-500 to-secondary-500 text-white hover:from-primary-600 
                     hover:to-secondary-600 transition-colors disabled:opacity-50"
          >
            {status === "loading" ? (
              <svg
                className="w-5 h-5 animate-spin"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <circle
                  className="opacity-25"
                  cx="12"
                  cy="12"
                  r="10"
                  stroke="currentColor"
                  strokeWidth="4"
                />
                <path
                  className="opacity-75"
                  fill="currentColor"
                  d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                />
              </svg>
            ) : (
              <Send className="w-5 h-5" />
            )}
          </motion.button>
        </div>

        {/* Status Messages */}
        {status === "success" && (
          <motion.div
            initial={{ opacity: 0, y: 10 }}
            animate={{ opacity: 1, y: 0 }}
            className="absolute -bottom-8 left-0 flex items-center gap-2 text-sm text-green-500"
          >
            <CheckCircle className="w-4 h-4" />
            <span>Thanks for subscribing!</span>
          </motion.div>
        )}
        {status === "error" && (
          <motion.div
            initial={{ opacity: 0, y: 10 }}
            animate={{ opacity: 1, y: 0 }}
            className="absolute -bottom-8 left-0 flex items-center gap-2 text-sm text-red-500"
          >
            <XCircle className="w-4 h-4" />
            <span>Something went wrong. Please try again.</span>
          </motion.div>
        )}
      </form>
    </div>
  );
}
