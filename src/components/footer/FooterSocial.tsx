import { motion } from "framer-motion";
import { Twitter, Mail, Globe } from "lucide-react";

const socials = [
  {
    name: "GitLab",
    href: "https://gitlab.com/snigdhaos",
    icon: ({ className }: { className?: string }) => (
      <svg className={className} viewBox="0 0 24 24" fill="currentColor">
        <path d="M22.65 14.39L12 22.13 1.35 14.39a.84.84 0 0 1-.3-.94l1.22-3.78 2.44-7.51A.42.42 0 0 1 4.82 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.49h8.1l2.44-7.51A.42.42 0 0 1 18.6 2a.43.43 0 0 1 .58 0 .42.42 0 0 1 .11.18l2.44 7.51L23 13.45a.84.84 0 0 1-.35.94z" />
      </svg>
    ),
    color: "hover:text-[#FC6D26]",
    bgColor: "hover:bg-[#FC6D26]/10",
  },
  {
    name: "Twitter",
    href: "https://twitter.com/snigdhaos",
    icon: Twitter,
    color: "hover:text-[#1DA1F2]",
    bgColor: "hover:bg-[#1DA1F2]/10",
  },
  {
    name: "Telegram",
    href: "https://t.me/snigdhaos",
    icon: ({ className }: { className?: string }) => (
      <svg className={className} viewBox="0 0 24 24" fill="currentColor">
        <path d="M23.1117 4.49449C23.4296 2.94472 21.9074 1.65683 20.4317 2.227L2.3425 9.21601C0.694517 9.85273 0.621087 12.1572 2.22518 12.8975L6.1645 14.7157L8.03849 21.2746C8.13583 21.6153 8.40618 21.8791 8.74917 21.968C9.09216 22.0568 9.45658 21.9576 9.70712 21.707L12.5938 18.8203L16.6375 21.8531C17.8113 22.7334 19.5019 22.0922 19.7967 20.6549L23.1117 4.49449ZM3.0633 11.0816L21.1525 4.0926L17.8375 20.2531L13.1 16.6999C12.7019 16.4013 12.1448 16.4409 11.7929 16.7928L10.5565 18.0292L10.928 15.9861L18.2071 8.70703C18.5614 8.35278 18.5988 7.79106 18.2947 7.39293C17.9906 6.99479 17.4389 6.88312 17.0039 7.13168L6.95124 12.876L3.0633 11.0816ZM8.17695 14.4791L8.78333 16.6015L9.01614 15.321C9.05253 15.1209 9.14908 14.9366 9.29291 14.7928L11.5128 12.573L8.17695 14.4791Z" />
      </svg>
    ),
    color: "hover:text-[#26A5E4]",
    bgColor: "hover:bg-[#26A5E4]/10",
  },
  {
    name: "Email",
    href: "mailto:contact@snigdha-os.org",
    icon: Mail,
    color: "hover:text-[#EA4335]",
    bgColor: "hover:bg-[#EA4335]/10",
  },
  {
    name: "Website",
    href: "https://snigdha-os.org",
    icon: Globe,
    color: "hover:text-[#0ACF83]",
    bgColor: "hover:bg-[#0ACF83]/10",
  },
];

export default function FooterSocial() {
  return (
    <div className="flex flex-wrap items-center gap-4">
      {socials.map((social) => (
        <motion.a
          key={social.name}
          href={social.href}
          target="_blank"
          rel="noopener noreferrer"
          whileHover={{ scale: 1.1, rotate: 5 }}
          className={`p-3 rounded-xl transition-colors ${social.color} ${social.bgColor} 
                   bg-gray-100 dark:bg-gray-800 group relative overflow-hidden`}
          aria-label={social.name}
        >
          <div
            className="absolute inset-0 bg-gradient-to-r from-primary-500/10 to-secondary-500/10 
                       opacity-0 group-hover:opacity-100 transition-opacity"
          />
          <social.icon className="w-5 h-5 relative z-10" />
        </motion.a>
      ))}
    </div>
  );
}
