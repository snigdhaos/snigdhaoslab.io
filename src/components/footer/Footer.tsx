import { Heart } from "lucide-react";
import FooterLinks from "./FooterLinks";
import FooterSocial from "./FooterSocial";
import FooterNewsletter from "./FooterNewsletter";
import Logo from "../layout/Logo";

export default function Footer() {
  return (
    <footer className="relative overflow-hidden">
      {/* Background Elements */}
      <div className="absolute inset-0 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900" />
      <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
      <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />

      {/* Main Footer */}
      <div className="relative border-t border-gray-200 dark:border-gray-800">
        <div className="section-container py-16">
          <div className="grid lg:grid-cols-12 gap-12 lg:gap-8">
            {/* Brand and Description */}
            <div className="lg:col-span-4">
              <div className="mb-6">
                <Logo />
              </div>
              <p className="text-gray-600 dark:text-gray-400 mb-8 leading-relaxed">
                Experience the next generation of Linux with unmatched
                performance, security, and elegance. Built for developers and
                power users who demand the best.
              </p>
              <FooterSocial />
            </div>

            {/* Links and Newsletter */}
            <div className="lg:col-span-8">
              <div className="grid md:grid-cols-2 gap-12">
                <FooterLinks />
                <FooterNewsletter />
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Bottom Bar */}
      <div className="relative border-t border-gray-200 dark:border-gray-800 bg-white/50 dark:bg-gray-900/50 backdrop-blur-sm">
        <div className="section-container py-6">
          <div className="flex flex-col md:flex-row justify-between items-center gap-4">
            <div className="flex items-center gap-2 text-sm text-gray-600 dark:text-gray-400">
              <span>© {new Date().getFullYear()} Snigdha OS.</span>
              <span className="hidden md:inline">•</span>
              <span className="flex items-center gap-1">
                Made with <Heart className="w-4 h-4 text-red-500" /> by
                <a
                  href="https://snigdhaos.org"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="text-primary-500 hover:text-primary-600 dark:text-primary-400 dark:hover:text-primary-300"
                >
                  Snigdha OS Team
                </a>
              </span>
              <span className="hidden md:inline">•</span>
              <span>
                Powered by{" "}
                <a
                  href="https://tivision.gitlab.io"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="text-primary-500 hover:text-primary-600 dark:text-primary-400 dark:hover:text-primary-300"
                >
                  Tonmoy Infrastructure (TiVision)
                </a>
              </span>
            </div>
            <div className="flex items-center gap-6 text-sm">
              <a
                href="/privacy"
                className="text-gray-600 dark:text-gray-400 hover:text-primary-500 dark:hover:text-primary-400 transition-colors"
              >
                Privacy Policy
              </a>
              <a
                href="/terms"
                className="text-gray-600 dark:text-gray-400 hover:text-primary-500 dark:hover:text-primary-400 transition-colors"
              >
                Terms of Service
              </a>
              <a
                href="/sitemap"
                className="text-gray-600 dark:text-gray-400 hover:text-primary-500 dark:hover:text-primary-400 transition-colors"
              >
                Sitemap
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}