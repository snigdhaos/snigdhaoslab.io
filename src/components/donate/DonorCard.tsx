import { motion } from "framer-motion";
import { Github, Twitter, Calendar, Award, ExternalLink } from "lucide-react";
import type { Donor } from "../../types";

interface DonorCardProps {
  donor: Donor;
}

export default function DonorCard({ donor }: DonorCardProps) {
  return (
    <motion.div
      initial={{ opacity: 0, y: 20 }}
      animate={{ opacity: 1, y: 0 }}
      className="group relative"
    >
      <div
        className="bg-white dark:bg-gray-800 rounded-2xl p-6 shadow-lg hover:shadow-xl 
                   transition-all duration-300 relative overflow-hidden"
      >
        {/* Background Gradient Effect */}
        <div
          className="absolute inset-0 bg-gradient-to-br from-primary-500/5 via-transparent to-secondary-500/5 
                     opacity-0 group-hover:opacity-100 transition-opacity duration-500"
        />

        <div className="relative">
          {/* Header */}
          <div className="flex items-start gap-4 mb-6">
            <img
              src={donor.avatar}
              alt={donor.name}
              className="w-16 h-16 rounded-2xl object-cover transform group-hover:scale-105 
                     group-hover:rotate-3 transition-transform duration-300"
            />
            <div>
              <h3 className="text-xl font-semibold mb-1 group-hover:text-primary-500 transition-colors">
                {donor.name}
              </h3>
              <div className="flex items-center gap-2 text-sm text-gray-500 dark:text-gray-400">
                <Calendar className="w-4 h-4" />
                <span>
                  Donor since {new Date(donor.joinDate).toLocaleDateString()}
                </span>
              </div>
            </div>
          </div>

          {/* Donation Info */}
          <div className="mb-4">
            <div
              className="inline-flex items-center px-3 py-1 rounded-full bg-primary-100 dark:bg-primary-900/30 
                         text-primary-600 dark:text-primary-400 text-sm font-medium"
            >
              {donor.tier} • ${donor.amount}
            </div>
          </div>

          {/* Testimonial */}
          {donor.testimonial && (
            <blockquote className="mb-6 text-gray-600 dark:text-gray-300 italic">
              "{donor.testimonial}"
            </blockquote>
          )}

          {/* Badges */}
          <div className="flex flex-wrap gap-2 mb-6">
            {donor.badges.map((badge) => (
              <div
                key={badge}
                className="flex items-center gap-1 px-2 py-1 rounded-lg bg-secondary-100 dark:bg-secondary-900/30 
                         text-secondary-600 dark:text-secondary-400 text-xs font-medium"
              >
                <Award className="w-3 h-3" />
                {badge}
              </div>
            ))}
          </div>

          {/* Social Links */}
          {donor.social && (
            <div className="flex gap-3">
              {donor.social.github && (
                <a
                  href={`https://github.com/${donor.social.github}`}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="p-2 rounded-lg bg-gray-100 dark:bg-gray-700 hover:bg-gray-200 dark:hover:bg-gray-600 
                         transition-colors group/link"
                >
                  <Github className="w-5 h-5" />
                  <ExternalLink className="w-3 h-3 opacity-0 group-hover/link:opacity-100 absolute top-1 right-1" />
                </a>
              )}
              {donor.social.twitter && (
                <a
                  href={`https://twitter.com/${donor.social.twitter}`}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="p-2 rounded-lg bg-gray-100 dark:bg-gray-700 hover:bg-gray-200 dark:hover:bg-gray-600 
                         transition-colors group/link"
                >
                  <Twitter className="w-5 h-5" />
                  <ExternalLink className="w-3 h-3 opacity-0 group-hover/link:opacity-100 absolute top-1 right-1" />
                </a>
              )}
            </div>
          )}
        </div>
      </div>
    </motion.div>
  );
}
