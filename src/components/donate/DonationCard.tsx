import { motion } from "framer-motion";
import { Heart, Coffee, Star, Check } from "lucide-react";
import type { DonationTier } from "../../types";

interface DonationCardProps {
  tier: DonationTier;
  index: number;
}

const icons = {
  Supporter: Heart,
  Enthusiast: Coffee,
  Champion: Star,
};

const gradients = {
  Supporter: "from-rose-400 to-red-600",
  Enthusiast: "from-amber-400 to-orange-600",
  Champion: "from-yellow-400 to-yellow-600",
};

export default function DonationCard({ tier, index }: DonationCardProps) {
  const Icon = icons[tier.name as keyof typeof icons];
  const gradient = gradients[tier.name as keyof typeof gradients];

  return (
    <motion.div
      initial={{ opacity: 0, y: 20 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ delay: index * 0.1 }}
      className={`card relative overflow-hidden group h-full flex flex-col ${
        index === 1 ? "transform md:-translate-y-4" : ""
      }`}
    >
      {/* Popular Badge */}
      {index === 1 && (
        <div className="absolute -top-4 left-1/2 transform -translate-x-1/2">
          <div className="bg-gradient-to-r from-primary-500 to-secondary-500 text-white px-4 py-1 rounded-full text-sm font-medium shadow-lg">
            Most Popular
          </div>
        </div>
      )}

      {/* Background Effects */}
      <div className="absolute -top-10 -right-10 w-40 h-40 bg-gradient-to-br from-primary-500/10 to-secondary-500/10 rounded-full blur-3xl group-hover:opacity-75 transition-opacity" />
      <div className="absolute -bottom-10 -left-10 w-40 h-40 bg-gradient-to-tr from-secondary-500/10 to-primary-500/10 rounded-full blur-3xl group-hover:opacity-75 transition-opacity" />

      <div className="relative flex flex-col h-full">
        {/* Header */}
        <div className="flex items-center gap-4 mb-6">
          <div
            className={`p-3 rounded-xl bg-gradient-to-br ${gradient} 
                        shadow-lg transform group-hover:scale-110 group-hover:rotate-6 
                        transition-all duration-300`}
          >
            <Icon className="w-6 h-6 text-white" />
          </div>
          <div>
            <h3 className="text-2xl font-semibold">{tier.name}</h3>
            <div className="flex items-baseline gap-1">
              <span className="text-3xl font-bold">${tier.amount}</span>
              <span className="text-gray-600 dark:text-gray-400">/month</span>
            </div>
          </div>
        </div>

        {/* Description */}
        <p className="text-gray-600 dark:text-gray-300 mb-6 text-lg">
          {tier.description}
        </p>

        {/* Benefits */}
        <div className="flex-grow space-y-4 mb-8">
          {tier.benefits.map((benefit) => (
            <div key={benefit} className="flex items-start gap-3 group/benefit">
              <div
                className="mt-1 p-1 rounded-full bg-green-100 dark:bg-green-900/30 
                           group-hover/benefit:scale-110 transition-transform"
              >
                <Check className="w-4 h-4 text-green-500" />
              </div>
              <span className="text-gray-700 dark:text-gray-300">
                {benefit}
              </span>
            </div>
          ))}
        </div>

        {/* Action Button */}
        <a
          href={`https://gitlab.com/users/eshanized/-/donate_subscriptions`}
          target="_blank"
          rel="noopener noreferrer"
          className="w-full btn-primary py-4 group relative overflow-hidden rounded-xl 
                   shadow-lg hover:shadow-xl transition-shadow"
        >
          <span className="relative z-10 flex items-center justify-center gap-2 text-lg">
            Support via GitLab
            <Icon className="w-5 h-5" />
          </span>
          <motion.div
            className={`absolute inset-0 bg-gradient-to-r ${gradient}`}
            initial={{ x: "100%" }}
            whileHover={{ x: 0 }}
            transition={{ duration: 0.3 }}
          />
        </a>
      </div>
    </motion.div>
  );
}
