import { motion } from "framer-motion";
import { Cpu, Workflow, Zap, Shield } from "lucide-react";

const fadeInUp = {
  hidden: { opacity: 0, y: 20 },
  visible: { opacity: 1, y: 0 },
};

const features = [
  {
    icon: Cpu,
    title: "Modern Architecture",
    description:
      "Built on the latest Linux kernel with optimized system architecture.",
    gradient: "from-blue-500 to-indigo-500",
  },
  {
    icon: Workflow,
    title: "Streamlined Workflow",
    description:
      "Intuitive interface and tools designed for maximum productivity.",
    gradient: "from-purple-500 to-pink-500",
  },
  {
    icon: Zap,
    title: "Performance First",
    description: "Optimized for speed with minimal resource overhead.",
    gradient: "from-amber-500 to-orange-500",
  },
  {
    icon: Shield,
    title: "Security Focus",
    description: "Enhanced security features and regular security updates.",
    gradient: "from-emerald-500 to-teal-500",
  },
];

export default function Differentiators() {
  return (
    <motion.section className="section-container bg-gradient-to-b from-primary-500/10 via-transparent to-transparent">
      <motion.h2
        variants={fadeInUp}
        className="text-3xl font-bold text-center mb-12"
      >
        What Makes Snigdha OS <span className="gradient-text">Different</span>
      </motion.h2>
      <div className="grid md:grid-cols-2 lg:grid-cols-4 gap-8">
        {features.map((feature) => (
          <motion.div
            key={feature.title}
            variants={fadeInUp}
            className="card group hover:scale-105 transition-all duration-300"
          >
            <div
              className={`w-14 h-14 rounded-xl bg-gradient-to-br ${feature.gradient} 
                          flex items-center justify-center mb-6 transform group-hover:rotate-6 
                          transition-transform duration-300`}
            >
              <feature.icon className="w-7 h-7 text-white" />
            </div>
            <h3 className="text-xl font-semibold mb-3 group-hover:text-primary-500 transition-colors">
              {feature.title}
            </h3>
            <p className="text-gray-600 dark:text-gray-300">
              {feature.description}
            </p>
          </motion.div>
        ))}
      </div>
    </motion.section>
  );
}
