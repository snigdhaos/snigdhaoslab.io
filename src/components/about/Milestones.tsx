import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { Trophy, Users, Download, Code, Star, GitMerge } from "lucide-react";

const milestones = [
  {
    icon: Download,
    value: "100K+",
    label: "Downloads",
    description: "Active installations worldwide",
    color: "from-blue-500 to-indigo-500",
  },
  {
    icon: Users,
    value: "50K+",
    label: "Community",
    description: "Active community members",
    color: "from-emerald-500 to-teal-500",
  },
  {
    icon: Code,
    value: "2.5K+",
    label: "Packages",
    description: "Optimized software packages",
    color: "from-purple-500 to-pink-500",
  },
  {
    icon: Star,
    value: "10K+",
    label: "GitHub Stars",
    description: "Open source recognition",
    color: "from-amber-500 to-orange-500",
  },
  {
    icon: GitMerge,
    value: "5K+",
    label: "Contributions",
    description: "Community pull requests",
    color: "from-red-500 to-rose-500",
  },
  {
    icon: Trophy,
    value: "15+",
    label: "Awards",
    description: "Industry recognition",
    color: "from-cyan-500 to-blue-500",
  },
];

export default function Milestones() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.1,
  });

  return (
    <section className="py-20 relative overflow-hidden">
      {/* Background Elements */}
      <div className="absolute inset-0 bg-gradient-to-b from-white via-gray-50 to-white dark:from-gray-800 dark:via-gray-900 dark:to-gray-800" />
      <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
      <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />

      <motion.div
        ref={ref}
        initial={{ opacity: 0 }}
        animate={inView ? { opacity: 1 } : { opacity: 0 }}
        className="section-container relative"
      >
        {/* Header */}
        <div className="text-center mb-16">
          <div className="inline-flex items-center justify-center p-3 mb-4 rounded-2xl bg-gradient-to-br from-primary-500 to-secondary-500">
            <Trophy className="w-8 h-8 text-white" />
          </div>
          <h2 className="text-4xl font-bold mb-4">
            Our <span className="gradient-text">Achievements</span>
          </h2>
          <p className="text-xl text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
            Key milestones that showcase our growth and impact in the Linux
            community
          </p>
        </div>

        {/* Milestones Grid */}
        <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-8 max-w-6xl mx-auto">
          {milestones.map((milestone, index) => (
            <motion.div
              key={milestone.label}
              initial={{ opacity: 0, y: 20 }}
              animate={inView ? { opacity: 1, y: 0 } : { opacity: 0, y: 20 }}
              transition={{ delay: index * 0.1 }}
              className="relative group"
            >
              <div className="card overflow-hidden">
                {/* Gradient Background */}
                <div
                  className="absolute inset-0 bg-gradient-to-br opacity-0 group-hover:opacity-5 
                             transition-opacity duration-300"
                />

                {/* Content */}
                <div className="relative z-10">
                  <div
                    className={`w-16 h-16 rounded-2xl bg-gradient-to-br ${milestone.color} 
                                flex items-center justify-center mb-6 transform group-hover:scale-110 
                                group-hover:rotate-6 transition-transform duration-300`}
                  >
                    <milestone.icon className="w-8 h-8 text-white" />
                  </div>

                  <div className="space-y-2">
                    <motion.div
                      initial={{ opacity: 0, y: 10 }}
                      animate={
                        inView ? { opacity: 1, y: 0 } : { opacity: 0, y: 10 }
                      }
                      transition={{ delay: index * 0.1 + 0.2 }}
                      className="flex items-baseline gap-2"
                    >
                      <h3
                        className={`text-4xl font-bold bg-gradient-to-r ${milestone.color} 
                                  bg-clip-text text-transparent`}
                      >
                        {milestone.value}
                      </h3>
                      <span className="text-lg font-medium text-gray-600 dark:text-gray-300">
                        {milestone.label}
                      </span>
                    </motion.div>
                    <p className="text-gray-600 dark:text-gray-300">
                      {milestone.description}
                    </p>
                  </div>

                  {/* Decorative Elements */}
                  <div
                    className="absolute -bottom-4 -right-4 w-24 h-24 bg-gradient-to-br opacity-10 
                               rounded-full blur-2xl transition-opacity duration-300 
                               group-hover:opacity-20"
                  />
                </div>
              </div>
            </motion.div>
          ))}
        </div>
      </motion.div>
    </section>
  );
}
