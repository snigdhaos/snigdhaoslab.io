import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { History, Star, Users, Award, Rocket, Terminal } from "lucide-react";

const milestones = [
  {
    year: "2023",
    title: "Project Launch",
    description:
      "Initial release of Snigdha OS with core features and optimizations.",
    icon: Rocket,
    color: "from-blue-500 to-indigo-500",
  },
  {
    year: "2024 Q1",
    title: "Growing Community",
    description:
      "Reached 10,000+ active users and established developer community.",
    icon: Users,
    color: "from-emerald-500 to-teal-500",
  },
  {
    year: "2024 Q2",
    title: "Major Recognition",
    description:
      "Featured in major Linux publications and received community awards.",
    icon: Award,
    color: "from-amber-500 to-orange-500",
  },
  {
    year: "2024 Q3",
    title: "Package Evolution",
    description:
      "Introduced AI-powered package management and system optimization.",
    icon: Terminal,
    color: "from-purple-500 to-pink-500",
  },
  {
    year: "2024 Q4",
    title: "Enterprise Adoption",
    description:
      "Major organizations began deploying Snigdha OS in production.",
    icon: Star,
    color: "from-red-500 to-rose-500",
  },
  {
    year: "2025",
    title: "Future Vision",
    description:
      "Continuing innovation with focus on AI, security, and performance.",
    icon: History,
    color: "from-cyan-500 to-blue-500",
  },
];

export default function Timeline() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.1,
  });

  return (
    <section className="py-20 relative overflow-hidden">
      {/* Background Elements */}
      <div className="absolute inset-0 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900" />
      <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
      <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />

      <motion.div
        ref={ref}
        initial={{ opacity: 0 }}
        animate={inView ? { opacity: 1 } : { opacity: 0 }}
        className="section-container relative"
      >
        {/* Header */}
        <div className="text-center mb-16">
          <div className="inline-flex items-center justify-center p-3 mb-4 rounded-2xl bg-gradient-to-br from-primary-500 to-secondary-500">
            <History className="w-8 h-8 text-white" />
          </div>
          <h2 className="text-4xl font-bold mb-4">
            Snigdha OS <span className="gradient-text">Journey</span>
          </h2>
          <p className="text-xl text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
            Explore the key milestones in our mission to revolutionize Linux
          </p>
        </div>

        {/* Timeline */}
        <div className="max-w-4xl mx-auto">
          {milestones.map((milestone, index) => (
            <motion.div
              key={milestone.year}
              initial={{ opacity: 0, x: index % 2 === 0 ? -50 : 50 }}
              animate={
                inView
                  ? { opacity: 1, x: 0 }
                  : { opacity: 0, x: index % 2 === 0 ? -50 : 50 }
              }
              transition={{ delay: index * 0.2 }}
              className={`flex items-center gap-8 mb-12 ${
                index % 2 === 0 ? "flex-row" : "flex-row-reverse"
              }`}
            >
              {/* Line Connector */}
              <div className="hidden md:block flex-1 relative">
                <div className="absolute inset-0 flex items-center justify-center">
                  <motion.div
                    initial={{ width: 0 }}
                    animate={inView ? { width: "100%" } : { width: 0 }}
                    transition={{ delay: index * 0.2, duration: 0.5 }}
                    className={`h-0.5 bg-gradient-to-r ${milestone.color}`}
                  />
                </div>
              </div>

              {/* Content Card */}
              <motion.div
                whileHover={{ scale: 1.02 }}
                className="flex-1 card group hover:shadow-xl transition-all duration-300"
              >
                <div className="flex items-start gap-4">
                  <div
                    className={`p-3 rounded-xl bg-gradient-to-br ${milestone.color} 
                                transform group-hover:scale-110 group-hover:rotate-6 transition-transform duration-300`}
                  >
                    <milestone.icon className="w-6 h-6 text-white" />
                  </div>
                  <div>
                    <div className="flex items-baseline gap-2 mb-2">
                      <h3 className="text-xl font-semibold">
                        {milestone.title}
                      </h3>
                      <span
                        className={`text-sm font-medium bg-gradient-to-r ${milestone.color} 
                                   bg-clip-text text-transparent`}
                      >
                        {milestone.year}
                      </span>
                    </div>
                    <p className="text-gray-600 dark:text-gray-300">
                      {milestone.description}
                    </p>
                  </div>
                </div>
              </motion.div>
            </motion.div>
          ))}

          {/* Future Indicator */}
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            animate={inView ? { opacity: 1, y: 0 } : { opacity: 0, y: 20 }}
            transition={{ delay: milestones.length * 0.2 }}
            className="text-center"
          >
            <div className="inline-flex items-center justify-center p-4 rounded-full bg-gradient-to-br from-primary-500/20 to-secondary-500/20">
              <div className="p-2 rounded-full bg-gradient-to-br from-primary-500 to-secondary-500">
                <Rocket className="w-6 h-6 text-white" />
              </div>
            </div>
            <p className="mt-4 text-gray-600 dark:text-gray-300">
              And still innovating...
            </p>
          </motion.div>
        </div>
      </motion.div>
    </section>
  );
}
