import { motion } from "framer-motion";
import {
  Gauge,
  MemoryStick as Memory,
  Package,
  Zap,
  Clock,
  Cpu,
  HardDrive,
  Network,
} from "lucide-react";

const fadeInUp = {
  hidden: { opacity: 0, y: 20 },
  visible: { opacity: 1, y: 0 },
};

const metrics = [
  {
    title: "Boot Time",
    value: "85%",
    description:
      "Average boot time: 15 seconds (85% faster than traditional distributions)",
    comparison: "Traditional Linux: ~60 seconds",
    icon: Clock,
    gradient: "from-blue-500 to-indigo-500",
    glow: "from-blue-500/20 to-indigo-500/20",
    chart: {
      value: 85,
      color: "from-blue-500 to-indigo-500",
    },
  },
  {
    title: "Memory Usage",
    value: "70%",
    description:
      "Base memory usage: 500MB (30% less than comparable distributions)",
    comparison: "Traditional Linux: ~800MB",
    icon: Memory,
    gradient: "from-purple-500 to-pink-500",
    glow: "from-purple-500/20 to-pink-500/20",
    chart: {
      value: 70,
      color: "from-purple-500 to-pink-500",
    },
  },
  {
    title: "Package Installation",
    value: "90%",
    description:
      "Package installation: 90% faster with optimized package manager",
    comparison: "Average speed: 50MB/s vs 5MB/s",
    icon: Package,
    gradient: "from-emerald-500 to-teal-500",
    glow: "from-emerald-500/20 to-teal-500/20",
    chart: {
      value: 90,
      color: "from-emerald-500 to-teal-500",
    },
  },
  {
    title: "System Response",
    value: "95%",
    description: "Application launch time reduced by 95% with optimized I/O",
    comparison: "Average launch time: 0.5s vs 3s",
    icon: Zap,
    gradient: "from-amber-500 to-orange-500",
    glow: "from-amber-500/20 to-orange-500/20",
    chart: {
      value: 95,
      color: "from-amber-500 to-orange-500",
    },
  },
];

const additionalMetrics = [
  {
    title: "CPU Utilization",
    icon: Cpu,
    gradient: "from-rose-500 to-red-500",
    glow: "from-rose-500/20 to-red-500/20",
    stats: [
      { label: "Idle State", value: "2-3%", baseline: "5-7%" },
      { label: "Under Load", value: "30-40%", baseline: "50-60%" },
      { label: "Power Saving", value: "45%", baseline: "25%" },
    ],
  },
  {
    title: "Storage Performance",
    icon: HardDrive,
    gradient: "from-cyan-500 to-blue-500",
    glow: "from-cyan-500/20 to-blue-500/20",
    stats: [
      { label: "Read Speed", value: "2.5 GB/s", baseline: "1.2 GB/s" },
      { label: "Write Speed", value: "1.8 GB/s", baseline: "800 MB/s" },
      { label: "I/O Operations", value: "120k IOPS", baseline: "80k IOPS" },
    ],
  },
  {
    title: "Network Stack",
    icon: Network,
    gradient: "from-violet-500 to-purple-500",
    glow: "from-violet-500/20 to-purple-500/20",
    stats: [
      { label: "TCP Latency", value: "< 1ms", baseline: "2-3ms" },
      { label: "Connection Pool", value: "10k+", baseline: "5k" },
      { label: "Throughput", value: "10 Gb/s", baseline: "5 Gb/s" },
    ],
  },
];

export default function PerformanceMetrics() {
  return (
    <motion.section className="section-container relative overflow-hidden">
      {/* Background Elements */}
      <div className="absolute inset-0 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900" />
      <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
      <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />

      <motion.div className="relative">
        <motion.h2
          variants={fadeInUp}
          className="text-3xl font-bold text-center mb-4"
        >
          Performance <span className="gradient-text">Metrics</span>
        </motion.h2>
        <motion.p
          variants={fadeInUp}
          className="text-xl text-gray-600 dark:text-gray-300 text-center mb-12 max-w-3xl mx-auto"
        >
          Experience unparalleled performance with our optimized system
        </motion.p>

        <div className="max-w-7xl mx-auto space-y-12">
          {/* Main Metrics */}
          <motion.div variants={fadeInUp} className="grid lg:grid-cols-2 gap-8">
            {metrics.map((metric) => (
              <div key={metric.title} className="relative group">
                {/* Card */}
                <div className="card h-full relative overflow-hidden">
                  {/* Background Glow */}
                  <div
                    className={`absolute -inset-2 bg-gradient-to-r ${metric.glow} rounded-3xl blur-2xl 
                                opacity-0 group-hover:opacity-100 transition-opacity duration-500`}
                  />

                  <div className="relative">
                    {/* Header */}
                    <div className="flex items-center gap-4 mb-6">
                      <div
                        className={`p-3 rounded-xl bg-gradient-to-br ${metric.gradient} 
                                   group-hover:scale-110 group-hover:rotate-6 transition-transform duration-300`}
                      >
                        <metric.icon className="w-6 h-6 text-white" />
                      </div>
                      <div>
                        <h3 className="text-xl font-semibold">
                          {metric.title}
                        </h3>
                        <p className="text-sm text-gray-500 dark:text-gray-400">
                          {metric.comparison}
                        </p>
                      </div>
                    </div>

                    {/* Chart */}
                    <div className="mb-6">
                      <div className="relative h-4 bg-gray-200 dark:bg-gray-700 rounded-full overflow-hidden">
                        <motion.div
                          initial={{ width: 0 }}
                          whileInView={{ width: `${metric.chart.value}%` }}
                          transition={{ duration: 1.5, ease: "easeOut" }}
                          viewport={{ once: true }}
                          className={`h-full bg-gradient-to-r ${metric.chart.color} rounded-full`}
                        />
                      </div>
                      <div className="mt-2 flex justify-between text-sm">
                        <span className="text-gray-600 dark:text-gray-300">
                          Improvement
                        </span>
                        <span
                          className={`font-medium bg-gradient-to-r ${metric.gradient} bg-clip-text text-transparent`}
                        >
                          {metric.value}
                        </span>
                      </div>
                    </div>

                    {/* Description */}
                    <p className="text-gray-600 dark:text-gray-300">
                      {metric.description}
                    </p>
                  </div>
                </div>
              </div>
            ))}
          </motion.div>

          {/* Additional Performance Metrics */}
          <motion.div variants={fadeInUp} className="grid md:grid-cols-3 gap-8">
            {additionalMetrics.map((section) => (
              <div key={section.title} className="relative group">
                <div className="card h-full relative overflow-hidden">
                  {/* Background Glow */}
                  <div
                    className={`absolute -inset-2 bg-gradient-to-r ${section.glow} rounded-3xl blur-2xl 
                                opacity-0 group-hover:opacity-100 transition-opacity duration-500`}
                  />

                  <div className="relative">
                    {/* Header */}
                    <div className="flex items-center gap-4 mb-6">
                      <div
                        className={`p-3 rounded-xl bg-gradient-to-br ${section.gradient} 
                                   group-hover:scale-110 group-hover:rotate-6 transition-transform duration-300`}
                      >
                        <section.icon className="w-6 h-6 text-white" />
                      </div>
                      <h3 className="text-xl font-semibold">{section.title}</h3>
                    </div>

                    {/* Stats */}
                    <div className="space-y-4">
                      {section.stats.map((stat) => (
                        <div key={stat.label} className="group/stat">
                          <div className="flex justify-between items-center mb-1">
                            <span className="text-sm text-gray-600 dark:text-gray-300">
                              {stat.label}
                            </span>
                            <div className="text-right">
                              <span
                                className={`text-sm font-medium bg-gradient-to-r ${section.gradient} 
                                           bg-clip-text text-transparent`}
                              >
                                {stat.value}
                              </span>
                              <span
                                className="text-xs text-gray-400 dark:text-gray-500 ml-2 opacity-0 
                                           group-hover/stat:opacity-100 transition-opacity"
                              >
                                vs {stat.baseline}
                              </span>
                            </div>
                          </div>
                          <div className="h-1.5 bg-gray-200 dark:bg-gray-700 rounded-full overflow-hidden">
                            <motion.div
                              initial={{ width: 0 }}
                              whileInView={{ width: "100%" }}
                              transition={{ duration: 1, ease: "easeOut" }}
                              viewport={{ once: true }}
                              className={`h-full bg-gradient-to-r ${section.gradient} rounded-full`}
                            />
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </motion.div>

          {/* Benchmark Note */}
          <motion.div
            variants={fadeInUp}
            className="text-center text-sm text-gray-500 dark:text-gray-400"
          >
            * All benchmarks performed on standard hardware: AMD Ryzen 7 5800X,
            32GB RAM, NVMe SSD
          </motion.div>
        </div>
      </motion.div>
    </motion.section>
  );
}
