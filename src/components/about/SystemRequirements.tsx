import { motion } from "framer-motion";
import {
  Cpu,
  HardDrive,
  MemoryStick as Memory,
  Monitor,
  Zap,
  Gauge,
  Wifi,
  Layers,
} from "lucide-react";

const fadeInUp = {
  hidden: { opacity: 0, y: 20 },
  visible: { opacity: 1, y: 0 },
};

const requirements = {
  minimum: [
    {
      icon: Cpu,
      title: "Processor",
      value: "Dual-core 2GHz or better",
      color: "from-blue-400 to-blue-600",
      metric: "2.0+ GHz",
      detail: "x86_64 architecture",
    },
    {
      icon: Memory,
      title: "Memory",
      value: "4GB RAM",
      color: "from-purple-400 to-purple-600",
      metric: "4 GB",
      detail: "DDR3 or better",
    },
    {
      icon: HardDrive,
      title: "Storage",
      value: "20GB free space",
      color: "from-emerald-400 to-emerald-600",
      metric: "20 GB",
      detail: "SSD recommended",
    },
    {
      icon: Monitor,
      title: "Display",
      value: "1024x768 resolution",
      color: "from-amber-400 to-amber-600",
      metric: "1024×768",
      detail: "32-bit color",
    },
    {
      icon: Wifi,
      title: "Network",
      value: "Broadband connection",
      color: "from-sky-400 to-sky-600",
      metric: "1+ Mbps",
      detail: "For updates",
    },
    {
      icon: Layers,
      title: "Graphics",
      value: "OpenGL 2.0",
      color: "from-rose-400 to-rose-600",
      metric: "2.0+",
      detail: "Basic GPU",
    },
  ],
  recommended: [
    {
      icon: Cpu,
      title: "Processor",
      value: "Quad-core 3GHz or better",
      color: "from-blue-500 to-blue-700",
      metric: "3.0+ GHz",
      detail: "Modern CPU",
    },
    {
      icon: Memory,
      title: "Memory",
      value: "8GB RAM or more",
      color: "from-purple-500 to-purple-700",
      metric: "8+ GB",
      detail: "DDR4 or better",
    },
    {
      icon: HardDrive,
      title: "Storage",
      value: "40GB free space (SSD)",
      color: "from-emerald-500 to-emerald-700",
      metric: "40+ GB",
      detail: "NVMe SSD",
    },
    {
      icon: Monitor,
      title: "Display",
      value: "1920x1080 or higher",
      color: "from-amber-500 to-amber-700",
      metric: "1080p+",
      detail: "High DPI ready",
    },
    {
      icon: Wifi,
      title: "Network",
      value: "High-speed connection",
      color: "from-sky-500 to-sky-700",
      metric: "10+ Mbps",
      detail: "Ethernet preferred",
    },
    {
      icon: Layers,
      title: "Graphics",
      value: "OpenGL 3.0+",
      color: "from-rose-500 to-rose-700",
      metric: "3.0+",
      detail: "Dedicated GPU",
    },
  ],
};

export default function SystemRequirements() {
  return (
    <motion.section className="section-container relative overflow-hidden">
      {/* Background Elements */}
      <div className="absolute inset-0 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900" />
      <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
      <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />

      <motion.div className="relative">
        <motion.h2
          variants={fadeInUp}
          className="text-3xl font-bold text-center mb-4"
        >
          System <span className="gradient-text">Requirements</span>
        </motion.h2>
        <motion.p
          variants={fadeInUp}
          className="text-xl text-gray-600 dark:text-gray-300 text-center mb-12 max-w-3xl mx-auto"
        >
          Choose the configuration that best suits your needs
        </motion.p>

        <div className="grid lg:grid-cols-2 gap-8 max-w-7xl mx-auto">
          {["minimum", "recommended"].map((type) => (
            <motion.div
              key={type}
              variants={fadeInUp}
              className="relative group"
            >
              {/* Card */}
              <div className="card h-full relative overflow-hidden">
                {/* Header */}
                <div className="flex items-center gap-4 mb-8">
                  <div
                    className={`p-3 rounded-xl bg-gradient-to-br ${
                      type === "minimum"
                        ? "from-primary-500 to-secondary-500"
                        : "from-secondary-500 to-accent-500"
                    } text-white`}
                  >
                    {type === "minimum" ? (
                      <Zap className="w-6 h-6" />
                    ) : (
                      <Gauge className="w-6 h-6" />
                    )}
                  </div>
                  <div>
                    <h3 className="text-2xl font-semibold capitalize">
                      {type}
                    </h3>
                    <p className="text-gray-600 dark:text-gray-400">
                      {type === "minimum"
                        ? "Basic functionality"
                        : "Optimal experience"}
                    </p>
                  </div>
                </div>

                {/* Requirements Grid */}
                <div className="grid md:grid-cols-2 gap-6">
                  {requirements[type as keyof typeof requirements].map(
                    (req) => (
                      <div
                        key={req.title}
                        className="relative group/item overflow-hidden rounded-xl bg-gray-50 dark:bg-gray-800/50 
                               p-4 hover:bg-gradient-to-br hover:from-gray-50 hover:to-gray-100 
                               dark:hover:from-gray-800/50 dark:hover:to-gray-700/50 transition-colors"
                      >
                        <div className="flex items-start gap-4">
                          <div
                            className={`p-2 rounded-lg bg-gradient-to-br ${req.color} 
                                    text-white transform group-hover/item:scale-110 
                                    group-hover/item:rotate-6 transition-transform`}
                          >
                            <req.icon className="w-5 h-5" />
                          </div>
                          <div>
                            <h4 className="font-medium mb-1">{req.title}</h4>
                            <p className="text-sm text-gray-600 dark:text-gray-300 mb-2">
                              {req.value}
                            </p>
                            <div className="flex items-center gap-3 text-xs text-gray-500 dark:text-gray-400">
                              <span className="px-2 py-1 rounded-full bg-gray-100 dark:bg-gray-700">
                                {req.metric}
                              </span>
                              <span>{req.detail}</span>
                            </div>
                          </div>
                        </div>

                        {/* Hover Effect */}
                        <div
                          className={`absolute inset-0 bg-gradient-to-r ${req.color} 
                                   opacity-0 group-hover/item:opacity-5 transition-opacity`}
                        />
                      </div>
                    ),
                  )}
                </div>
              </div>
            </motion.div>
          ))}
        </div>

        {/* Note */}
        <motion.div
          variants={fadeInUp}
          className="text-center mt-12 text-sm text-gray-500 dark:text-gray-400"
        >
          * Requirements may vary based on usage and installed applications
        </motion.div>
      </motion.div>
    </motion.section>
  );
}
