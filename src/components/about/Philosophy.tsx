import { motion } from "framer-motion";
import { Lightbulb, Target } from "lucide-react";

const fadeInUp = {
  hidden: { opacity: 0, y: 20 },
  visible: { opacity: 1, y: 0 },
};

export default function Philosophy() {
  return (
    <motion.section className="section-container">
      <motion.h1
        variants={fadeInUp}
        className="text-4xl md:text-5xl font-bold text-center mb-8"
      >
        About <span className="gradient-text">Snigdha OS</span>
      </motion.h1>
      <motion.div
        variants={fadeInUp}
        className="max-w-3xl mx-auto text-center mb-12"
      >
        <p className="text-lg text-gray-600 dark:text-gray-300">
          Snigdha OS is built on the principles of simplicity, security, and
          efficiency. Our mission is to provide a modern Linux distribution that
          empowers developers and users alike with powerful tools and a seamless
          experience.
        </p>
      </motion.div>
      <div className="grid md:grid-cols-2 gap-8 max-w-4xl mx-auto">
        <motion.div
          variants={fadeInUp}
          className="card group hover:scale-105 transition-all duration-300"
        >
          <div className="flex items-center gap-4 mb-4">
            <div className="p-3 rounded-xl bg-gradient-to-br from-blue-500 to-purple-500 text-white">
              <Lightbulb className="w-6 h-6" />
            </div>
            <h3 className="text-xl font-semibold">Our Vision</h3>
          </div>
          <p className="text-gray-600 dark:text-gray-300">
            To create a Linux distribution that combines the power of
            traditional Unix philosophy with modern development practices and
            user experience.
          </p>
        </motion.div>
        <motion.div
          variants={fadeInUp}
          className="card group hover:scale-105 transition-all duration-300"
        >
          <div className="flex items-center gap-4 mb-4">
            <div className="p-3 rounded-xl bg-gradient-to-br from-emerald-500 to-teal-500 text-white">
              <Target className="w-6 h-6" />
            </div>
            <h3 className="text-xl font-semibold">Our Mission</h3>
          </div>
          <p className="text-gray-600 dark:text-gray-300">
            To provide a secure, efficient, and user-friendly operating system
            that enhances productivity and creativity for developers and power
            users.
          </p>
        </motion.div>
      </div>
    </motion.section>
  );
}
