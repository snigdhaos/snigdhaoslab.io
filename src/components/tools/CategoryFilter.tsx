import { ChevronDown } from "lucide-react";
import { motion } from "framer-motion";
import { useState, useRef, useEffect } from "react";

interface CategoryFilterProps {
  categories: string[];
  selectedCategory: string;
  onSelect: (category: string) => void;
}

export default function CategoryFilter({
  categories,
  selectedCategory,
  onSelect,
}: CategoryFilterProps) {
  const [isOpen, setIsOpen] = useState(false);
  const dropdownRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        dropdownRef.current &&
        !dropdownRef.current.contains(event.target as Node)
      ) {
        setIsOpen(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => document.removeEventListener("mousedown", handleClickOutside);
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0, y: -20 }}
      animate={{ opacity: 1, y: 0 }}
      className="relative min-w-[200px]"
      ref={dropdownRef}
    >
      <button
        onClick={() => setIsOpen(!isOpen)}
        className="w-full px-4 py-3 rounded-xl bg-white dark:bg-gray-800 border-2 border-gray-200 
                 dark:border-gray-700 flex items-center justify-between gap-2 hover:border-primary-500 
                 transition-colors duration-200"
      >
        <span className="text-gray-700 dark:text-gray-300">
          {selectedCategory}
        </span>
        <ChevronDown
          className={`w-5 h-5 transition-transform duration-200 ${isOpen ? "rotate-180" : ""}`}
        />
      </button>

      {isOpen && (
        <motion.div
          initial={{ opacity: 0, y: -10 }}
          animate={{ opacity: 1, y: 0 }}
          className="absolute z-50 w-full mt-2 py-2 bg-white dark:bg-gray-800 rounded-xl shadow-lg 
                   border border-gray-200 dark:border-gray-700 max-h-60 overflow-y-auto"
        >
          {categories.map((category) => (
            <button
              key={category}
              onClick={() => {
                onSelect(category);
                setIsOpen(false);
              }}
              className={`w-full px-4 py-2 text-left hover:bg-gray-100 dark:hover:bg-gray-700 
                       transition-colors duration-200 ${
                         selectedCategory === category
                           ? "text-primary-500 bg-primary-50 dark:bg-primary-900/20"
                           : "text-gray-700 dark:text-gray-300"
                       }`}
            >
              {category}
            </button>
          ))}
        </motion.div>
      )}
    </motion.div>
  );
}
