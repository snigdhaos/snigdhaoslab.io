import {
  Package,
  ExternalLink,
  Bug,
  GitBranch,
  ChevronRight,
} from "lucide-react";
import { motion } from "framer-motion";
import type { Package as PackageType } from "../../types";
import CopyButton from "../common/CopyButton";

interface PackageCardProps {
  pkg: PackageType;
}

export default function PackageCard({ pkg }: PackageCardProps) {
  const installCommand = `sudo pacman -S ${pkg.name}`;

  return (
    <motion.div
      initial={{ opacity: 0, y: 20 }}
      animate={{ opacity: 1, y: 0 }}
      className="group relative"
    >
      {/* Card */}
      <div
        className="relative bg-white dark:bg-gray-800 rounded-2xl shadow-lg hover:shadow-xl 
                   transition-all duration-300 overflow-hidden border border-gray-100 dark:border-gray-700"
      >
        {/* Background Gradient Effect */}
        <div
          className="absolute inset-0 bg-gradient-to-br from-primary-500/5 via-transparent to-secondary-500/5 
                     opacity-0 group-hover:opacity-100 transition-opacity duration-500"
        />

        {/* Category Badge */}
        {pkg.category && (
          <div className="absolute top-4 right-4">
            <span
              className="px-3 py-1 text-sm rounded-full bg-primary-100 dark:bg-primary-900/30 
                         text-primary-600 dark:text-primary-400 font-medium"
            >
              {pkg.category}
            </span>
          </div>
        )}

        <div className="p-6">
          {/* Header */}
          <div className="flex items-start gap-4 mb-6">
            <div
              className="p-3 rounded-xl bg-gradient-to-br from-primary-500 to-secondary-500 
                         shadow-lg transform group-hover:scale-110 group-hover:rotate-6 transition-transform"
            >
              <Package className="w-6 h-6 text-white" />
            </div>
            <div className="flex-1">
              <h3 className="text-xl font-semibold mb-1 group-hover:text-primary-500 transition-colors">
                {pkg.name}
              </h3>
              <p className="text-sm text-gray-500 dark:text-gray-400">
                Version {pkg.version} • Updated {pkg.lastUpdated}
              </p>
            </div>
          </div>

          {/* Description */}
          <p className="text-gray-600 dark:text-gray-300 mb-6 line-clamp-2">
            {pkg.description}
          </p>

          {/* Installation Command */}
          <div className="mb-6">
            <div className="relative group/copy">
              <div
                className="flex items-center justify-between gap-2 p-3 bg-gray-50 dark:bg-gray-900 
                           rounded-xl font-mono text-sm overflow-hidden"
              >
                <code className="text-gray-700 dark:text-gray-300">
                  {installCommand}
                </code>
                <CopyButton
                  text={installCommand}
                  className="opacity-0 group-hover/copy:opacity-100 transition-opacity"
                />
              </div>
              <div
                className="absolute inset-0 bg-gradient-to-r from-primary-500/10 to-secondary-500/10 
                           rounded-xl blur opacity-0 group-hover/copy:opacity-100 transition-opacity"
              />
            </div>
          </div>

          {/* Links */}
          <div className="flex flex-wrap gap-4 mb-6">
            {pkg.repository && (
              <a
                href={pkg.repository}
                target="_blank"
                rel="noopener noreferrer"
                className="inline-flex items-center gap-2 text-sm text-gray-600 dark:text-gray-300
                         hover:text-primary-500 transition-colors group/link"
              >
                <div
                  className="p-1.5 rounded-lg bg-gray-100 dark:bg-gray-800 group-hover/link:bg-primary-100 
                             dark:group-hover/link:bg-primary-900/30 transition-colors"
                >
                  <GitBranch className="w-4 h-4" />
                </div>
                Source
                <ExternalLink className="w-3 h-3 opacity-0 group-hover/link:opacity-100 transition-opacity" />
              </a>
            )}
            {pkg.bugTracker && (
              <a
                href={pkg.bugTracker}
                target="_blank"
                rel="noopener noreferrer"
                className="inline-flex items-center gap-2 text-sm text-gray-600 dark:text-gray-300
                         hover:text-primary-500 transition-colors group/link"
              >
                <div
                  className="p-1.5 rounded-lg bg-gray-100 dark:bg-gray-800 group-hover/link:bg-primary-100 
                             dark:group-hover/link:bg-primary-900/30 transition-colors"
                >
                  <Bug className="w-4 h-4" />
                </div>
                Issues
                <ExternalLink className="w-3 h-3 opacity-0 group-hover/link:opacity-100 transition-opacity" />
              </a>
            )}
          </div>

          {/* Dependencies */}
          {pkg.depends && pkg.depends.length > 0 && (
            <div className="mb-6 pt-6 border-t border-gray-100 dark:border-gray-700">
              <h4 className="text-sm font-medium text-gray-500 dark:text-gray-400 mb-3">
                Dependencies
              </h4>
              <div className="flex flex-wrap gap-2">
                {pkg.depends.map((dep) => (
                  <span
                    key={dep}
                    className="px-2.5 py-1 text-xs rounded-full bg-gray-100 dark:bg-gray-700 
                             text-gray-600 dark:text-gray-300 hover:bg-gray-200 dark:hover:bg-gray-600 
                             transition-colors cursor-default"
                  >
                    {dep}
                  </span>
                ))}
              </div>
            </div>
          )}

          {/* Maintainers */}
          {pkg.maintainers && pkg.maintainers.length > 0 && (
            <div className="pt-6 border-t border-gray-100 dark:border-gray-700">
              <div className="flex items-center justify-between mb-4">
                <h4 className="text-sm font-medium text-gray-500 dark:text-gray-400">
                  Maintainers
                </h4>
                <ChevronRight className="w-4 h-4 text-gray-400 group-hover:text-primary-500 transition-colors" />
              </div>
              <div className="flex -space-x-3">
                {pkg.maintainers.map((maintainer) => (
                  <img
                    key={maintainer.email}
                    src={
                      maintainer.avatar ||
                      `https://ui-avatars.com/api/?name=${encodeURIComponent(maintainer.name)}&background=random`
                    }
                    alt={maintainer.name}
                    title={maintainer.name}
                    className="w-8 h-8 rounded-full border-2 border-white dark:border-gray-800 
                             hover:scale-110 transition-transform cursor-pointer"
                  />
                ))}
              </div>
            </div>
          )}
        </div>
      </div>
    </motion.div>
  );
}
