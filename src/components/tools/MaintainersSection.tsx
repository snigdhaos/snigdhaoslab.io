import { useState, useEffect } from "react";
import { motion } from "framer-motion";
import { Users, Loader2, Star, Award, Code, GitBranch } from "lucide-react";
import { getMaintainers } from "../../data/maintainers";
import MaintainerProfile from "./MaintainerProfile";
import type { PackageMaintainer } from "../../types";

const fadeInUp = {
  hidden: { opacity: 0, y: 20 },
  visible: { opacity: 1, y: 0 },
};

const stats = [
  {
    icon: Code,
    label: "Packages Maintained",
    value: "500+",
    color: "from-blue-500 to-indigo-500",
  },
  {
    icon: GitBranch,
    label: "Contributions",
    value: "2.5K+",
    color: "from-emerald-500 to-teal-500",
  },
  {
    icon: Star,
    label: "Community Rating",
    value: "4.9/5",
    color: "from-amber-500 to-orange-500",
  },
  {
    icon: Award,
    label: "Team Awards",
    value: "15+",
    color: "from-purple-500 to-pink-500",
  },
];

export default function MaintainersSection() {
  const [maintainers, setMaintainers] = useState<PackageMaintainer[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    async function fetchMaintainers() {
      try {
        setLoading(true);
        const data = await getMaintainers();
        setMaintainers(data);
      } catch (err) {
        setError(
          err instanceof Error ? err.message : "Failed to load maintainers",
        );
      } finally {
        setLoading(false);
      }
    }

    fetchMaintainers();
  }, []);

  if (loading) {
    return (
      <div className="flex items-center justify-center py-16">
        <div className="relative">
          <div className="w-16 h-16 rounded-full border-4 border-primary-500/30 border-t-primary-500 animate-spin" />
          <div className="absolute inset-0 flex items-center justify-center">
            <Users className="w-6 h-6 text-primary-500" />
          </div>
        </div>
      </div>
    );
  }

  if (error) {
    return (
      <div className="text-center py-16">
        <div className="inline-flex items-center justify-center p-4 rounded-full bg-red-100 dark:bg-red-900/30 mb-4">
          <Users className="w-8 h-8 text-red-500" />
        </div>
        <h3 className="text-xl font-semibold text-red-500 mb-2">
          Failed to load team members
        </h3>
        <p className="text-gray-500 dark:text-gray-400 mb-4">{error}</p>
        <button
          onClick={() => window.location.reload()}
          className="px-4 py-2 rounded-lg bg-red-500 text-white hover:bg-red-600 transition-colors"
        >
          Try Again
        </button>
      </div>
    );
  }

  const maintainersList = maintainers.filter((m) => m.role === "maintainer");
  const contributorsList = maintainers.filter((m) => m.role === "contributor");

  return (
    <motion.section
      initial="hidden"
      animate="visible"
      variants={{
        visible: {
          transition: {
            staggerChildren: 0.1,
          },
        },
      }}
      className="mt-24 relative"
    >
      {/* Decorative Elements */}
      <div className="absolute inset-0 overflow-hidden pointer-events-none">
        <div className="absolute -top-40 -right-40 w-80 h-80 bg-primary-500/10 rounded-full blur-3xl" />
        <div className="absolute -bottom-40 -left-40 w-80 h-80 bg-secondary-500/10 rounded-full blur-3xl" />
      </div>

      {/* Header */}
      <motion.div variants={fadeInUp} className="text-center mb-16 relative">
        <div className="inline-flex items-center justify-center p-3 mb-4 rounded-2xl bg-gradient-to-br from-primary-500 to-secondary-500">
          <Users className="w-8 h-8 text-white" />
        </div>
        <h2 className="text-4xl font-bold mb-4">
          Meet Our <span className="gradient-text">Team</span>
        </h2>
        <p className="text-xl text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
          The dedicated individuals who maintain and contribute to Snigdha OS
          packages
        </p>
      </motion.div>

      {/* Stats Grid */}
      <motion.div
        variants={fadeInUp}
        className="grid grid-cols-2 lg:grid-cols-4 gap-6 mb-16"
      >
        {stats.map((stat, index) => (
          <motion.div
            key={stat.label}
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ delay: index * 0.1 }}
            className="relative group"
          >
            <div className="bg-white dark:bg-gray-800 rounded-2xl p-6 relative overflow-hidden">
              {/* Background Gradient */}
              <div
                className={`absolute inset-0 bg-gradient-to-br ${stat.color} opacity-0 
                           group-hover:opacity-5 transition-opacity duration-300`}
              />

              <div className="relative">
                <div
                  className={`w-14 h-14 rounded-2xl bg-gradient-to-br ${stat.color} 
                             flex items-center justify-center mb-4 transform group-hover:scale-110 
                             group-hover:rotate-6 transition-transform duration-300`}
                >
                  <stat.icon className="w-7 h-7 text-white" />
                </div>
                <h3
                  className={`text-3xl font-bold mb-2 bg-gradient-to-r ${stat.color} bg-clip-text text-transparent`}
                >
                  {stat.value}
                </h3>
                <p className="text-gray-600 dark:text-gray-300">{stat.label}</p>
              </div>
            </div>
          </motion.div>
        ))}
      </motion.div>

      {/* Core Maintainers */}
      <motion.div variants={fadeInUp} className="mb-16">
        <div className="flex items-center justify-between mb-8">
          <h3 className="text-2xl font-bold">Core Maintainers</h3>
          <div className="h-1 flex-1 mx-6 rounded-full bg-gradient-to-r from-primary-500/20 to-secondary-500/20" />
          <span
            className="px-4 py-1 rounded-full bg-primary-100 dark:bg-primary-900/30 
                       text-primary-600 dark:text-primary-400 text-sm font-medium"
          >
            {maintainersList.length} Members
          </span>
        </div>
        <div className="grid md:grid-cols-2 gap-6">
          {maintainersList.map((maintainer) => (
            <MaintainerProfile key={maintainer.email} maintainer={maintainer} />
          ))}
        </div>
      </motion.div>

      {/* Contributors */}
      {contributorsList.length > 0 && (
        <motion.div variants={fadeInUp}>
          <div className="flex items-center justify-between mb-8">
            <h3 className="text-2xl font-bold">Contributors</h3>
            <div className="h-1 flex-1 mx-6 rounded-full bg-gradient-to-r from-secondary-500/20 to-accent-500/20" />
            <span
              className="px-4 py-1 rounded-full bg-secondary-100 dark:bg-secondary-900/30 
                         text-secondary-600 dark:text-secondary-400 text-sm font-medium"
            >
              {contributorsList.length} Members
            </span>
          </div>
          <div className="grid md:grid-cols-2 gap-6">
            {contributorsList.map((maintainer) => (
              <MaintainerProfile
                key={maintainer.email}
                maintainer={maintainer}
              />
            ))}
          </div>
        </motion.div>
      )}
    </motion.section>
  );
}
