import { motion } from "framer-motion";
import PackageCard from "./PackageCard";
import type { Package } from "../../types";
import { PackageX } from "lucide-react";

interface PackageListProps {
  packages: Package[];
}

export default function PackageList({ packages }: PackageListProps) {
  return (
    <motion.div
      initial="hidden"
      animate="visible"
      variants={{
        visible: {
          transition: {
            staggerChildren: 0.1,
          },
        },
      }}
      className="grid md:grid-cols-2 gap-8"
    >
      {packages.length === 0 ? (
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          className="col-span-2 text-center py-16"
        >
          <div
            className="inline-flex items-center justify-center p-4 mb-4 rounded-full 
                       bg-gray-100 dark:bg-gray-800"
          >
            <PackageX className="w-8 h-8 text-gray-400" />
          </div>
          <h3 className="text-xl font-semibold text-gray-900 dark:text-gray-100 mb-2">
            No packages found
          </h3>
          <p className="text-gray-500 dark:text-gray-400">
            Try adjusting your search or filter criteria
          </p>
        </motion.div>
      ) : (
        packages.map((pkg) => <PackageCard key={pkg.name} pkg={pkg} />)
      )}
    </motion.div>
  );
}
