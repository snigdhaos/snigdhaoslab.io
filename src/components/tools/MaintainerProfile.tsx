import { motion } from "framer-motion";
import {
  Gitlab,
  Mail,
  Hash,
  MessageSquare,
  Key,
  ExternalLink,
  MapPin,
} from "lucide-react";
import type { PackageMaintainer } from "../../types";
import CopyButton from "../common/CopyButton";

interface MaintainerProfileProps {
  maintainer: PackageMaintainer;
}

export default function MaintainerProfile({
  maintainer,
}: MaintainerProfileProps) {
  return (
    <motion.div
      initial={{ opacity: 0, y: 10 }}
      animate={{ opacity: 1, y: 0 }}
      className="group relative"
    >
      {/* Card */}
      <div
        className="bg-white dark:bg-gray-800 rounded-2xl p-6 shadow-lg hover:shadow-xl 
                   transition-all duration-300 relative overflow-hidden"
      >
        {/* Background Gradient Effect */}
        <div
          className="absolute inset-0 bg-gradient-to-br from-primary-500/5 via-transparent to-secondary-500/5 
                     opacity-0 group-hover:opacity-100 transition-opacity duration-500"
        />

        <div className="relative">
          {/* Header */}
          <div className="flex items-start gap-4 mb-6">
            <div className="relative">
              <img
                src={
                  maintainer.avatar ||
                  `https://ui-avatars.com/api/?name=${encodeURIComponent(maintainer.name)}&background=random`
                }
                alt={maintainer.name}
                className="w-16 h-16 rounded-2xl object-cover transform group-hover:scale-105 
                         group-hover:rotate-3 transition-transform duration-300"
              />
              <div className="absolute -bottom-2 -right-2">
                <span
                  className={`inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium
                               ${
                                 maintainer.role === "maintainer"
                                   ? "bg-primary-100 dark:bg-primary-900/30 text-primary-600 dark:text-primary-400"
                                   : "bg-secondary-100 dark:bg-secondary-900/30 text-secondary-600 dark:text-secondary-400"
                               }`}
                >
                  {maintainer.role}
                </span>
              </div>
            </div>
            <div className="flex-1 min-w-0">
              <h4 className="text-xl font-semibold mb-1 group-hover:text-primary-500 transition-colors">
                {maintainer.name}
              </h4>
              <div className="flex items-center gap-2 text-sm text-gray-500 dark:text-gray-400">
                <MapPin className="w-4 h-4" />
                <span>Worldwide</span>
              </div>
            </div>
          </div>

          {/* Contact Info */}
          <div className="space-y-3">
            {/* Email */}
            <div className="flex items-center gap-3 group/item">
              <div
                className="p-2 rounded-lg bg-gray-100 dark:bg-gray-700 group-hover/item:bg-primary-100 
                           dark:group-hover/item:bg-primary-900/30 transition-colors"
              >
                <Mail
                  className="w-4 h-4 text-gray-500 dark:text-gray-400 group-hover/item:text-primary-500 
                             transition-colors"
                />
              </div>
              <div className="flex-1 min-w-0 flex items-center gap-2">
                <span className="truncate text-gray-600 dark:text-gray-300">
                  {maintainer.email}
                </span>
                <CopyButton
                  text={maintainer.email}
                  className="opacity-0 group-hover/item:opacity-100"
                />
              </div>
            </div>

            {/* GitLab */}
            {maintainer.gitlab && (
              <div className="flex items-center gap-3 group/item">
                <div
                  className="p-2 rounded-lg bg-gray-100 dark:bg-gray-700 group-hover/item:bg-[#333]/10 
                             dark:group-hover/item:bg-white/10 transition-colors"
                >
                  <Gitlab
                    className="w-4 h-4 text-gray-500 dark:text-gray-400 group-hover/item:text-[#333] 
                                 dark:group-hover/item:text-white transition-colors"
                  />
                </div>
                <a
                  href={`https://gitlab.com/${maintainer.gitlab}`}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="flex-1 min-w-0 flex items-center gap-2 text-gray-600 dark:text-gray-300 
                           hover:text-[#333] dark:hover:text-white transition-colors"
                >
                  <span className="truncate">{maintainer.gitlab}</span>
                  <ExternalLink className="w-3 h-3 opacity-0 group-hover/item:opacity-100" />
                </a>
              </div>
            )}

            {/* Matrix */}
            {maintainer.matrix && (
              <div className="flex items-center gap-3 group/item">
                <div
                  className="p-2 rounded-lg bg-gray-100 dark:bg-gray-700 group-hover/item:bg-purple-100 
                             dark:group-hover/item:bg-purple-900/30 transition-colors"
                >
                  <MessageSquare
                    className="w-4 h-4 text-gray-500 dark:text-gray-400 group-hover/item:text-purple-500 
                                       transition-colors"
                  />
                </div>
                <div className="flex-1 min-w-0 flex items-center gap-2">
                  <span className="truncate text-gray-600 dark:text-gray-300">
                    {maintainer.matrix}
                  </span>
                  <CopyButton
                    text={maintainer.matrix}
                    className="opacity-0 group-hover/item:opacity-100"
                  />
                </div>
              </div>
            )}

            {/* IRC */}
            {maintainer.irc && (
              <div className="flex items-center gap-3 group/item">
                <div
                  className="p-2 rounded-lg bg-gray-100 dark:bg-gray-700 group-hover/item:bg-blue-100 
                             dark:group-hover/item:bg-blue-900/30 transition-colors"
                >
                  <Hash
                    className="w-4 h-4 text-gray-500 dark:text-gray-400 group-hover/item:text-blue-500 
                               transition-colors"
                  />
                </div>
                <span className="text-gray-600 dark:text-gray-300">
                  {maintainer.irc}
                </span>
              </div>
            )}

            {/* PGP Key */}
            {maintainer.pgpKey && (
              <div className="flex items-center gap-3 group/item">
                <div
                  className="p-2 rounded-lg bg-gray-100 dark:bg-gray-700 group-hover/item:bg-emerald-100 
                             dark:group-hover/item:bg-emerald-900/30 transition-colors"
                >
                  <Key
                    className="w-4 h-4 text-gray-500 dark:text-gray-400 group-hover/item:text-emerald-500 
                              transition-colors"
                  />
                </div>
                <div className="flex-1 min-w-0 flex items-center gap-2">
                  <code
                    className="truncate text-xs bg-gray-50 dark:bg-gray-800 px-2 py-1 rounded 
                               text-gray-600 dark:text-gray-300"
                  >
                    {maintainer.pgpKey}
                  </code>
                  <CopyButton
                    text={maintainer.pgpKey}
                    className="opacity-0 group-hover/item:opacity-100"
                  />
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </motion.div>
  );
}
