import { useState } from "react";
import { Check, Copy } from "lucide-react";
import { motion } from "framer-motion";
import toast from "react-hot-toast";

interface CopyButtonProps {
  text: string;
  className?: string;
}

export default function CopyButton({ text, className = "" }: CopyButtonProps) {
  const [copied, setCopied] = useState(false);

  const handleCopy = async () => {
    try {
      await navigator.clipboard.writeText(text);
      setCopied(true);
      toast.success("Copied to clipboard!", {
        duration: 2000,
        position: "bottom-right",
        style: {
          background: "#10B981",
          color: "#fff",
          borderRadius: "8px",
        },
      });
      setTimeout(() => setCopied(false), 2000);
    } catch (err) {
      console.error("Failed to copy text:", err);
      toast.error("Failed to copy text", {
        duration: 2000,
        position: "bottom-right",
        style: {
          background: "#EF4444",
          color: "#fff",
          borderRadius: "8px",
        },
      });
    }
  };

  return (
    <motion.button
      onClick={handleCopy}
      whileTap={{ scale: 0.95 }}
      className={`inline-flex items-center justify-center p-2 rounded-lg 
                 transition-colors duration-200 ${
                   copied
                     ? "bg-green-100 dark:bg-green-900/30 text-green-600 dark:text-green-400"
                     : "bg-gray-100 dark:bg-gray-800 text-gray-600 dark:text-gray-400 hover:bg-gray-200 dark:hover:bg-gray-700"
                 } ${className}`}
      title={copied ? "Copied!" : "Copy to clipboard"}
    >
      {copied ? <Check className="w-4 h-4" /> : <Copy className="w-4 h-4" />}
    </motion.button>
  );
}
