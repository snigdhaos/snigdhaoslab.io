import { motion } from "framer-motion";
import type { LucideIcon } from "lucide-react";

interface GradientIconProps {
  Icon: LucideIcon;
  gradientFrom: string;
  gradientTo: string;
  size?: number;
  className?: string;
}

export default function GradientIcon({
  Icon,
  gradientFrom,
  gradientTo,
  size = 24,
  className = "",
}: GradientIconProps) {
  const gradientId = `gradient-${gradientFrom}-${gradientTo}`.replace("#", "");

  return (
    <motion.div
      whileHover={{ scale: 1.1, rotate: 5 }}
      transition={{ type: "spring", stiffness: 300 }}
      className={className}
    >
      <svg width={size} height={size} className="relative z-10">
        <defs>
          <linearGradient id={gradientId} x1="0%" y1="0%" x2="100%" y2="100%">
            <stop offset="0%" style={{ stopColor: gradientFrom }} />
            <stop offset="100%" style={{ stopColor: gradientTo }} />
          </linearGradient>
        </defs>
        <Icon
          size={size}
          className="stroke-[url(#${gradientId})]"
          strokeWidth={2}
        />
      </svg>
      <div
        className="absolute inset-0 blur-lg opacity-20"
        style={{
          background: `linear-gradient(135deg, ${gradientFrom}, ${gradientTo})`,
        }}
      />
    </motion.div>
  );
}
