import { Link } from "react-router-dom";

export default function Logo() {
  return (
    <Link to="/" className="flex items-center space-x-2">
      <img
        src="/logo.svg"
        alt="Snigdha OS"
        className="w-8 h-8 text-indigo-600 dark:text-indigo-400"
      />
      <span className="text-xl font-bold">Snigdha OS</span>
    </Link>
  );
}
