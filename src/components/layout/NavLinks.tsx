import { Link, useLocation } from "react-router-dom";
import {
  Home,
  Info,
  Download,
  Users,
  Heart,
  Image,
} from "lucide-react";
import type { NavItem } from "../../types";

const navItems: NavItem[] = [
  { label: "Home", path: "/", icon: Home },
  { label: "About", path: "/about", icon: Info },
  { label: "Download", path: "/download", icon: Download },
  { label: "Screenshots", path: "/view", icon: Image },
  { label: "Developers", path: "/developers", icon: Users },
  { label: "Donate", path: "/donate", icon: Heart },
];

export default function NavLinks() {
  const location = useLocation();

  return (
    <>
      {navItems.map((item) => {
        const Icon = item.icon;
        return (
          <div
            key={item.path}
            className={`text-sm font-medium transition-colors flex items-center gap-2 ${
              location.pathname === item.path
                ? "text-indigo-600 dark:text-indigo-400"
                : "text-gray-700 dark:text-gray-300 hover:text-indigo-600 dark:hover:text-indigo-400"
            }`}
          >
            <Link to={item.path}>
              {Icon && <Icon className="w-4 h-4" />}
              {item.label}
            </Link>
          </div>
        );
      })}
    </>
  );
}