import { useState } from "react";
import { Menu, X } from "lucide-react";
import {
  Home,
  Info,
  // PenTool as Tools,
  Download,
  Users,
  Heart,
} from "lucide-react";
import ThemeToggle from "./ThemeToggle";
import Logo from "./Logo";
import NavLinks from "./NavLinks";
import MobileMenu from "./MobileMenu";
import type { NavItem } from "../../types";

const navItems: NavItem[] = [
  { label: "Home", path: "/", icon: Home },
  { label: "About", path: "/about", icon: Info },
  // { label: "Tools", path: "/tools", icon: Tools },
  { label: "Download", path: "/download", icon: Download },
  { label: "Developers", path: "/developers", icon: Users },
  { label: "Donate", path: "/donate", icon: Heart },
];

export default function Navbar() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <nav className="fixed top-0 left-0 right-0 z-50 bg-white/80 dark:bg-gray-900/80 backdrop-blur-lg">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="flex items-center justify-between h-16">
          <Logo />

          {/* Desktop Navigation */}
          <div className="hidden md:flex items-center space-x-8">
            <NavLinks />
            <ThemeToggle />
          </div>

          {/* Mobile Menu Button */}
          <div className="md:hidden flex items-center space-x-4">
            <ThemeToggle />
            <button
              onClick={() => setIsOpen(!isOpen)}
              className="text-gray-700 dark:text-gray-300"
            >
              {isOpen ? (
                <X className="w-6 h-6" />
              ) : (
                <Menu className="w-6 h-6" />
              )}
            </button>
          </div>
        </div>
      </div>

      {/* Mobile Navigation */}
      <MobileMenu
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        items={navItems}
      />
    </nav>
  );
}
