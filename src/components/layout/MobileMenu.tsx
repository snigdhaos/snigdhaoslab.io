import { motion, AnimatePresence } from "framer-motion";
import { Link, useLocation } from "react-router-dom";
import {
  Home,
  Info,
  // PenTool as Tools,
  Download,
  Users,
  Heart,
  Image,
} from "lucide-react";
import type { NavItem } from "../../types";

const navItems: NavItem[] = [
  { label: "Home", path: "/", icon: Home },
  { label: "About", path: "/about", icon: Info },
  // { label: "Tools", path: "/tools", icon: Tools },
  { label: "Download", path: "/download", icon: Download },
  { label: "Screenshots", path: "/view", icon: Image },
  { label: "Developers", path: "/developers", icon: Users },
  { label: "Donate", path: "/donate", icon: Heart },
];

interface MobileMenuProps {
  isOpen: boolean;
  onClose: () => void;
}

export default function MobileMenu({ isOpen, onClose }: MobileMenuProps) {
  const location = useLocation();

  return (
    <AnimatePresence>
      {isOpen && (
        <div className="md:hidden bg-white dark:bg-gray-900">
          <motion.div
            initial={{ opacity: 0, height: 0 }}
            animate={{ opacity: 1, height: "auto" }}
            exit={{ opacity: 0, height: 0 }}
          >
            <div className="px-2 pt-2 pb-3 space-y-1">
              {navItems.map((item) => {
                const Icon = item.icon;
                return (
                  <Link
                    key={item.path}
                    to={item.path}
                    onClick={onClose}
                    className={`block px-3 py-2 rounded-md text-base font-medium flex items-center gap-2 ${
                      location.pathname === item.path
                        ? "bg-indigo-100 dark:bg-indigo-900 text-indigo-600 dark:text-indigo-400"
                        : "text-gray-700 dark:text-gray-300 hover:bg-gray-100 dark:hover:bg-gray-800"
                    }`}
                  >
                    {Icon && <Icon className="w-5 h-5" />}
                    {item.label}
                  </Link>
                );
              })}
            </div>
          </motion.div>
        </div>
      )}
    </AnimatePresence>
  );
}