import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { Link } from "react-router-dom";
import {
  Download,
  Usb,
  Settings,
  ArrowRight,
  Terminal,
  Shield,
  Cpu,
} from "lucide-react";

const steps = [
  {
    title: "Download & Verify",
    description:
      "Download the latest ISO image and verify its checksum to ensure integrity.",
    icon: Download,
    link: "/download",
    linkText: "View Download Instructions",
    color: "from-blue-400 to-indigo-500",
    bgGlow: "from-blue-500/20 to-indigo-500/20",
  },
  {
    title: "Create Boot Media",
    description:
      "Create a bootable USB drive using your preferred imaging tool.",
    icon: Usb,
    link: "/docs/installation",
    linkText: "View USB Creation Guide",
    color: "from-purple-400 to-pink-500",
    bgGlow: "from-purple-500/20 to-pink-500/20",
  },
  {
    title: "Install & Configure",
    description:
      "Follow our step-by-step installation guide to set up Snigdha OS.",
    icon: Settings,
    link: "/docs/installation",
    linkText: "View Installation Guide",
    color: "from-emerald-400 to-teal-500",
    bgGlow: "from-emerald-500/20 to-teal-500/20",
  },
];

const features = [
  {
    icon: Terminal,
    title: "Pre-configured",
    description: "Ready to use development environment",
    color: "text-amber-500",
  },
  {
    icon: Shield,
    title: "Secure by Default",
    description: "Enhanced security features enabled",
    color: "text-purple-500",
  },
  {
    icon: Cpu,
    title: "Optimized",
    description: "Fine-tuned for best performance",
    color: "text-emerald-500",
  },
];

const containerVariants = {
  hidden: { opacity: 0 },
  visible: {
    opacity: 1,
    transition: {
      staggerChildren: 0.2,
    },
  },
};

const itemVariants = {
  hidden: { opacity: 0, y: 20 },
  visible: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.5,
    },
  },
};

export default function QuickStart() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.1,
  });

  return (
    <section className="py-20 relative overflow-hidden">
      {/* Background Elements */}
      <div className="absolute inset-0 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900" />
      <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
      <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />

      <motion.div
        ref={ref}
        variants={containerVariants}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
        className="section-container relative"
      >
        <motion.div variants={itemVariants} className="text-center mb-16">
          <div className="inline-flex items-center justify-center p-3 mb-4 rounded-2xl bg-gradient-to-br from-primary-500 to-secondary-500">
            <Terminal className="w-8 h-8 text-white" />
          </div>
          <h2 className="text-4xl font-bold mb-4">
            Get Started with <span className="gradient-text">Snigdha OS</span>
          </h2>
          <p className="text-xl text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
            Follow these simple steps to begin your journey with Snigdha OS
          </p>
        </motion.div>

        {/* Steps */}
        <div className="grid lg:grid-cols-3 gap-8 mb-16">
          {steps.map((step, index) => (
            <motion.div
              key={step.title}
              variants={itemVariants}
              className="relative group"
            >
              {/* Card */}
              <div className="card h-full relative overflow-hidden">
                {/* Background Glow Effect */}
                <div
                  className={`absolute -inset-2 bg-gradient-to-r ${step.bgGlow} rounded-3xl blur-2xl 
                              opacity-0 group-hover:opacity-100 transition-opacity duration-500`}
                />

                <div className="relative">
                  {/* Step Number */}
                  <div
                    className="absolute -top-4 -left-4 w-8 h-8 rounded-full bg-gray-100 dark:bg-gray-800 
                                flex items-center justify-center text-sm font-medium"
                  >
                    {index + 1}
                  </div>

                  {/* Icon */}
                  <div
                    className={`w-16 h-16 rounded-2xl bg-gradient-to-br ${step.color} 
                                flex items-center justify-center mb-6 transform group-hover:scale-110 
                                group-hover:rotate-6 transition-transform duration-300`}
                  >
                    <step.icon className="w-8 h-8 text-white" />
                  </div>

                  {/* Content */}
                  <h3 className="text-2xl font-semibold mb-4">{step.title}</h3>
                  <p className="text-gray-600 dark:text-gray-300 mb-6">
                    {step.description}
                  </p>

                  {/* Link */}
                  <Link
                    to={step.link}
                    className={`inline-flex items-center gap-2 text-lg font-medium bg-gradient-to-r ${step.color} 
                             bg-clip-text text-transparent group-hover:gap-3 transition-all`}
                  >
                    {step.linkText}
                    <ArrowRight
                      className={`w-5 h-5 bg-gradient-to-r ${step.color} bg-clip-text`}
                    />
                  </Link>
                </div>
              </div>
            </motion.div>
          ))}
        </div>

        {/* Features Grid */}
        <motion.div
          variants={itemVariants}
          className="grid md:grid-cols-3 gap-6 max-w-4xl mx-auto"
        >
          {features.map((feature) => (
            <div
              key={feature.title}
              className="flex items-center gap-4 p-4 rounded-xl bg-white dark:bg-gray-800/50 
                       hover:shadow-lg transition-shadow duration-300"
            >
              <div className="p-2 rounded-lg bg-gray-100 dark:bg-gray-800">
                <feature.icon className={`w-6 h-6 ${feature.color}`} />
              </div>
              <div>
                <h4 className="font-medium mb-1">{feature.title}</h4>
                <p className="text-sm text-gray-600 dark:text-gray-300">
                  {feature.description}
                </p>
              </div>
            </div>
          ))}
        </motion.div>
      </motion.div>
    </section>
  );
}
