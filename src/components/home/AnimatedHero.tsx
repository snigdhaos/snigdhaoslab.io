import { motion } from "framer-motion";
import {
  Download,
  Terminal,
  Command,
  Box,
  Cpu,
  Twitter,
  ArrowRight,
  Book,
} from "lucide-react";
import { Link } from "react-router-dom";
import TypedHeader from "./TypedHeader";

const containerVariants = {
  hidden: { opacity: 0 },
  visible: {
    opacity: 1,
    transition: {
      staggerChildren: 0.2,
      delayChildren: 0.3,
    },
  },
};

const itemVariants = {
  hidden: { opacity: 0, y: 20 },
  visible: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.5,
      ease: "easeOut",
    },
  },
};

// const floatingVariants = {
//   initial: { y: 0 },
//   animate: {
//     y: [-10, 10],
//     transition: {
//       duration: 4,
//       repeat: Infinity,
//       repeatType: "reverse",
//       ease: "easeInOut"
//     }
//   }
// };

// const glowVariants = {
//   initial: { scale: 1, opacity: 0.5 },
//   animate: {
//     scale: [1, 1.2, 1],
//     opacity: [0.5, 0.8, 0.5],
//     transition: {
//       duration: 3,
//       repeat: Infinity,
//       repeatType: "reverse",
//       ease: "easeInOut"
//     }
//   }
// };

const terminalLines = [
  { type: "input", content: "sudo pacman -S dev-tools" },
  {
    type: "output",
    content: "[INFO] Installing development tools...",
    color: "text-blue-400",
  },
  { type: "progress", percent: 100 },
  {
    type: "success",
    content: "[SUCCESS] Installation completed successfully!",
    color: "text-green-400",
  },
  { type: "input", content: "sudo pacman -Syyu" },
  {
    type: "output",
    content: "[INFO] Checking for system updates...",
    color: "text-blue-400",
  },
  {
    type: "output",
    content: "[INFO] Found 3 updates available",
    color: "text-blue-400",
  },
  { type: "progress", percent: 100 },
  {
    type: "success",
    content: "[SUCCESS] System updated to latest version!",
    color: "text-green-400",
  },
];

export default function AnimatedHero() {
  return (
    <section className="relative min-h-screen flex items-center justify-center overflow-hidden">
      {/* Animated background gradient */}
      <div className="absolute inset-0 bg-gradient-to-br from-primary-500/5 via-transparent to-secondary-500/5" />

      {/* Floating elements */}
      <div className="absolute inset-0 overflow-hidden pointer-events-none">
        {[Command, Box, Cpu, Terminal].map((Icon, i) => (
          <motion.div
            key={i}
            className="absolute"
            style={{
              left: `${Math.random() * 100}%`,
              top: `${Math.random() * 100}%`,
            }}
            initial={{ opacity: 0, scale: 0 }}
            animate={{
              opacity: [0.1, 0.3, 0.1],
              scale: [1, 1.2, 1],
              rotate: [0, 360],
              x: [0, Math.random() * 100 - 50],
              y: [0, Math.random() * 100 - 50],
            }}
            transition={{
              duration: Math.random() * 5 + 5,
              repeat: Infinity,
              repeatType: "reverse",
              ease: "easeInOut",
              delay: i * 0.5,
            }}
          >
            <Icon className="w-12 h-12 text-primary-500/20" />
          </motion.div>
        ))}
      </div>

      <div className="section-container relative z-10">
        <div className="grid lg:grid-cols-2 gap-12 items-center">
          {/* Content Section */}
          <motion.div
            variants={containerVariants}
            initial="hidden"
            animate="visible"
            className="text-center lg:text-left"
          >
            <motion.div
              variants={itemVariants}
              className="inline-block mb-4 px-4 py-1.5 rounded-full bg-gradient-to-r 
                       from-primary-500/10 to-secondary-500/10 border border-primary-500/20"
            >
              <span className="bg-gradient-to-r from-primary-500 to-secondary-500 bg-clip-text text-transparent font-medium">
                Upcoming Version 2.0!
              </span>
            </motion.div>

            {/* Twitter Card */}
            <motion.a
              href="https://twitter.com/eshanized"
              target="_blank"
              rel="noopener noreferrer"
              variants={itemVariants}
              className="inline-flex items-center gap-3 px-4 py-2 bg-[#1DA1F2]/10 rounded-full mb-6 
                       hover:bg-[#1DA1F2]/20 transition-colors group"
            >
              <div className="p-1.5 rounded-full bg-[#1DA1F2]">
                <Twitter className="w-4 h-4 text-white" />
              </div>
              <p className="text-sm text-gray-600 dark:text-gray-300 truncate max-w-[full]">
                Building the future of Linux with Snigdha OS!
              </p>
              <div
                className="w-6 h-6 rounded-full bg-[#1DA1F2]/10 flex items-center justify-center 
                           group-hover:bg-[#1DA1F2]/20 transition-colors"
              >
                <ArrowRight className="w-4 h-4 text-[#1DA1F2]" />
              </div>
            </motion.a>

            <motion.div variants={itemVariants} className="mb-6">
              <h1 className="text-5xl md:text-6xl lg:text-7xl font-bold leading-tight mb-4">
                The Future of
              </h1>
              <TypedHeader />
            </motion.div>

            <motion.p
              variants={itemVariants}
              className="text-xl md:text-2xl text-gray-600 dark:text-gray-300 mb-8 leading-relaxed"
            >
              Experience the next generation of Linux with
              <br className="hidden md:block" />
              unmatched performance and security.
            </motion.p>

            <motion.div
              variants={itemVariants}
              className="flex flex-col sm:flex-row gap-4 justify-center lg:justify-start"
            >
              <Link
                to="/download"
                className="btn-primary text-lg px-8 py-4 group relative overflow-hidden"
              >
                <span className="relative z-10 flex items-center gap-2">
                  <Download className="w-6 h-6 group-hover:animate-bounce" />
                  Download
                </span>
                <motion.div
                  className="absolute inset-0 bg-gradient-to-r from-primary-600 to-secondary-600"
                  initial={{ x: "100%" }}
                  whileHover={{ x: 0 }}
                  transition={{ duration: 0.3 }}
                />
              </Link>
              <Link
                to="/about"
                className="btn-secondary text-lg px-8 py-4 group relative overflow-hidden"
              >
                <span className="relative z-10 flex items-center gap-2">
                  <Book className="w-6 h-6 group-hover:rotate-12 transition-transform" />
                  Documentation
                </span>
                <motion.div
                  className="absolute inset-0 bg-gradient-to-r from-secondary-600 to-accent-600"
                  initial={{ x: "-100%" }}
                  whileHover={{ x: 0 }}
                  transition={{ duration: 0.3 }}
                />
              </Link>
            </motion.div>
          </motion.div>

          {/* Terminal Preview Section */}
          <motion.div
            className="relative"
            // variants={floatingVariants}
            initial="initial"
            animate="animate"
          >
            <motion.div
              className="absolute -inset-4 bg-gradient-to-r from-primary-500 to-secondary-500 rounded-2xl opacity-20 blur-2xl"
              animate={{
                scale: [1, 1.1, 1],
                opacity: [0.1, 0.2, 0.1],
              }}
              transition={{
                duration: 4,
                repeat: Infinity,
                repeatType: "reverse",
              }}
            />

            <div className="relative rounded-2xl overflow-hidden shadow-2xl bg-gray-900/95 backdrop-blur-xl border border-gray-700/50">
              {/* Terminal Header */}
              <div className="bg-gray-800/90 px-4 py-2 flex items-center gap-2">
                <div className="flex gap-1.5">
                  <motion.div
                    className="w-3 h-3 rounded-full bg-red-500"
                    whileHover={{ scale: 1.2 }}
                  />
                  <motion.div
                    className="w-3 h-3 rounded-full bg-yellow-500"
                    whileHover={{ scale: 1.2 }}
                  />
                  <motion.div
                    className="w-3 h-3 rounded-full bg-green-500"
                    whileHover={{ scale: 1.2 }}
                  />
                </div>
                <div className="flex-1 text-center">
                  <span className="text-sm text-gray-400 font-medium">
                    SNIGDHA OS
                  </span>
                </div>
              </div>

              {/* Terminal Content */}
              <div className="p-6 font-mono text-sm space-y-2">
                {terminalLines.map((line, index) => (
                  <motion.div
                    key={index}
                    initial={{ opacity: 0, x: -20 }}
                    animate={{ opacity: 1, x: 0 }}
                    transition={{ delay: index * 0.2 }}
                  >
                    {line.type === "input" ? (
                      <div className="flex items-center text-gray-300">
                        <span className="text-purple-400">whoami</span>
                        <span className="text-gray-500">@</span>
                        <span className="text-blue-400">snigdhaos</span>
                        <span className="text-gray-500">:~$</span>
                        <span className="ml-2">{line.content}</span>
                      </div>
                    ) : line.type === "progress" ? (
                      <div className="space-y-1">
                        <div className="w-full h-2 bg-gray-700 rounded-full overflow-hidden">
                          <motion.div
                            className="h-full bg-gradient-to-r from-primary-500 to-secondary-500 rounded-full"
                            initial={{ width: 0 }}
                            animate={{ width: `${line.percent}%` }}
                            transition={{ duration: 1, ease: "easeOut" }}
                          />
                        </div>
                      </div>
                    ) : (
                      <div className={line.color}>{line.content}</div>
                    )}
                  </motion.div>
                ))}
                <motion.div
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  transition={{ delay: terminalLines.length * 0.2 }}
                >
                  <div className="flex items-center text-gray-300">
                    <span className="text-purple-400">whoami</span>
                    <span className="text-gray-500">@</span>
                    <span className="text-blue-400">snigdhaos</span>
                    <span className="text-gray-500">:~$</span>
                    <motion.span
                      animate={{ opacity: [0, 1] }}
                      transition={{ duration: 0.8, repeat: Infinity }}
                      className="inline-block w-2 h-4 bg-gray-300"
                    />
                  </div>
                </motion.div>
              </div>
            </div>

            {/* Decorative Elements */}
            <motion.div
              className="absolute -right-8 -bottom-8 w-32 h-32 bg-accent-500/20 rounded-full blur-2xl"
              animate={{
                scale: [1, 1.2, 1],
                opacity: [0.3, 0.6, 0.3],
              }}
              transition={{
                duration: 4,
                repeat: Infinity,
                repeatType: "reverse",
              }}
            />
          </motion.div>
        </div>
      </div>
    </section>
  );
}
