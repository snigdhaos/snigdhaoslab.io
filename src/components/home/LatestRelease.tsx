import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { Link } from "react-router-dom";
import {
  Download,
  Calendar,
  HardDrive,
  Cpu,
  Sparkles,
  RefreshCw,
  Palette,
  Shield,
  ArrowRight,
} from "lucide-react";
import { useState, useEffect } from "react";
import { getLatestRelease } from "../../data/latest";
import type { Release } from "../../types";

const containerVariants = {
  hidden: { opacity: 0, y: 50 },
  visible: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.6,
      ease: "easeOut",
    },
  },
};

const featureIcons = {
  0: Sparkles,
  1: RefreshCw,
  2: Palette,
  3: Shield,
};

export default function LatestRelease() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.1,
  });

  const [release, setRelease] = useState<Release | null>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function fetchLatestRelease() {
      try {
        const data = await getLatestRelease();
        setRelease(data);
      } catch (error) {
        console.error("Failed to fetch latest release:", error);
      } finally {
        setLoading(false);
      }
    }

    fetchLatestRelease();
  }, []);

  if (loading || !release) {
    return (
      <section className="section-container bg-gradient-to-b from-secondary-500/10 via-transparent to-transparent py-20">
        <div className="flex justify-center items-center min-h-[400px]">
          <div className="animate-spin rounded-full h-12 w-12 border-4 border-primary-500 border-t-transparent"></div>
        </div>
      </section>
    );
  }

  return (
    <section className="py-20 relative overflow-hidden">
      {/* Background Elements */}
      <div className="absolute inset-0 bg-gradient-to-b from-secondary-500/5 via-transparent to-transparent" />
      <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
      <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />

      <div className="section-container relative">
        <motion.div
          ref={ref}
          variants={containerVariants}
          initial="hidden"
          animate={inView ? "visible" : "hidden"}
          className="max-w-5xl mx-auto"
        >
          {/* Header */}
          <div className="text-center mb-12">
            <motion.div
              initial={{ scale: 0 }}
              animate={{ scale: 1 }}
              transition={{ type: "spring", stiffness: 200, delay: 0.2 }}
              className="inline-flex items-center justify-center p-3 mb-4 rounded-2xl bg-gradient-to-br from-primary-500 to-secondary-500"
            >
              <Download className="w-8 h-8 text-white" />
            </motion.div>
            <h2 className="text-4xl font-bold mb-4">
              Latest <span className="gradient-text">Release</span>
            </h2>
            <p className="text-xl text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
              Experience the newest features and improvements in Snigdha OS
            </p>
          </div>

          {/* Main Card */}
          <div className="bg-white/80 dark:bg-gray-800/80 backdrop-blur-xl rounded-3xl shadow-xl overflow-hidden">
            {/* Version Banner */}
            <div className="bg-gradient-to-r from-primary-500 to-secondary-500 p-6 relative overflow-hidden">
              <div className="absolute inset-0 bg-grid-white/10" />
              <div className="relative flex items-center justify-between">
                <div>
                  <h3 className="text-2xl font-bold text-white mb-2">
                    Version {release.version}
                  </h3>
                  <div className="flex items-center gap-3 text-white/80">
                    <Calendar className="w-5 h-5" />
                    <span>Released on {release.date}</span>
                  </div>
                </div>
                <motion.div
                  whileHover={{ scale: 1.05 }}
                  whileTap={{ scale: 0.95 }}
                  className="px-6 py-3 bg-white/10 rounded-xl border border-white/20 backdrop-blur-sm
                           text-white font-medium hover:bg-white/20 transition-colors"
                >
                  {release.type}
                </motion.div>
              </div>
            </div>

            <div className="p-8">
              {/* Metrics Grid */}
              <div className="grid grid-cols-3 gap-6 mb-8">
                <div className="p-4 rounded-2xl bg-gray-50 dark:bg-gray-700/50">
                  <div className="flex items-center gap-3 mb-2">
                    <div className="p-2 rounded-lg bg-blue-100 dark:bg-blue-900/30">
                      <Cpu className="w-5 h-5 text-blue-500" />
                    </div>
                    <span className="font-medium">Boot Time</span>
                  </div>
                  <p className="text-2xl font-bold text-blue-500">
                    {release.metrics.bootTime}
                  </p>
                </div>
                <div className="p-4 rounded-2xl bg-gray-50 dark:bg-gray-700/50">
                  <div className="flex items-center gap-3 mb-2">
                    <div className="p-2 rounded-lg bg-purple-100 dark:bg-purple-900/30">
                      <HardDrive className="w-5 h-5 text-purple-500" />
                    </div>
                    <span className="font-medium">Memory Usage</span>
                  </div>
                  <p className="text-2xl font-bold text-purple-500">
                    {release.metrics.memoryUsage}
                  </p>
                </div>
                <div className="p-4 rounded-2xl bg-gray-50 dark:bg-gray-700/50">
                  <div className="flex items-center gap-3 mb-2">
                    <div className="p-2 rounded-lg bg-emerald-100 dark:bg-emerald-900/30">
                      <Download className="w-5 h-5 text-emerald-500" />
                    </div>
                    <span className="font-medium">Package Speed</span>
                  </div>
                  <p className="text-2xl font-bold text-emerald-500">
                    {release.metrics.packageInstallSpeed}
                  </p>
                </div>
              </div>

              {/* Changes Grid */}
              <div className="grid md:grid-cols-2 gap-4 mb-8">
                {release.changes.map((change, index) => {
                  const Icon = featureIcons[index % 4];
                  return (
                    <motion.div
                      key={index}
                      initial={{ opacity: 0, x: -20 }}
                      animate={{ opacity: 1, x: 0 }}
                      transition={{ delay: index * 0.1 }}
                      className="flex items-start gap-4 p-4 rounded-xl bg-gray-50 dark:bg-gray-700/50 group
                             hover:bg-gradient-to-r hover:from-primary-50 hover:to-secondary-50 
                             dark:hover:from-primary-900/20 dark:hover:to-secondary-900/20 transition-colors"
                    >
                      <div
                        className="p-2 rounded-lg bg-gradient-to-br from-primary-500 to-secondary-500 text-white
                                transform group-hover:scale-110 group-hover:rotate-6 transition-transform"
                      >
                        <Icon className="w-5 h-5" />
                      </div>
                      <p className="text-gray-600 dark:text-gray-300">
                        {change}
                      </p>
                    </motion.div>
                  );
                })}
              </div>

              {/* Action Buttons */}
              <div className="flex flex-col sm:flex-row gap-4">
                <Link
                  to="/download"
                  className="flex-1 inline-flex items-center justify-center gap-2 px-6 py-3 rounded-xl
                           bg-gradient-to-r from-primary-500 to-secondary-500 text-white font-medium
                           hover:from-primary-600 hover:to-secondary-600 transition-colors group"
                >
                  <Download className="w-5 h-5 group-hover:animate-bounce" />
                  Download v{release.version}
                </Link>
                <Link
                  to="/about"
                  className="flex-1 inline-flex items-center justify-center gap-2 px-6 py-3 rounded-xl
                           bg-gray-100 dark:bg-gray-700 text-gray-700 dark:text-gray-300 font-medium
                           hover:bg-gray-200 dark:hover:bg-gray-600 transition-colors group"
                >
                  Learn More
                  <ArrowRight className="w-5 h-5 group-hover:translate-x-1 transition-transform" />
                </Link>
              </div>
            </div>
          </div>
        </motion.div>
      </div>
    </section>
  );
}
