import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import type { FeatureCard } from "../../types";
import {
  Zap,
  Shield,
  Code,
  Package,
  Cpu,
  Cloud,
  Palette,
  Terminal,
} from "lucide-react";

const features: FeatureCard[] = [
  {
    title: "Blazing Fast Performance",
    description:
      "Optimized system processes and minimal resource usage for lightning-quick operations.",
    icon: Zap,
    color: "from-yellow-400 to-orange-500",
  },
  {
    title: "Enhanced Security",
    description:
      "Built-in security features and regular updates to keep your system protected.",
    icon: Shield,
    color: "from-emerald-400 to-teal-500",
  },
  {
    title: "Developer Friendly",
    description:
      "Pre-configured development environment with essential tools and frameworks.",
    icon: Code,
    color: "from-blue-400 to-indigo-500",
  },
  {
    title: "Smart Package Management",
    description:
      "Intuitive package manager with access to thousands of applications.",
    icon: Package,
    color: "from-purple-400 to-pink-500",
  },
  {
    title: "Resource Optimization",
    description:
      "Efficient resource management for optimal system performance.",
    icon: Cpu,
    color: "from-red-400 to-rose-500",
  },
  {
    title: "Cloud Integration",
    description:
      "Seamless integration with cloud services and storage solutions.",
    icon: Cloud,
    color: "from-sky-400 to-cyan-500",
  },
  {
    title: "Beautiful Themes",
    description:
      "Customizable themes and visual settings for your perfect setup.",
    icon: Palette,
    color: "from-fuchsia-400 to-violet-500",
  },
  {
    title: "Advanced Terminal",
    description:
      "Feature-rich terminal with modern capabilities and customizations.",
    icon: Terminal,
    color: "from-lime-400 to-green-500",
  },
];

const containerVariants = {
  hidden: { opacity: 0 },
  visible: {
    opacity: 1,
    transition: {
      staggerChildren: 0.1,
    },
  },
};

const itemVariants = {
  hidden: { opacity: 0, y: 20 },
  visible: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 0.5,
    },
  },
};

export default function Features() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.1,
  });

  return (
    <section className="section-container py-20" ref={ref}>
      <motion.div
        variants={containerVariants}
        initial="hidden"
        animate={inView ? "visible" : "hidden"}
      >
        <motion.h2
          variants={itemVariants}
          className="text-4xl md:text-5xl font-bold text-center mb-4"
        >
          Why Choose <span className="gradient-text">Snigdha OS</span>?
        </motion.h2>
        <motion.p
          variants={itemVariants}
          className="text-xl text-gray-600 dark:text-gray-300 text-center mb-16 max-w-3xl mx-auto"
        >
          Experience the perfect blend of power, security, and elegance
        </motion.p>

        <div className="grid md:grid-cols-2 lg:grid-cols-4 gap-8">
          {features.map((feature, index) => (
            <motion.div
              key={feature.title}
              variants={itemVariants}
              className="card group hover:scale-105 transition-all duration-300 overflow-hidden
                       hover:shadow-xl dark:hover:shadow-2xl dark:hover:shadow-primary-500/10"
              style={{
                transitionDelay: `${index * 50}ms`,
              }}
            >
              <div className="relative">
                <div
                  className={`absolute -top-6 -left-6 w-16 h-16 rounded-full bg-gradient-to-br ${feature.color} 
                              opacity-20 blur-xl group-hover:opacity-40 transition-opacity`}
                />
                <div
                  className={`w-14 h-14 rounded-xl bg-gradient-to-br ${feature.color} 
                              flex items-center justify-center mb-6 transform group-hover:scale-110 
                              group-hover:rotate-3 transition-transform duration-300`}
                >
                  <feature.icon className="w-7 h-7 text-white" />
                </div>
              </div>
              <h3 className="text-xl font-semibold mb-3 group-hover:text-primary-500 transition-colors">
                {feature.title}
              </h3>
              <p className="text-gray-600 dark:text-gray-300">
                {feature.description}
              </p>
            </motion.div>
          ))}
        </div>
      </motion.div>
    </section>
  );
}
