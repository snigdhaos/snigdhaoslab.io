import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { Users, Download, Star, GitBranch } from "lucide-react";

const stats = [
  {
    label: "Active Users",
    value: "N/A",
    icon: Users,
    color: "from-blue-500 to-indigo-500",
  },
  {
    label: "Downloads",
    value: "N/A",
    icon: Download,
    color: "from-emerald-500 to-teal-500",
  },
  {
    label: "GitLab Stars",
    value: "N/A",
    icon: Star,
    color: "from-amber-500 to-orange-500",
  },
  {
    label: "Commits",
    value: "5K+",
    icon: GitBranch,
    color: "from-purple-500 to-pink-500",
  },
];

export default function Statistics() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.1,
  });

  return (
    <section className="py-20 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900">
      <div className="section-container">
        <motion.div
          ref={ref}
          initial={{ opacity: 0 }}
          animate={inView ? { opacity: 1 } : { opacity: 0 }}
          className="grid grid-cols-2 md:grid-cols-4 gap-8"
        >
          {stats.map((stat, index) => (
            <motion.div
              key={stat.label}
              initial={{ opacity: 0, y: 20 }}
              animate={inView ? { opacity: 1, y: 0 } : { opacity: 0, y: 20 }}
              transition={{ delay: index * 0.1 }}
              className="text-center group"
            >
              <div className="mb-4 relative">
                <div
                  className={`w-16 h-16 mx-auto rounded-2xl bg-gradient-to-br ${stat.color}
                              flex items-center justify-center transform group-hover:scale-110 
                              group-hover:rotate-6 transition-transform duration-300`}
                >
                  <stat.icon className="w-8 h-8 text-white" />
                </div>
                <div
                  className={`absolute -inset-2 bg-gradient-to-r ${stat.color} opacity-20 
                              blur-xl group-hover:opacity-30 transition-opacity rounded-3xl`}
                />
              </div>
              <h3 className="text-3xl font-bold mb-2">{stat.value}</h3>
              <p className="text-gray-600 dark:text-gray-300">{stat.label}</p>
            </motion.div>
          ))}
        </motion.div>
      </div>
    </section>
  );
}
