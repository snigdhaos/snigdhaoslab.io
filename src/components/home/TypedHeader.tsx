import { useState, useEffect } from "react";
import { motion, AnimatePresence } from "framer-motion";

const words = ["Powerful", "Secure", "Modern", "Fast", "Reliable"];

const colors = [
  "from-blue-500 to-indigo-500",
  "from-purple-500 to-pink-500",
  "from-emerald-500 to-teal-500",
  "from-amber-500 to-orange-500",
  "from-red-500 to-rose-500",
];

export default function TypedHeader() {
  const [currentWord, setCurrentWord] = useState(0);
  const [isDeleting, setIsDeleting] = useState(false);
  const [text, setText] = useState("");
  const [delta, setDelta] = useState(150);

  useEffect(() => {
    let timeout: number;

    if (isDeleting) {
      timeout = window.setTimeout(() => {
        setText(words[currentWord].substring(0, text.length - 1));
        setDelta(50);
      }, delta);
    } else {
      timeout = window.setTimeout(() => {
        setText(words[currentWord].substring(0, text.length + 1));
        setDelta(150);
      }, delta);
    }

    if (!isDeleting && text === words[currentWord]) {
      timeout = window.setTimeout(() => {
        setIsDeleting(true);
        setDelta(100);
      }, 2000);
    } else if (isDeleting && text === "") {
      setIsDeleting(false);
      setCurrentWord((prev) => (prev + 1) % words.length);
      setDelta(200);
    }

    return () => clearTimeout(timeout);
  }, [text, isDeleting, currentWord, delta]);

  return (
    <div className="h-[40px] sm:h-[50px] flex items-center justify-center sm:justify-start overflow-hidden">
      <AnimatePresence mode="wait">
        <motion.div
          key={text}
          initial={{ y: 20, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          exit={{ y: -20, opacity: 0 }}
          transition={{ duration: 0.2 }}
          className="relative"
        >
          <span
            className={`text-3xl sm:text-4xl md:text-5xl font-bold bg-gradient-to-r ${colors[currentWord]} 
                        bg-clip-text text-transparent filter drop-shadow-sm`}
          >
            {text}
          </span>
          <motion.div
            className={`absolute -right-1 top-0 h-full w-[3px] bg-gradient-to-b ${colors[currentWord]}`}
            initial={{ opacity: 0 }}
            animate={{ opacity: [0, 1, 0] }}
            transition={{
              duration: 0.8,
              repeat: Infinity,
              ease: "linear",
            }}
          />
        </motion.div>
      </AnimatePresence>
    </div>
  );
}
