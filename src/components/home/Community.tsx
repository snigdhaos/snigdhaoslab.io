import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import {
  Users,
  Gitlab,
  Twitter,
  MessageCircle as Telegram,
} from "lucide-react";

const platforms = [
  {
    name: "GitLab",
    description:
      "Join our open-source community on GitLab and help shape the future of Snigdha OS. Contribute code, report issues, and collaborate with fellow developers.",
    icon: Gitlab,
    link: "https://gitlab.com/snigdhaos",
    color: "from-[#2A3544] to-[#22272E]",
    glow: "from-[#2A3544]/20 to-[#22272E]/20",
    stats: {
      value: "2",
      label: "Contributors",
    },
    features: [
      "Access to source code",
      "Issue tracking",
      "Pull requests",
      "Project discussions",
    ],
  },
  {
    name: "Telegram",
    description:
      "Join our vibrant Telegram community for real-time discussions, support, and collaboration. Connect with developers and users from around the world.",
    icon: Telegram,
    link: "https://t.me/snigdhaos",
    color: "from-[#5865F2] to-[#4752C4]",
    glow: "from-[#5865F2]/20 to-[#4752C4]/20",
    stats: {
      value: "N/A",
      label: "Members",
    },
    features: [
      "Live chat support",
      "Development updates",
      "Community events",
      "Voice channels",
    ],
  },
  {
    name: "Twitter",
    description:
      "Follow us on Twitter for the latest news, tips, and community highlights. Stay updated with announcements and engage with our growing community.",
    icon: Twitter,
    link: "https://twitter.com/snigdhaos",
    color: "from-[#1DA1F2] to-[#0D8BD9]",
    glow: "from-[#1DA1F2]/20 to-[#0D8BD9]/20",
    stats: {
      value: "N/A",
      label: "Followers",
    },
    features: [
      "Latest updates",
      "Community highlights",
      "Tips & tricks",
      "Live discussions",
    ],
  },
];

export default function Community() {
  const [ref, inView] = useInView({
    triggerOnce: true,
    threshold: 0.1,
  });

  return (
    <section className="py-20 relative overflow-hidden">
      {/* Background Elements */}
      <div className="absolute inset-0 bg-gradient-to-b from-gray-50 via-white to-gray-50 dark:from-gray-900 dark:via-gray-800 dark:to-gray-900" />
      <div className="absolute -top-40 -right-40 w-96 h-96 bg-primary-500/10 rounded-full blur-3xl" />
      <div className="absolute -bottom-40 -left-40 w-96 h-96 bg-secondary-500/10 rounded-full blur-3xl" />

      <div className="section-container relative">
        <motion.div
          ref={ref}
          initial={{ opacity: 0 }}
          animate={inView ? { opacity: 1 } : { opacity: 0 }}
          className="text-center mb-12"
        >
          <div className="inline-flex items-center justify-center p-3 mb-4 rounded-2xl bg-gradient-to-br from-primary-500 to-secondary-500">
            <Users className="w-8 h-8 text-white" />
          </div>
          <h2 className="text-4xl font-bold mb-4">
            Join Our <span className="gradient-text">Community</span>
          </h2>
          <p className="text-xl text-gray-600 dark:text-gray-300 max-w-2xl mx-auto">
            Connect with fellow developers and contributors across our platforms
          </p>
        </motion.div>

        <div className="grid lg:grid-cols-3 gap-8">
          {platforms.map((platform, index) => (
            <motion.a
              key={platform.name}
              href={platform.link}
              target="_blank"
              rel="noopener noreferrer"
              initial={{ opacity: 0, y: 20 }}
              animate={inView ? { opacity: 1, y: 0 } : { opacity: 0, y: 20 }}
              transition={{ delay: index * 0.2 }}
              className="group relative"
            >
              {/* Card */}
              <div
                className="h-full bg-white dark:bg-gray-800 rounded-2xl p-8 relative overflow-hidden
                           shadow-lg hover:shadow-2xl transition-all duration-300 z-10"
              >
                {/* Background Glow */}
                <div
                  className={`absolute -inset-2 bg-gradient-to-r ${platform.glow} rounded-3xl blur-2xl 
                              opacity-0 group-hover:opacity-100 transition-opacity duration-500`}
                />

                <div className="relative">
                  {/* Icon & Title */}
                  <div className="flex items-center gap-4 mb-6">
                    <div
                      className={`p-4 rounded-xl bg-gradient-to-br ${platform.color} 
                                 group-hover:scale-110 group-hover:rotate-6 transition-all duration-300`}
                    >
                      <platform.icon className="w-6 h-6 text-white" />
                    </div>
                    <div>
                      <h3 className="text-xl font-semibold">{platform.name}</h3>
                      <div
                        className={`text-sm bg-gradient-to-r ${platform.color} bg-clip-text text-transparent`}
                      >
                        {platform.stats.value} {platform.stats.label}
                      </div>
                    </div>
                  </div>

                  {/* Description */}
                  <p className="text-gray-600 dark:text-gray-300 mb-6 min-h-[80px]">
                    {platform.description}
                  </p>

                  {/* Features */}
                  <ul className="space-y-3 mb-8">
                    {platform.features.map((feature, i) => (
                      <li
                        key={i}
                        className="flex items-center gap-2 text-sm text-gray-600 dark:text-gray-300"
                      >
                        <div
                          className={`w-1.5 h-1.5 rounded-full bg-gradient-to-r ${platform.color}`}
                        />
                        {feature}
                      </li>
                    ))}
                  </ul>

                  {/* Join Button */}
                  <div
                    className={`inline-flex items-center gap-2 text-sm font-medium bg-gradient-to-r 
                                ${platform.color} bg-clip-text text-transparent group-hover:gap-3 transition-all`}
                  >
                    Join {platform.name}
                    <svg
                      className="w-4 h-4 transition-transform group-hover:translate-x-1"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M17 8l4 4m0 0l-4 4m4-4H3"
                      />
                    </svg>
                  </div>
                </div>
              </div>
            </motion.a>
          ))}
        </div>
      </div>
    </section>
  );
}
