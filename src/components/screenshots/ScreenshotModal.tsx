import { motion } from "framer-motion";
import { X, Download, Share2, User, Calendar, Tag } from "lucide-react";
import type { Screenshot } from "../../types";

interface ScreenshotModalProps {
  screenshot: Screenshot;
  onClose: () => void;
}

export default function ScreenshotModal({
  screenshot,
  onClose,
}: ScreenshotModalProps) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      className="fixed inset-0 z-50 flex items-center justify-center p-4 bg-black/50 backdrop-blur-sm"
      onClick={onClose}
    >
      <motion.div
        layoutId={`screenshot-${screenshot.id}`}
        className="relative w-full max-w-4xl bg-white dark:bg-gray-800 rounded-2xl overflow-hidden"
        onClick={(e) => e.stopPropagation()}
      >
        <button
          onClick={onClose}
          className="absolute top-4 right-4 p-2 rounded-full bg-black/20 hover:bg-black/30 
                   text-white transition-colors z-10"
        >
          <X className="w-5 h-5" />
        </button>

        <div className="relative aspect-video">
          <img
            src={screenshot.imageUrl}
            alt={screenshot.title}
            className="w-full h-full object-cover"
          />
        </div>

        <div className="p-6">
          <div className="flex items-start justify-between gap-4 mb-4">
            <div>
              <h2 className="text-2xl font-semibold mb-2">
                {screenshot.title}
              </h2>
              <p className="text-gray-600 dark:text-gray-300">
                {screenshot.description}
              </p>
            </div>
            <div className="flex gap-2">
              <button className="btn-primary">
                <Download className="w-5 h-5" />
                Download
              </button>
              <button className="btn-secondary">
                <Share2 className="w-5 h-5" />
                Share
              </button>
            </div>
          </div>

          <div className="flex flex-wrap gap-2 mb-6">
            {screenshot.tags.map((tag) => (
              <span
                key={tag}
                className="px-3 py-1 rounded-full bg-gray-100 dark:bg-gray-700 
                         text-gray-600 dark:text-gray-300 text-sm"
              >
                <Tag className="w-4 h-4 inline-block mr-1" />
                {tag}
              </span>
            ))}
          </div>

          <div className="flex items-center justify-between text-sm text-gray-500 dark:text-gray-400">
            <div className="flex items-center gap-2">
              <User className="w-4 h-4" />
              <span>Uploaded by {screenshot.uploadedBy}</span>
            </div>
            <div className="flex items-center gap-2">
              <Calendar className="w-4 h-4" />
              <span>
                {new Date(screenshot.uploadDate).toLocaleDateString()}
              </span>
            </div>
          </div>
        </div>
      </motion.div>
    </motion.div>
  );
}
