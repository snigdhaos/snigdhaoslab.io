import { motion } from "framer-motion";
import { Calendar, User, Tag } from "lucide-react";
import type { Screenshot } from "../../types";

interface ScreenshotCardProps {
  screenshot: Screenshot;
  onClick: () => void;
}

export default function ScreenshotCard({
  screenshot,
  onClick,
}: ScreenshotCardProps) {
  return (
    <motion.div
      layoutId={`screenshot-${screenshot.id}`}
      onClick={onClick}
      className="group cursor-pointer"
      whileHover={{ y: -5 }}
    >
      <div className="relative overflow-hidden rounded-xl bg-white dark:bg-gray-800 shadow-lg">
        <div className="aspect-video overflow-hidden">
          <img
            src={screenshot.imageUrl}
            alt={screenshot.title}
            className="w-full h-full object-cover transform group-hover:scale-110 transition-transform duration-300"
          />
        </div>
        <div className="p-4">
          <h3 className="text-lg font-semibold mb-2 group-hover:text-primary-500 transition-colors">
            {screenshot.title}
          </h3>
          <p className="text-gray-600 dark:text-gray-300 text-sm mb-4">
            {screenshot.description}
          </p>
          <div className="flex flex-wrap gap-2 mb-4">
            {screenshot.tags.map((tag) => (
              <span
                key={tag}
                className="px-2 py-1 text-xs rounded-full bg-gray-100 dark:bg-gray-700 
                         text-gray-600 dark:text-gray-300"
              >
                {tag}
              </span>
            ))}
          </div>
          <div className="flex items-center justify-between text-sm text-gray-500 dark:text-gray-400">
            <div className="flex items-center gap-2">
              <User className="w-4 h-4" />
              <span>{screenshot.uploadedBy}</span>
            </div>
            <div className="flex items-center gap-2">
              <Calendar className="w-4 h-4" />
              <span>
                {new Date(screenshot.uploadDate).toLocaleDateString()}
              </span>
            </div>
          </div>
        </div>
      </div>
    </motion.div>
  );
}
