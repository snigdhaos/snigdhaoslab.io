import { motion } from "framer-motion";
import type { ScreenshotCategory } from "../../types";
import { Monitor, Layout, Palette, Terminal } from "lucide-react";

const icons = {
  desktop: Monitor,
  apps: Layout,
  themes: Palette,
  terminal: Terminal,
};

interface CategorySelectorProps {
  categories: ScreenshotCategory[];
  selectedCategory: string;
  onSelect: (categoryId: string) => void;
}

export default function CategorySelector({
  categories,
  selectedCategory,
  onSelect,
}: CategorySelectorProps) {
  return (
    <div className="grid grid-cols-2 md:grid-cols-4 gap-4">
      {categories.map((category) => {
        const Icon = icons[category.id as keyof typeof icons];
        return (
          <motion.button
            key={category.id}
            onClick={() => onSelect(category.id)}
            whileHover={{ scale: 1.02 }}
            whileTap={{ scale: 0.98 }}
            className={`p-4 rounded-xl text-left transition-all duration-200 ${
              selectedCategory === category.id
                ? "bg-primary-500 text-white shadow-lg shadow-primary-500/20"
                : "bg-white dark:bg-gray-800 hover:bg-gray-50 dark:hover:bg-gray-700"
            }`}
          >
            <div className="flex items-start gap-3">
              <div
                className={`p-2 rounded-lg ${
                  selectedCategory === category.id
                    ? "bg-white/20"
                    : "bg-primary-100 dark:bg-primary-900/30"
                }`}
              >
                <Icon
                  className={`w-5 h-5 ${
                    selectedCategory === category.id
                      ? "text-white"
                      : "text-primary-500"
                  }`}
                />
              </div>
              <div>
                <h3 className="font-medium mb-1">{category.name}</h3>
                <p
                  className={`text-sm ${
                    selectedCategory === category.id
                      ? "text-white/80"
                      : "text-gray-500 dark:text-gray-400"
                  }`}
                >
                  {category.description}
                </p>
              </div>
            </div>
          </motion.button>
        );
      })}
    </div>
  );
}
