import { Suspense } from "react";
import { HashRouter } from "react-router-dom";
import { Toaster } from "react-hot-toast";
import Navbar from "./components/layout/Navbar";
import Footer from "./components/footer/Footer";
import GitLabButton from "./components/common/GitLabButton";
import AppRoutes from "./routes";
import "./utils/i18n";
import { useGitLabCommits } from "./hooks/useGitLabCommits";

function App() {
  useGitLabCommits(); // Hook to fetch and show commits

  return (
    <HashRouter>
      <div className="min-h-screen bg-gray-50 dark:bg-gray-900">
        <Navbar />
        <main className="pt-16">
          <Suspense fallback={<div>Loading...</div>}>
            <AppRoutes />
          </Suspense>
        </main>
        <Footer />
        <GitLabButton />
        <Toaster
          position="bottom-right"
          toastOptions={{
            className:
              "bg-white dark:bg-gray-800 text-gray-900 dark:text-gray-100",
            duration: 5000,
            style: {
              padding: "16px",
              borderRadius: "12px",
              boxShadow:
                "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)",
            },
          }}
        />
      </div>
    </HashRouter>
  );
}

export default App;