# 🚀 Snigdha OS Website

[![GitLab Pipeline Status](https://img.shields.io/gitlab/pipeline/SnigdhaOS/SnigdhaOS.gitlab.io/master?logo=gitlab)](https://gitlab.com/SnigdhaOS/SnigdhaOS.gitlab.io/-/pipelines)
[![GitLab License](https://img.shields.io/gitlab/license/SnigdhaOS/SnigdhaOS.gitlab.io?color=blue)](https://gitlab.com/SnigdhaOS/SnigdhaOS.gitlab.io/-/blob/master/LICENSE)
[![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/SnigdhaOS/SnigdhaOS.gitlab.io?logo=gitlab)](https://gitlab.com/SnigdhaOS/SnigdhaOS.gitlab.io/-/commits/master)
[![GitLab Issues](https://img.shields.io/gitlab/issues/open/SnigdhaOS/SnigdhaOS.gitlab.io?logo=gitlab)](https://gitlab.com/SnigdhaOS/SnigdhaOS.gitlab.io/-/issues)
[![GitLab Stars](https://img.shields.io/badge/dynamic/json?logo=gitlab&label=stars&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F<PROJECT_ID>%2F)](https://gitlab.com/SnigdhaOS/SnigdhaOS.gitlab.io)

The **official website** for **Snigdha OS**, built with modern web technologies to deliver a fast, responsive, and visually stunning experience. 🌟

---

## ✨ Features

- 🎨 **Modern and Responsive Design**: Built with Tailwind CSS for a sleek and responsive UI.
- 🌙 **Dark Mode Support**: Automatically adapts to user preferences.
- 🌐 **Internationalization Ready**: Supports multiple languages with i18next.
- ⚡ **Blazing Fast Performance**: Optimized with Vite for lightning-fast load times.
- 📱 **Mobile-First Approach**: Fully responsive across all devices.
- 🔍 **SEO Friendly**: Optimized for search engines.
- 🎭 **Beautiful Animations**: Powered by Framer Motion for smooth and engaging animations.

---

## 🛠️ Tech Stack

- **Frontend**: React 18, TypeScript
- **Styling**: Tailwind CSS
- **Build Tool**: Vite
- **Animations**: Framer Motion
- **Routing**: React Router
- **Icons**: Lucide Icons
- **Internationalization**: i18next

---

## 🚀 Getting Started

### Prerequisites

- Node.js 18 or later
- npm or yarn

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/SnigdhaOS/SnigdhaOS.gitlab.io.git
   cd SnigdhaOS.gitlab.io
   ```

2. Install dependencies:

   ```bash
   npm install
   ```

3. Start the development server:

   ```bash
   npm run dev
   ```

4. Build for production:

   ```bash
   npm run build
   ```

5. Preview the production build:
   ```bash
   npm run preview
   ```

---

## 🌍 Translations

The website uses **i18next** for internationalization. Here's how to add new translations:

### Translation Structure

Translations are stored in the `public/locales` directory with the following structure:

```
public/locales/
├── en/
│   └── translation.json
├── es/
│   └── translation.json
└── [language-code]/
    └── translation.json
```

### Adding a New Language

1. Create a new directory in `public/locales` with the language code (e.g., `fr` for French).
2. Create a `translation.json` file inside the new directory.
3. Copy the content from `en/translation.json` and translate the values.

Example `translation.json`:

```json
{
  "home": {
    "title": "The Future of Linux",
    "description": "Experience the next generation of Linux with unmatched performance and security.",
    "downloadButton": "Download Now",
    "learnMoreButton": "Learn More"
  }
}
```

### Using Translations

In your components, use the `useTranslation` hook:

```tsx
import { useTranslation } from "react-i18next";

function MyComponent() {
  const { t } = useTranslation();

  return (
    <div>
      <h1>{t("home.title")}</h1>
      <p>{t("home.description")}</p>
    </div>
  );
}
```

---

## 🗂️ Project Structure

```
src/
├── components/     # Reusable components
├── pages/         # Page components
├── data/          # Data fetching and API
├── hooks/         # Custom React hooks
├── types/         # TypeScript types
├── utils/         # Utility functions
└── routes/        # Route definitions
```

---

## 🤝 Contributing

We welcome contributions! Here's how you can help:

1. Fork the repository.
2. Create a feature branch:
   ```bash
   git checkout -b feature/your-feature-name
   ```
3. Commit your changes:
   ```bash
   git commit -m "feat: add your feature"
   ```
4. Push to the branch:
   ```bash
   git push origin feature/your-feature-name
   ```
5. Open a merge request.

### Commit Guidelines

Follow the [Conventional Commits](https://www.conventionalcommits.org/) specification:

- `feat:` New features
- `fix:` Bug fixes
- `docs:` Documentation changes
- `style:` Code style changes
- `refactor:` Code refactoring
- `test:` Test updates
- `chore:` Build process or auxiliary tool changes

---

## 📜 License

This project is licensed under the **MIT License** - see the [LICENSE](LICENSE) file for details.

---

## 🙏 Acknowledgments

- [Vite](https://vitejs.dev/) for the blazing-fast build tool.
- [Tailwind CSS](https://tailwindcss.com/) for the utility-first CSS framework.
- [Framer Motion](https://www.framer.com/motion/) for smooth animations.
- [Lucide Icons](https://lucide.dev/) for the beautiful icons.

---

## 📞 Support

For support, please:

1. Check the [documentation](https://docs.snigdha-os.org).
2. Open an issue on [GitLab](https://gitlab.com/SnigdhaOS/SnigdhaOS.gitlab.io/-/issues).
3. Join our [Discord community](https://discord.gg/snigdhaos).

---

Made with ❤️ by the **Snigdha OS Team**.  
🌐 [Visit the Website](https://snigdha-os.org) | 🐦 [Follow us on Twitter](https://twitter.com/snigdhaos)
